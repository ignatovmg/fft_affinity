from model import NetSE3
from dataset import _rec_grid_config, _mol_to_grid

from mol_grid import grid_maker, Grid
from fft_ml.training import engines
from fft_ml.loggers import logger
from fft_ml import utils
from fft_ml.training.metrics import BaseMetrics
import subprocess

import contextlib
import torch
import torch.nn.functional as F
import prody
from rdkit import Chem
from rdkit.Chem import AllChem
from functools import partial
import numpy as np
import gc
import json
import os
from tqdm import tqdm
from path import Path
from collections import Counter

from sblu.ft import read_rotations, FTRESULT_DTYPE, read_ftresults, apply_ftresult, apply_ftresults_atom_group
from sblu.cli.docking.cmd_cluster import cluster


NUM_THREADS = 32
NUM_CONFS = 500


def _load_model(checkpoint, device):
    model = NetSE3(95, 52, 64, 64, 64,
                   kernel_size=5,
                   n_middle_layers=7,
                   dense_depth=3,
                   middle_activation=F.relu,
                   final_activation=F.tanh)
    engines.load_checkpoint(checkpoint, device, model=model)
    model.to(device)
    model.eval()
    return model


def _grid_to_tensor(grid, device):
    return torch.tensor(grid.astype(np.float32), device=device, requires_grad=False)[None, :]


def _dock_single_conf(rec_grid, lig_grid, net, device, ntop=1):
    rec_torch = _grid_to_tensor(rec_grid.grid, device)
    lig_torch = _grid_to_tensor(lig_grid.grid, device)
    energy_grid = net(rec_torch, lig_torch)
    del rec_torch
    del lig_torch

    energy_grid = energy_grid[0]
    cor_shape = [x * 2 - 1 for x in rec_grid.grid.shape[1:]]

    #cor_origin = rec.get_center() - np.array(cor_shape) / 2 * 1.25
    #Grid(energy_grid.cpu().detach().numpy().reshape(cor_shape)[None, :] / 1000, cor_origin, np.array([1.25, 1.25, 1.25])).save('cor.grid')

    energies = []
    tvs = []
    #s = energy_grid.sort()[0]
    #print(s)
    #print('asdf', s[0], s[1000], s[2000], s[5000], s[10000], s[100000])

    for min_idx in energy_grid.argsort()[:ntop]:
        min_idx = min_idx.item()
        min_energy = energy_grid[min_idx].item()
        min_idx3d = np.unravel_index(min_idx, cor_shape)
        cor_shift = (np.array(min_idx3d) - np.array(rec_grid.grid.shape[1:])) * lig_grid.delta
        tv = rec_grid.origin - lig_grid.origin + cor_shift

        energies.append(min_energy)
        tvs.append(tv)

    del energy_grid
    gc.collect()

    return energies, tvs


def _write_ft_results(fname, ft_results):
    with open(fname, 'w') as f:
        for x in ft_results:
            f.write(f'{x[0]:<4d} {x[1][0]:<8f} {x[1][1]:<8f} {x[1][2]:<8f} {x[2]:<8f} {x[3]:<4d}\n')


def _cluster_models(ag, radius):
    rmsd_matrix = []
    for i in range(ag.numCoordsets()):
        ag.setACSIndex(i)
        prody.alignCoordsets(ag)
        rmsd_matrix.append(prody.calcRMSD(ag))
    rmsd_matrix = np.stack(rmsd_matrix)
    return cluster(rmsd_matrix, radius, 1, ag.numCoordsets())


def _sample_ligand(lig_rd, cluster_radius):
    lig_rd = AllChem.AddHs(lig_rd)
    AllChem.EmbedMultipleConfs(lig_rd, numConfs=NUM_CONFS, numThreads=NUM_THREADS)
    energies = AllChem.MMFFOptimizeMoleculeConfs(lig_rd, numThreads=NUM_THREADS, maxIters=1000)
    #print(energies)

    conf_coords = np.stack([lig_rd.GetConformer(x).GetPositions() for x in range(lig_rd.GetNumConformers())])
    lig_ag = utils.mol_to_ag(lig_rd)
    lig_ag._setCoords(conf_coords, overwrite=True)

    clusters = _cluster_models(lig_ag.heavy.copy(), cluster_radius)
    lig_ag._setCoords(conf_coords[[x for x, _ in clusters]], overwrite=True)
    return lig_rd, lig_ag


def _make_rec_grid(rec_ag):
    grid_maker_rec = grid_maker.GridMaker(
        cell=1.25,
        padding=3,
        atom_radius=2.0,
        config=_rec_grid_config,
        fill_value=1,
        box_size=None,
        box_center=None,
        gaussians=True,
        centering='coe'
    )
    rec_grid = grid_maker_rec.make_grids(rec_ag)
    return rec_grid


def _make_lig_grid(lig_rd, lig_ag, grid_shape):
    grid_maker_lig = grid_maker.GridMaker(
        cell=1.25,
        atom_radius=2.0,
        config=partial(_mol_to_grid, lig_rd),
        fill_value=1,
        box_shape=grid_shape,
        box_center=None,
        gaussians=True,
        centering='com'
    )
    lig_grid = grid_maker_lig.make_grids(lig_ag)
    return lig_grid


def _get_ft_ag(ag, ft_results):
    ag = ag.copy()
    ag._setCoords(np.stack([x[4] for x in ft_results]), overwrite=True)
    return ag


def _calc_sym_rmsd(ref_ag, mob_ag, ref_rd):
    ref_rd = Chem.RemoveHs(ref_rd)
    matches = ref_rd.GetSubstructMatches(ref_rd, uniquify=False)
    rmsds = []
    for m in matches:
        rmsds.append(prody.calcRMSD(ref_ag.getCoords(), mob_ag.getCoordsets()[:, list(m), :]))
    rmsds = np.stack(rmsds)
    return rmsds.min(axis=0)


def dock(rec_pdb, lig_mol, rot_file, num_rots, checkpoint, device='cuda:0', num_poses=1000, num_ft_per_rotation=5, sampling_density=2.0):
    rec_ag = prody.parsePDB(rec_pdb).protein.copy()
    rec_grid = _make_rec_grid(rec_ag)
    prody.writePDB('rec.pdb', rec_ag)

    # too large for GPU
    if any([x > 50 for x in rec_grid.grid.shape[1:]]):
        return

    Path(lig_mol).copy('lig_orig.mol')
    lig_rd_orig = Chem.MolFromMolFile(lig_mol, removeHs=False)
    lig_crd_orig = lig_rd_orig.GetConformer(0).GetPositions()
    lig_rd, lig_ag = _sample_ligand(lig_rd_orig, sampling_density)
    # add the original conformation to the set
    lig_ag._setCoords(np.insert(lig_ag.getCoordsets(), 0, lig_crd_orig, axis=0), overwrite=True)
    prody.writePDB('lig_confs.pdb', lig_ag)

    rots = read_rotations(rot_file, num_rots)
    net = _load_model(checkpoint, device)

    ft_results = []
    lig_ag_cur_cset = lig_ag.copy()
    for conf_id in tqdm(range(lig_ag.numCoordsets())):
        for rot_id, rot_mat in enumerate(tqdm(rots)):
            lig_coords = lig_ag.getCoordsets()[conf_id]
            lig_coords = np.dot(lig_coords - lig_coords.mean(0), rot_mat.T) + lig_coords.mean(0)
            lig_ag_cur_cset._setCoords(lig_coords, overwrite=True)
            lig_grid = _make_lig_grid(lig_rd, lig_ag_cur_cset, rec_grid.grid.shape[1:])
            #print(subprocess.check_output('nvidia-smi').decode('utf-8'))

            min_energies, tvs = _dock_single_conf(rec_grid, lig_grid, net, device, num_ft_per_rotation)
            for min_energy, tv in zip(min_energies, tvs):
                ft_results.append((rot_id, tuple(tv), min_energy, conf_id, lig_coords + np.array(tv)))
            ft_results = sorted(ft_results, key=lambda x: x[2])[:num_poses]

    _write_ft_results('ft.000.00', ft_results)

    lig_ft_ag = _get_ft_ag(lig_ag, ft_results)
    prody.writePDB('lig_ft.pdb', lig_ft_ag)

    np.savetxt('rmsd.txt', _calc_sym_rmsd(utils.mol_to_ag(lig_rd_orig).heavy, lig_ft_ag.heavy, lig_rd))


def main():
    case = '1qrp_HH0_1_E_327__B___'
    rec_pdb = f'/home/ignatovmg/projects/ligand_benchmark/fft/data/{case}/unbound_clean.pdb'
    lig_sdf = f'/home/ignatovmg/projects/ligand_benchmark/fft/data/{case}/lig_ah.mol'
    rot_file = 'rot_sets/rot70k.0.0.6.jm.mol2'
    dock(
        rec_pdb,
        lig_sdf,
        rot_file,
        2,
        '../run/checkpoint_14.tar',
        'cuda:0',
        num_ft_per_rotation=5,
        sampling_density=2.0
    )


@contextlib.contextmanager
def cwd(path):
    oldpwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)


def run():
    rot_file = Path.getcwd() / 'rot_sets/rot70k.0.0.6.jm.mol2'
    checkpoint = Path.getcwd() / '../run/checkpoint_14.tar'
    num_ft_per_rotation = 5
    sampling_density = 2.0
    num_poses = 2000

    with open('/home/ignatovmg/projects/ligand_benchmark/fft/train_split/test.json', 'r') as f:
        cases = json.load(f)
    cases = [x for x in cases if x['natoms_heavy'] < 50]
    clusters = set()
    cases_filt = []
    for x in cases:
        if x['seqclus40'] in clusters:
            continue
        clusters.add(x['seqclus40'])
        cases_filt.append(x)
    #cases_filt = cases_filt[:50]
    print(Counter([x['seqclus40'] for x in cases_filt]))

    for case in cases_filt:
        name = case['sdf_id']
        print(name)
        wdir = (Path('cases').mkdir_p() / name).mkdir_p()
        with cwd(wdir):
            rec_pdb = f'/home/ignatovmg/projects/ligand_benchmark/fft/data/{name}/unbound_clean.pdb'
            lig_sdf = f'/home/ignatovmg/projects/ligand_benchmark/fft/data/{name}/lig_ah.mol'
            dock(rec_pdb, lig_sdf, rot_file, 200, checkpoint, 'cuda:0', num_poses=num_poses, num_ft_per_rotation=num_ft_per_rotation, sampling_density=sampling_density)


if __name__ == '__main__':
    run()
