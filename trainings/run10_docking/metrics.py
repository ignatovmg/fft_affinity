import torch
import numpy as np
from pytorch_lightning.metrics import Metric


class TopN(Metric):
    def __init__(self, topn, rmsd_cutoff=2.0, dist_sync_on_step=False, compute_on_step=True):
        super().__init__(dist_sync_on_step=dist_sync_on_step, compute_on_step=compute_on_step)

        self.topn = topn
        self.rmsd_cutoff = rmsd_cutoff
        self.add_state("correct", default=torch.tensor(0), dist_reduce_fx="sum")
        self.add_state("total", default=torch.tensor(0), dist_reduce_fx="sum")

    def update(self, preds, target):
        preds = preds.detach().flatten()
        target = target.detach().flatten()
        top_vals = torch.topk(preds, self.topn, largest=False)
        self.correct += torch.any(target[top_vals.indices] <= self.rmsd_cutoff)
        self.total += 1

    def compute(self):
        result = self.correct.float() / self.total
        return result


class RMSDCounter(Metric):
    def __init__(self, min_rmsd, max_rmsd, cell=1.0, ntop=5, radius=3):
        super().__init__(dist_sync_on_step=False, compute_on_step=True)
        self.cell = cell
        self.ntop = ntop
        self.radius = radius
        self.min_rmsd = min_rmsd
        self.max_rmsd = max_rmsd
        self.add_state("correct", default=torch.tensor(0), dist_reduce_fx="sum")
        self.add_state("total", default=torch.tensor(0), dist_reduce_fx="sum")

    def _select_ntop_rmsds(self, energy_grid, rmsd_grid):
        cor_shape = list(energy_grid.shape[-3:])
        radius_cells = self.radius / self.cell
        rmsds = []
        tvs = []
        energy_grid = energy_grid.flatten()
        for min_idx in energy_grid.argsort():
            if len(rmsds) >= self.ntop:
                break

            min_idx = min_idx.item()
            min_idx3d = np.array(np.unravel_index(min_idx, cor_shape))
            dists = [np.power(min_idx3d - x, 2).sum() for x in tvs]
            if len(dists) > 0 and any([x < radius_cells**2 for x in dists]):
                continue

            rmsds.append(rmsd_grid[tuple(min_idx3d)].item())
            tvs.append(min_idx3d)

        return rmsds

    def update(self, preds, target):
        preds = preds.detach()
        target = target.detach()
        rmsds = []
        for roti in range(preds.shape[0]):
            rmsds += self._select_ntop_rmsds(preds[roti], target[roti])

        self.correct += self.min_rmsd <= min(rmsds) <= self.max_rmsd
        self.total += 1

    def compute(self):
        result = self.correct.float() / self.total
        return result