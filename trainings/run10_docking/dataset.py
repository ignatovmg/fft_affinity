import traceback
import random
import itertools
import numpy as np
import prody
from mol_grid import GridMaker, calc_sasa
from mol_grid.loggers import logger
from path import Path
from scipy.spatial.transform import Rotation
from torch.utils.data import Dataset
from rdkit import Chem
from rdkit.Chem import AllChem

import torch

import utils_loc
from amino_acids import residue_bonds_noh, FUNCTIONAL_GROUPS
from sblu.ft import read_rotations, FTRESULT_DTYPE, read_ftresults, apply_ftresult, apply_ftresults_atom_group
import dgl

logger.setLevel('INFO')


DTYPE_FLOAT = np.float32
DTYPE_INT = np.int32

_ELEMENTS = {x[1]: x[0] for x in enumerate(['I', 'S', 'F', 'N', 'C', 'CL', 'BR', 'O', 'P', 'H'])}
_HYBRIDIZATIONS = {x: i for i, x in enumerate(Chem.rdchem.HybridizationType.names.keys())}
_FORMAL_CHARGE = {-1: 0, 0: 1, 1: 2}
_VALENCE = {x: x - 1 for x in range(1, 7)}
_NUM_HS = {x: x for x in range(5)}
_DEGREE = {x: x - 1 for x in range(1, 5)}


def _atom_to_vector(atom):
    vec = [0] * (len(_ELEMENTS) + 1)
    vec[_ELEMENTS[atom.GetSymbol().upper()]] = 1

    # total density of all atoms
    vec[-1] = 1

    # hydrogen switch
    vec += [int(atom.GetSymbol() != 'H')]

    new_vec = [0] * len(_HYBRIDIZATIONS)
    new_vec[_HYBRIDIZATIONS[str(atom.GetHybridization())]] = 1
    vec += new_vec

    new_vec = [atom.GetFormalCharge()]
    vec += new_vec

    new_vec = [0, 0]
    new_vec[int(atom.GetIsAromatic())] = 1
    vec += new_vec

    vec += [atom.GetTotalDegree()]

    vec += [atom.GetTotalNumHs()]

    new_vec = [0] * len(_VALENCE)
    new_vec[_VALENCE[atom.GetTotalValence()]] = 1
    vec += new_vec

    new_vec = [0, 0]
    new_vec[int(atom.IsInRing())] = 1
    vec += new_vec

    # Gasteiger charge
    vec += [float(atom.GetProp('_GasteigerCharge'))]

    return np.array(vec, dtype=DTYPE_FLOAT)


def _bond_to_vector(bond):
    # bond type
    vec = [0] * 4
    vec[max(min(int(bond.GetBondTypeAsDouble())-1, 3), 0)] = 1

    # is conjugated
    new_vec = [0] * 2
    new_vec[bond.GetIsConjugated()] = 1
    vec += new_vec

    # in ring
    new_vec = [0] * 2
    new_vec[bond.IsInRing()] = 1
    vec += new_vec
    return np.array(vec, dtype=DTYPE_FLOAT)


def _lig_grid_config(ag):
    return [ag[x:x+1] for x in range(len(ag))]


def _make_lig_grid(lig_ag, **box_kwargs):
    maker = GridMaker(
        config=_lig_grid_config,
        centering='com',
        **box_kwargs
    )
    return maker.make_grids(lig_ag)


def make_lig_graph(lig_rd, lig_ag, connect_all=True, self_edge=True, include_hs=True, box_kwargs={}):
    AllChem.ComputeGasteigerCharges(lig_rd, throwOnParamFailure=True)

    mol_atoms = []
    node_features = []
    for atom in lig_rd.GetAtoms():
        if not include_hs and atom.GetSymbol() == 'H':
            continue
        node_features.append(_atom_to_vector(atom))
        mol_atoms.append(atom.GetIdx())
    node_features = np.stack(node_features, axis=0).astype(DTYPE_FLOAT)

    mol_elements = np.array([lig_rd.GetAtomWithIdx(idx).GetSymbol().upper() for idx in mol_atoms])
    pdb_elements = np.array([lig_ag.getElements()[idx].upper() for idx in mol_atoms])
    assert all(mol_elements == pdb_elements), f'Elements are different:\nRDkit: {mol_elements}\nPDB  : {pdb_elements}'

    src, dst = [], []
    edge_features = []
    for i in mol_atoms:
        for j in mol_atoms:
            if i == j and not self_edge:
                continue
            edge = [mol_atoms.index(i), mol_atoms.index(j)]
            bond = lig_rd.GetBondBetweenAtoms(i, j)
            if not connect_all and bond is None:
                continue

            feat = np.zeros(9)
            feat[-1] = 1  # always set last bit to one
            if bond is not None:
                feat[:8] = _bond_to_vector(bond)

            src += edge
            dst += [edge[1], edge[0]]
            edge_features += [feat] * 2

    edge_features = np.stack(edge_features, axis=0).astype(DTYPE_FLOAT)

    grid = _make_lig_grid(lig_ag[mol_atoms].copy(), **box_kwargs)

    coords = lig_ag.getCoords()[mol_atoms, :].astype(DTYPE_FLOAT)
    G = dgl.graph((src, dst))
    G.ndata['x'] = torch.tensor(coords)
    G.ndata['f'] = torch.tensor(node_features)[..., None]
    G.ndata['grid'] = torch.tensor(grid.grid.astype(DTYPE_FLOAT))
    G.ndata['grid_origin'] = torch.tensor(np.tile(grid.origin, (coords.shape[0], 1)).astype(DTYPE_FLOAT))
    G.ndata['grid_delta'] = torch.tensor(np.tile(grid.delta, (coords.shape[0], 1)).astype(DTYPE_FLOAT))
    G.edata['d'] = torch.tensor(coords[dst] - coords[src])
    G.edata['w'] = torch.tensor(edge_features)
    return G, grid


def _rec_grid_config(ag):
    '''
    102 channels
    '''
    
    # elements
    ag = ag.stdaa.copy()
    selectors = [f'element {x}' for x in ['C', 'N', 'O', 'S', 'H']]
    
    # add sidechain atoms
    backbone = ['C', 'N', 'O', 'CA', 'OXT']
    sidechain_names = []
    for aa, x in residue_bonds_noh.items():
        for name in x.keys():
            if name not in backbone:
                sidechain_names.append((aa, name))
    selectors += [f'resname {aa} and name {name}' for aa, name in sidechain_names]
              
    # functional groups
    for gname, aas in FUNCTIONAL_GROUPS.items():
        aa_to_names = {}
        for aa, atoms in aas.items():
            names = []
            for entry in atoms:
                for x in entry:
                    names += x
            aa_to_names[aa] = names
        selectors.append(' or '.join([f'(resname {aa} and name {" ".join(names)})' for aa, names in aa_to_names.items()]))
        
    # backbone
    selectors += [f'backbone and name {x}' for x in ['C', 'N', 'CA', 'O', 'H']]
    
    return [ag.select(x) for x in selectors]


def _get_rec_grid_maker(**box_kwargs):
    return GridMaker(
        config=_rec_grid_config,
        centering='coe',
        **box_kwargs
    )


def make_protein_grids(rec_ag, **box_kwargs):
    grid_maker_rec = _get_rec_grid_maker(**box_kwargs)
    rec_grid = grid_maker_rec.make_grids(rec_ag)
    return rec_grid


def make_surface_mask(rec_ag, sasa=None, **box_kwargs):
    if sasa is None:
        sasa = calc_sasa(rec_ag, normalize=False)
    return GridMaker(atom_radius=7, config=lambda x: [x], mode='sphere', **box_kwargs).make_grids(rec_ag, weights=sasa)
    

class DockingDataset(Dataset):
    def __init__(
        self,
        dataset_dir,
        json_file,
        subset=None,
        num_conformers=4,
        rot_file='../../prms/rot70k.0.0.6.jm.mol2',
        box_size=None, #(80, 80, 80),
        cell=1.0,
        atom_radius=2.0,
        random_rotation=True,
        random_state=12345,
        shuffle_data=False
    ):

        self.dataset_dir = Path(dataset_dir).abspath()
        if not self.dataset_dir.exists():
            raise OSError(f'Directory {self.dataset_dir} does not exist')

        self.subset = subset

        self.json_file = self.dataset_dir.joinpath(json_file)
        if not self.json_file.exists():
            raise OSError(f'File {self.json_file} does not exist')

        self.data = utils_loc.read_json(self.json_file)
        if subset is not None:
            self.data = [v for k, v in enumerate(self.data) if k in subset]
        self.num_conformers = num_conformers
        
        self.random = random.Random(random_state)
        
        self.box_size = box_size
        self.cell = cell
        self.atom_radius = atom_radius
        self.random_rotation = random_rotation
        self.random_state = random_state
        
        self.rotations = read_rotations(rot_file, limit=10000)
        self.random.shuffle(self.rotations)
        
        if shuffle_data:
            self.random.shuffle(self.data)
        self._split_data_dict_to_conformers()

    def __len__(self):
        return len(self.data)

    def _split_data_dict_to_conformers(self):
        data = []
        for x in self.data:
            for conf_id in range(self.num_conformers):
                new_item = x.copy()
                new_item['conf_id'] = conf_id
                data.append(new_item)
        self.data = data

    def _rotate_ligand(self, ag, rot_mat):
        ag = ag.copy()
        coords = ag.getCoords()
        coords = np.dot(coords - coords.mean(0), rot_mat.T) + coords.mean(0)
        ag._setCoords(coords, overwrite=True)
        return ag

    #def _get_rand_rotation(self):
    #    return self.random.choice(self.rotations)

    @staticmethod
    def _split_models(ag):
        buf = ag.copy()
        buf._setCoords(buf.getCoords(), overwrite=True)
        out_ags = []
        for x in ag.getCoordsets():
            out_ags.append(buf.copy())
            out_ags[-1]._setCoords(x, overwrite=True)
        return out_ags

    def _calc_rmsd_square(self, crd, ref_crd):
        return np.power(crd - ref_crd, 2).sum(1).mean()

    def _get_rmsd_map(self, lig_ag, crys_lig_ag, rec_grid, lig_grid, interface_atoms=None, symmetries=None):
        cell = self.cell
        if interface_atoms is None:
            interface_atoms = range(len(lig_ag))

        crd = lig_ag.getCoords()[interface_atoms]
        crys_crd = crys_lig_ag.getCoords()[interface_atoms]
        
        cor_shape = np.array(rec_grid.grid.shape)[1:] + np.array(lig_grid.grid.shape)[1:]
        crd_aligned = crd + rec_grid.origin - lig_grid.origin - cell * (np.array(lig_grid.grid.shape)[1:] - 1)
        vec_grid = np.indices(cor_shape.astype(int)) * cell
        vec_grid = np.rollaxis(vec_grid, 0, 4)
        
        if symmetries is None:
            symmetries = [range(crys_crd.shape[0])]
            
        rmsd_grids = []
        for sym in symmetries:
            rmsd_aligned = self._calc_rmsd_square(crd_aligned[list(sym)], crys_crd)
            rmsd_grid = (vec_grid**2).sum(-1) + rmsd_aligned + 2 * np.dot(vec_grid, (crd_aligned - crys_crd).mean(0))
            rmsd_grids.append(rmsd_grid)
        rmsd_grid = np.stack(rmsd_grids).min(0)
        
        return np.sqrt(rmsd_grid)
    
    def _get_box_size_from_rec(self, rec_ag, padding=5.0):
        crd = rec_ag.getCoords()
        box = (crd.max(0) - crd.min(0) + padding * 2)
        box = self.cell * (box // self.cell + 1)
        return box

    def _get_box_size_from_lig(self, lig_ag, padding=5.0):
        crd = lig_ag.getCoords()
        box = (np.max(np.stack([crd.max(0) - crd.mean(0), crd.mean(0) - crd.min(0)]), 0) + padding) * 2
        box = self.cell * (box // self.cell + 1)
        return box
    
    def _get_item(self, ix):
        item = self.data[ix]
        case_dir = self.dataset_dir / 'data' / Path(item['dir']).basename()
        print(case_dir)
        rec_ag = prody.parsePDB(case_dir / 'rec_unbound_nmin.pdb')
        lig_ag = prody.parsePDB(case_dir / 'frag_crys_ah.pdb')
        lig_rd = Chem.MolFromMolFile(case_dir / 'frag_crys_ah.mol', removeHs=False)
        lig_ag_crys = lig_ag.copy()
        
        #symmetries = utils_loc.read_json(case_dir / 'symmetries.json')
        sasa = np.loadtxt(case_dir / 'sasa.txt')

        if item['conf_id'] != 0:
            ligand_rotation = self.rotations[ix % len(self.rotations)]
            lig_ag = self._rotate_ligand(lig_ag, ligand_rotation)
        else:
            ligand_rotation = np.eye(3)

        if self.random_rotation:
            complex_rotation = self.rotations[(ix // self.num_conformers) % len(self.rotations)]
            tr = prody.Transformation(complex_rotation, np.array([0, 0, 0]))
            rec_ag = tr.apply(rec_ag)
            lig_ag = tr.apply(lig_ag)
            lig_ag_crys = tr.apply(lig_ag_crys)
        
        rec_grid = make_protein_grids(rec_ag, cell=self.cell, atom_radius=self.atom_radius, padding=7.0)
        sasa_grid = make_surface_mask(rec_ag, sasa=sasa, box_shape=rec_grid.grid.shape[1:], box_origin=rec_grid.origin, cell=self.cell)
        print(rec_grid.grid.shape)
        if np.product(rec_grid.grid.shape[-3:]) > 90**3:
            raise RuntimeError('rec grid too large')

        # derive lig box size from crystal ligand so that all rotated ligand grids are the same size
        lig_box_size = self._get_box_size_from_lig(lig_ag_crys, padding=7.0)
        print(lig_box_size)
        G, lig_grid = make_lig_graph(lig_rd, lig_ag, include_hs=True, box_kwargs={'cell': self.cell, 'atom_radius': self.atom_radius, 'box_size': lig_box_size})
        
        lig_rd = Chem.RemoveHs(lig_rd)
        symmetries = lig_rd.GetSubstructMatches(lig_rd, uniquify=False)

        assert Chem.RemoveHs(lig_rd).GetNumAtoms() == len(lig_ag.heavy)
        rmsd_map = self._get_rmsd_map(lig_ag.heavy, lig_ag_crys.heavy, rec_grid, lig_grid, symmetries=symmetries)
        
        if ix % 400 == 0:
            dumpdir = Path('grids').mkdir_p()
            prody.writePDB(dumpdir / 'rec.pdb', rec_ag)
            prody.writePDB(dumpdir / 'lig.pdb', lig_ag)
            prody.writePDB(dumpdir / 'lig_crys.pdb', lig_ag_crys)
            
        #dumpdir = Path('tmp_files').mkdir_p()
        #prody.writePDB(dumpdir / f'rec_{ix}_{self.random_state}.pdb', rec_ag)
        #prody.writePDB(dumpdir / f'lig_{ix}_{self.random_state}.pdb', lig_ag)
        #prody.writePDB(dumpdir / f'lig_crys_{ix}_{self.random_state}.pdb', lig_ag_crys)
        
        #print('ix', ix)
        #print('rot id', ix % len(self.rotations))
        #print('lig', ligand_rotation.flatten())
        #print('cpx', complex_rotation.flatten())

        sample = {
            'id': ix,
            'rmsd_map': rmsd_map,
            'ligand_rotation': ligand_rotation,
            'complex_rotation': complex_rotation,
            'rec_grid': rec_grid.grid.astype(DTYPE_FLOAT),
            'rec_grid_origin': rec_grid.origin.astype(DTYPE_FLOAT),
            'rec_grid_delta': rec_grid.delta.astype(DTYPE_FLOAT),
            'sasa_grid': sasa_grid.grid.astype(DTYPE_FLOAT),
            'conf_id': item['conf_id'],
            'src': G.edges()[0],
            'dst': G.edges()[1],
            'coords': G.ndata['x'],
            'node_features': G.ndata['f'],
            'lig_grid': G.ndata['grid'],
            'lig_grid_origin': G.ndata['grid_origin'][0],
            'lig_grid_delta': G.ndata['grid_delta'][0],
            'edge_features': G.edata['w'],
            'num_nodes': G.num_nodes(),
            'num_edges': G.num_edges()
        }
        return sample
    
    def __getitem__(self, ix):
        # if there is an error, fall back to the first sample
        try:
            return self._get_item(ix)
        except Exception:
            traceback.print_exc()
            return self._get_item(0)


def main():
    ds = DockingDataset('dataset', 'train_split/debug.json', rot_file='../../prms/rot70k.0.0.6.jm.mol2')
    #print(ds[0]['affinity_class'])
    print(ds[0])


if __name__ == '__main__':
    main()

