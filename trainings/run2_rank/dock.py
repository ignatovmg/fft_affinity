from model import NetSE3
from dataset import _rec_grid_config, _mol_to_grid

from mol_grid import grid_maker, Grid
from fft_ml.training import engines
from fft_ml.loggers import logger
from fft_ml import utils
from fft_ml.training.metrics import BaseMetrics

import torch
import torch.nn.functional as F
import prody
from rdkit import Chem
from rdkit.Chem import AllChem
from functools import partial
import numpy as np

from sblu.ft import read_rotations, FTRESULT_DTYPE, read_ftresults, apply_ftresult, apply_ftresults_atom_group


def make_grids(rec_pdb, lig_mol, rot_mat):
    cell = 1.25

    rec_ag = prody.parsePDB(rec_pdb)
    grid_maker_rec = grid_maker.GridMaker(
        cell=cell,
        padding=5,
        atom_radius=2.0,
        config=_rec_grid_config,
        fill_value=1,
        box_size=None,
        box_center=None,
        gaussians=True,
        centering='coe'
    )
    rec_grid = grid_maker_rec.make_grids(rec_ag)
    prody.writePDB('rec.pdb', rec_ag)

    lig_rd = Chem.MolFromMolFile(lig_mol, removeHs=False)
    #lig_rd = AllChem.AddHs(lig_rd)
    #AllChem.MMFFOptimizeMoleculeConfs(lig_rd, numThreads=0, maxIters=200)

    grid_maker_lig = grid_maker.GridMaker(
        cell=cell,
        atom_radius=2.0,
        config=partial(_mol_to_grid, lig_rd),
        fill_value=1,
        box_shape=rec_grid.grid.shape[1:],
        box_center=None,
        gaussians=True,
        centering='com'
    )
    lig_ag = utils.mol_to_ag(lig_rd)
    prody.writePDB('lig_before.pdb', lig_ag)

    coords = lig_ag.getCoords()
    lig_ag._setCoords(np.dot(coords - coords.mean(0), rot_mat.T) + coords.mean(0), overwrite=True)
    lig_grid = grid_maker_lig.make_grids(lig_ag)

    prody.writePDB('lig_prep.pdb', lig_ag)
    return rec_grid, lig_grid


def load_model(checkpoint, device):
    model = NetSE3(95, 52, 64, 32, 32,
                   kernel_size=5,
                   n_middle_layers=10,
                   dense_depth=2,
                   middle_activation=F.relu,
                   final_activation=F.tanh)
    engines.load_checkpoint(checkpoint, device, model=model)
    model.to(device)
    model.eval()
    return model


def _grid_to_tensor(grid, device):
    return torch.tensor(grid.astype(np.float32), device=device)[None, :]


def dock_single_rot(rec_pdb, lig_mol, rot_mat, net, device, ntop=1):
    rec, lig = make_grids(rec_pdb, lig_mol, rot_mat)
    energy_grid, _, cor_grid = net(_grid_to_tensor(rec.grid, device), _grid_to_tensor(lig.grid, device))

    energy_grid = energy_grid[0]
    cor_shape = list(cor_grid.shape[2:])

    gc = rec.get_center() - np.array(cor_shape) / 2 * 1.25
    Grid(energy_grid.cpu().detach().numpy().reshape(cor_shape)[None, :], gc, np.array([1.25, 1.25, 1.25])).save('cor.grid')

    energies = []
    tvs = []
    s = energy_grid.sort()[0]
    print(s)
    print('asdf', s[0], s[1000], s[2000], s[5000], s[10000], s[100000])

    for min_idx in energy_grid.argsort()[:ntop]:
        min_idx = min_idx.item()
        min_energy = energy_grid[min_idx]
        min_idx3d = np.unravel_index(min_idx, cor_shape)
        cor_shift = (np.array(min_idx3d) - np.array(rec.grid.shape[1:])) * lig.delta
        tv = rec.origin - lig.origin + cor_shift

        energies.append(min_energy)
        tvs.append(tv)

    return energies, tvs


def _write_ft_results(fname, ft_results):
    with open(fname, 'w') as f:
        for x in ft_results:
            f.write(f'{x[0]:<4d} {x[1][0]:<8f} {x[1][1]:<8f} {x[1][2]:<8f} {x[2]:<8f}\n')


def _apply_ft_to_mol(lig_mol, rots):
    lig_rd = Chem.MolFromMolFile(lig_mol, removeHs=False)
    lig_ag = utils.mol_to_ag(lig_rd)

    ft_results = read_ftresults('ft.000.00')
    ft_ag = apply_ftresults_atom_group(lig_ag, ft_results, rots)
    prody.writePDB('lig_ft.pdb', ft_ag)
    #np.array(ft_results, dtype=FTRESULT_DTYPE)


def dock(rec_pdb, lig_mol, rot_file, num_rots, checkpoint, device='cuda:0'):
    rots = read_rotations(rot_file, num_rots)
    net = load_model(checkpoint, device)

    ft_results = []
    for roti, rot_mat in enumerate(rots):
        min_energies, tvs = dock_single_rot(rec_pdb, lig_mol, rot_mat, net, device, 1000)
        for min_energy, tv in zip(min_energies, tvs):
            ft_results.append((roti, tuple(tv), min_energy))
    #ft_results = np.array(ft_results, dtype=FTRESULT_DTYPE)

    sorted(ft_results, key=lambda x: x[2])
    _write_ft_results('ft.000.00', ft_results)

    _apply_ft_to_mol(lig_mol, rots)


def main():
    rec_pdb = '/home/ignatovmg/projects/ligand_benchmark/fft/data/4avh_FK9_1_A_201__C___/unbound_clean.pdb'
    lig_sdf = '/home/ignatovmg/projects/ligand_benchmark/fft/data/4avh_FK9_1_A_201__C___/lig_ah.mol'
    rot_file = 'norot' # 'rot70k.0.0.6.jm.mol2'
    dock(rec_pdb, lig_sdf, rot_file, 1, '../../run3_sumloss/run/checkpoint_68.tar')


if __name__ == '__main__':
    main()