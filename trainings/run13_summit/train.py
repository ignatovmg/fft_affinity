import logging
import sys
import numpy as np
import math
from path import Path
from tqdm import tqdm
import torch
import torch.optim as optim
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.tensorboard import SummaryWriter

from dataset import DockingDataset
from model import NetSE3

SRC_DIR = Path(__file__).dirname().abspath()
log_dir = Path('.').mkdir_p().abspath()
global_step = 0
device = 'cuda:0'

NUM_CONFORMERS = 3
RMSD_NEAR_NATIVE = 1.0
LR = 5e-5
SCHEDULER_PATIENCE = 2
SCHEDULER_FACTOR = 1. / 3
SCHEDULER_MIN_LR = 1e-8
RANK_LOSS_MARGIN = 100
NUM_PAIRS = None # int(3e+7)
CLIP_GRADIENT = False
CLIP_GRADIENT_VALUE = 1.0
USE_AMP = False

HOROVOD = True
HOROVOD_RANK = 0
if HOROVOD:
    import horovod.torch as hvd
    if __name__ == '__main__':
        hvd.init()
        HOROVOD_RANK = hvd.rank()
        assert hvd.size() % NUM_CONFORMERS == 0


def report_epoch_end(epoch, global_stats, stage='Train', save_model=True):
    if HOROVOD_RANK == 0:
        global global_step
        writer.add_scalar('HasNans/Epoch/' + stage, math.isnan(sum(global_stats['Loss'])), epoch)
        for key in global_stats.keys():
            vals = [x for x in global_stats[key] if not math.isnan(x)]
            global_stats[key] = sum(vals) / len(vals)
            writer.add_scalar(key + '/Epoch/' + stage, global_stats[key], epoch)
        writer.add_scalar('LearningRate/Epoch/' + stage, optimizer.param_groups[0]['lr'], epoch)
        if save_model:
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'scheduler_state_dict': scheduler.state_dict(),
                'loss': global_stats,
                'global_step': global_step,
            }, log_dir / f'epoch_{epoch}_loss_{global_stats["Loss"]:.3f}.pth')
        print('Epoch_stats', global_stats)


def _select_ntop_rmsds(energy_grid, rmsd_grid, ntop, radius, cell):
    cor_shape = list(energy_grid.shape[-3:])
    radius_cells = radius / cell
    rmsds = []
    tvs = []
    energy_grid = energy_grid.flatten()
    for min_idx in energy_grid.argsort():
        if len(rmsds) >= ntop:
            break

        min_idx = min_idx.item()
        min_idx3d = np.array(np.unravel_index(min_idx, cor_shape))
        dists = [np.power(min_idx3d - x, 2).sum() for x in tvs]
        if len(dists) > 0 and any([x < radius_cells**2 for x in dists]):
            continue

        rmsds.append(rmsd_grid[tuple(min_idx3d)].item())
        tvs.append(min_idx3d)
    return rmsds


def topn_rmsd_translation_clustered(energy_grids, rmsd_grids, ntop, clus_radius, cell_size):
    rmsds = []
    for roti in range(energy_grids.shape[0]):
        rmsds.append(_select_ntop_rmsds(energy_grids[roti], rmsd_grids[roti], ntop, clus_radius, cell_size))
    return torch.tensor(rmsds, dtype=energy_grids.dtype, device=energy_grids.device)


def report_step(global_stats, loss, num_pairs, energy_grids, rmsd_maps, train=True, stage='Train'):
    energy_grids = energy_grids.detach()
    rmsd_maps = rmsd_maps.detach()

    stats = {}
    loss = loss.item()
    if HOROVOD:
        zero_conf_rank = (HOROVOD_RANK // NUM_CONFORMERS) * NUM_CONFORMERS
        all_loss = hvd.allgather_object(loss)[zero_conf_rank:zero_conf_rank+NUM_CONFORMERS]
        all_num_pairs = hvd.allgather_object(num_pairs)[zero_conf_rank:zero_conf_rank+NUM_CONFORMERS]
        loss = sum([all_loss[i] * all_num_pairs[i] for i in range(len(all_loss))]) / sum(all_num_pairs)
    stats['Loss'] = loss

    indices = torch.topk(energy_grids.flatten(start_dim=1), 1000, largest=False).indices
    top_rmsds = torch.gather(rmsd_maps.flatten(start_dim=1), -1, indices)
    indices_flat = torch.topk(energy_grids.flatten(), 1000, largest=False).indices
    top_rmsds_flat = torch.gather(rmsd_maps.flatten(), -1, indices_flat)
    for n in [1, 100, 1000]:
        for cutoff in [1, 2, 4]:
            stats[f'Top{n}/PerRot_HasUnder{cutoff}A'] = torch.any(top_rmsds[:, :n] <= cutoff).item()
            stats[f'Top{n}/AllRot_HasUnder{cutoff}A'] = torch.any(top_rmsds_flat[:n] <= cutoff).item()

    if not train:
        top_clustered = topn_rmsd_translation_clustered(energy_grids, rmsd_maps, 5, 5, 1)
        for n in range(1, 6):
            for cutoff in [1, 2, 4]:
                stats[f'Clustered/Top{n}_HasUnder{cutoff}A'] = torch.any(top_clustered[:, :n] <= cutoff).item()

    if HOROVOD:
        all_stats = hvd.allgather_object(stats)
    else:
        all_stats = [stats]

    if HOROVOD_RANK == 0:
        print('Energy grid (min, median, max):', energy_grids[0].min(), energy_grids[0].median(), energy_grids[0].max())

        for idx, case_stats in enumerate(all_stats[::NUM_CONFORMERS]):
            for key, val in case_stats.items():
                if key not in global_stats:
                    global_stats[key] = []
                global_stats[key].append(val)

            if train:
                key = 'Loss'
                writer.add_scalar(key + '/Step/' + stage, case_stats[key], global_step + idx)

        for x in range(torch.cuda.device_count()):
            print('cuda:' + str(x), ':', torch.cuda.memory_stats(x)['allocated_bytes.all.peak'] / 1024**2)
        print('global step', global_step)
        print(stats)
        sys.stdout.flush()
    return all_stats


def validate(epoch, valid_json='train_split/fragments/valid.json', name='Valid'):
    model.eval()
    dset = DockingDataset(
        SRC_DIR / 'dataset',
        valid_json,
        rot_file=SRC_DIR / '../../prms/rot70k.0.0.6.jm.mol2',
        num_conformers=NUM_CONFORMERS,
        sample_to_size=None,
        random_state=(epoch+1)*100 + HOROVOD_RANK // NUM_CONFORMERS,
        conf_id=HOROVOD_RANK % NUM_CONFORMERS
        #subset=range(5)
    )
    if HOROVOD:
        sampler = torch.utils.data.distributed.DistributedSampler(dset, num_replicas=hvd.size() // NUM_CONFORMERS, rank=HOROVOD_RANK // NUM_CONFORMERS, shuffle=False)
        loader = torch.utils.data.DataLoader(dset, batch_size=1, shuffle=False, sampler=sampler, **kwargs)
        sampler.set_epoch(epoch)
    else:
        loader = torch.utils.data.DataLoader(dset, batch_size=1, shuffle=False, **kwargs)

    global_stats = {}
    local_step = 0

    for inputs in (tqdm(loader, desc=f'Epoch {epoch} (valid)') if HOROVOD_RANK == 0 else loader):
        inputs = {k: v.to(device) for k, v in inputs.items()}
        sys.stdout.flush()

        with torch.no_grad():
            with torch.cuda.amp.autocast(USE_AMP):
                energy_grids = model(inputs)
                rmsd_maps = inputs['rmsd_map']

                if HOROVOD:
                    zero_conf_rank = (HOROVOD_RANK // NUM_CONFORMERS) * NUM_CONFORMERS
                    energy_grids = hvd.allgather(energy_grids)[zero_conf_rank:zero_conf_rank+NUM_CONFORMERS]
                    rmsd_maps = hvd.allgather(rmsd_maps)[zero_conf_rank:zero_conf_rank+NUM_CONFORMERS]

                loss, num_pairs = loss_fn(energy_grids, rmsd_maps)

        all_stats = report_step(global_stats, loss, num_pairs, energy_grids, rmsd_maps, train=False, stage=name)
        step_size = len(all_stats) // NUM_CONFORMERS
        local_step += step_size

        torch.cuda.empty_cache()

    report_epoch_end(epoch, global_stats, stage=name, save_model=False)
    sys.stdout.flush()


def train(epoch):
    model.train()
    dset = DockingDataset(
        SRC_DIR / 'dataset',
        'train_split/fragments/1673_train_nonhom_rmsdcut4.json',
        rot_file=SRC_DIR / '../../prms/rot70k.0.0.6.jm.mol2',
        num_conformers=NUM_CONFORMERS,
        sample_to_size=1000,
        random_state=(epoch+1)*100 + HOROVOD_RANK // NUM_CONFORMERS,
        conf_id=HOROVOD_RANK % NUM_CONFORMERS
    )

    if HOROVOD:
        sampler = torch.utils.data.distributed.DistributedSampler(dset, num_replicas=hvd.size() // NUM_CONFORMERS, rank=HOROVOD_RANK // NUM_CONFORMERS, shuffle=False)
        loader = torch.utils.data.DataLoader(dset, batch_size=1, shuffle=False, sampler=sampler, **kwargs)
        sampler.set_epoch(epoch)
    else:
        loader = torch.utils.data.DataLoader(dset, batch_size=1, shuffle=False, **kwargs)

    global_stats = {}
    local_step = 0
    global global_step

    for inputs in (tqdm(loader, desc=f'Epoch {epoch} (train)') if HOROVOD_RANK == 0 else loader):
        inputs = {k: v.to(device) for k, v in inputs.items()}
        sys.stdout.flush()

        optimizer.zero_grad()
        with torch.cuda.amp.autocast(USE_AMP):
            energy_grids = model(inputs)
            rmsd_maps = inputs['rmsd_map']

            if HOROVOD:
                zero_conf_rank = (HOROVOD_RANK // NUM_CONFORMERS) * NUM_CONFORMERS
                energy_grids = hvd.allgather(energy_grids)[zero_conf_rank:zero_conf_rank+NUM_CONFORMERS]
                rmsd_maps = hvd.allgather(rmsd_maps)[zero_conf_rank:zero_conf_rank+NUM_CONFORMERS]

            loss, num_pairs = loss_fn(energy_grids, rmsd_maps)
            if USE_AMP:
                amp_scaler.scale(loss).backward()
            else:
                loss.backward()

        if HOROVOD:
            optimizer.synchronize()
            if USE_AMP and CLIP_GRADIENT:
                amp_scaler.unscale_(optimizer)
            if CLIP_GRADIENT:
                torch.nn.utils.clip_grad_norm_(model.parameters(), CLIP_GRADIENT_VALUE)
            with optimizer.skip_synchronize():
                if USE_AMP:
                    amp_scaler.step(optimizer)
                else:
                    optimizer.step()
        else:
            if USE_AMP and CLIP_GRADIENT:
                amp_scaler.unscale_(optimizer)
            if CLIP_GRADIENT:
                torch.nn.utils.clip_grad_norm_(model.parameters(), CLIP_GRADIENT_VALUE)

            if USE_AMP:
                amp_scaler.step(optimizer)
            else:
                optimizer.step()

        if USE_AMP:
            amp_scaler.update()

        all_stats = report_step(global_stats, loss, num_pairs, energy_grids, rmsd_maps, train=True, stage='Train')
        step_size = len(all_stats) // NUM_CONFORMERS
        local_step += step_size
        global_step += step_size

        torch.cuda.empty_cache()

    report_epoch_end(epoch, global_stats, stage='Train', save_model=True)
    if HOROVOD:
        global_stats = hvd.broadcast_object(global_stats, root_rank=0)
    scheduler.step(global_stats['Loss'], epoch=epoch)
    sys.stdout.flush()


def get_ranking_loss(num_decoys=None, num_true=None, num_pairs=None, nn_rmsd=1.0, upsample_within_radius=None, upsample_factor: int=10, hvd_use_only_local=True):
    rank_loss = torch.nn.MarginRankingLoss(margin=RANK_LOSS_MARGIN)

    def loss_fn(y_pred, rmsd_map):
        rmsd_map = rmsd_map.flatten()
        nn_rmsd_loc = nn_rmsd
        if (rmsd_map <= nn_rmsd_loc).sum() == 0:  # -> a horrible crutch
            nn_rmsd_loc = nn_rmsd + 0.5
        y_true = (rmsd_map > nn_rmsd_loc) #.type(y_pred.dtype)
        y_true_flat = y_true
        y_pred_flat = y_pred.flatten()

        # select ~num_decoys decoys with label != 0
        frac = min(num_decoys / y_pred_flat.shape[0], 1.0) if num_decoys is not None else 1.0
        mask_0 = y_true_flat == 0
        mask = (torch.rand_like(y_pred_flat) < frac) * (~mask_0)
        # zeros are false, ones are partially true

        # randomly select num_true decoys with label 0
        num_0 = mask_0.sum()
        assert num_0 > 0, 'No near-native samples are present'
        if num_true is not None and num_0 > num_true:
            exclude_ids = torch.randperm(num_0 - num_true)
            mask_0[mask_0.nonzero()[exclude_ids]] = False
        mask[mask_0] = True

        ids_to_use = torch.nonzero(mask)[:, 0]
        #print('selected samples:', len(ids_to_use))

        if upsample_within_radius is not None:
            reweight_ids = torch.nonzero(mask * (rmsd_map <= upsample_within_radius) * (rmsd_map > nn_rmsd_loc))[:, 0]
            #print('reweighted samples:', len(reweight_ids))
            ids_to_use = torch.cat([ids_to_use] + [reweight_ids] * (upsample_factor - 1))

        y_pred_flat = y_pred_flat[ids_to_use]
        y_true_flat = y_true_flat[ids_to_use]
        pairs = torch.cartesian_prod(y_pred_flat[y_true_flat == 0], y_pred_flat[y_true_flat == 1])

        # use only pairs which contain energies calculated by the current rank
        if hvd_use_only_local:
            ranks = torch.zeros_like(y_pred)
            ranks[HOROVOD_RANK % NUM_CONFORMERS] = 1
            ranks = ranks.flatten()[ids_to_use]
            pair_is_local = torch.cartesian_prod(ranks[y_true_flat == 0], ranks[y_true_flat == 1]).sum(-1)
            pairs = pairs[pair_is_local > 0]
            #y_pred_flat = y_pred_flat[ranks > 0]
            #y_true_flat = y_true_flat[ranks > 0]

        if num_pairs is not None:
            # select num_pairs at random
            pairs = pairs[torch.randperm(pairs.shape[0], device=pairs.device)][:num_pairs]

        #print('pairs', pairs.shape)
        #print('y_pred_batch', y_pred_flat.shape)
        #print('num nn', (y_true_flat == 0).sum())

        labels = -torch.ones(pairs.shape[0], dtype=torch.int, device=pairs.device)
        return rank_loss(pairs[:, 0], pairs[:, 1], labels), pairs.shape[0]

    return loss_fn


def find_last_pth(dir):
    pths = Path(dir).glob('epoch_*.pth')
    if len(pths) == 0:
        return None
    return sorted(pths, key=lambda x: -int(x.basename().split('_')[1]))[0]


if __name__ == '__main__':
    torch.set_num_threads(1)
    torch.manual_seed(123456)
    torch.cuda.manual_seed(123456)
    logging.getLogger('.prody').setLevel('CRITICAL')

    #print(HOROVOD_RANK, ': 1')
    #sys.stdout.flush()

    kwargs = {'num_workers': 0, 'pin_memory': True}
    lr = LR
    model = NetSE3().to(device)

    #print(HOROVOD_RANK, ': 2')
    if HOROVOD_RANK == 0:
        print('Num params:', sum(p.numel() for p in model.parameters() if p.requires_grad))
        print('Num param sets:', len([p for p in model.parameters() if p.requires_grad]))
        for x in range(torch.cuda.device_count()):
            print('cuda:' + str(x), ':', torch.cuda.memory_stats(x)['allocated_bytes.all.peak'] / 1024**2)
        sys.stdout.flush()

    optimizer = optim.Adam(model.parameters(), lr=lr)
    scheduler = ReduceLROnPlateau(optimizer, factor=SCHEDULER_FACTOR, patience=SCHEDULER_PATIENCE, min_lr=SCHEDULER_MIN_LR)

    #print(HOROVOD_RANK, ': 3')
    #sys.stdout.flush()

    loss_fn = get_ranking_loss(num_true=50, num_pairs=NUM_PAIRS, upsample_within_radius=7.0, upsample_factor=20, nn_rmsd=RMSD_NEAR_NATIVE)

    start_epoch = 1
    scheduler_state = scheduler.state_dict()
    pth_file = find_last_pth(log_dir)
    if pth_file is not None and HOROVOD_RANK == 0:
        pth = torch.load(pth_file)
        print('Loading saved model from', pth_file)
        global_step = pth['global_step']
        start_epoch = pth['epoch'] + 1
        model.load_state_dict(pth['model_state_dict'])
        optimizer.load_state_dict(pth['optimizer_state_dict'])
        scheduler_state = pth['scheduler_state_dict']

    if HOROVOD:
        hvd.broadcast_parameters(model.state_dict(), root_rank=0)

        #print(HOROVOD_RANK, ': 4')
        #sys.stdout.flush()

        hvd.broadcast_optimizer_state(optimizer, root_rank=0)

        #print(HOROVOD_RANK, ': 5')
        #sys.stdout.flush()

        scheduler_state = hvd.broadcast_object(scheduler_state, root_rank=0)

        optimizer = hvd.DistributedOptimizer(optimizer, named_parameters=model.named_parameters(), op=hvd.Average)

        #print(HOROVOD_RANK, ': 6')
        #sys.stdout.flush()

    scheduler.load_state_dict(scheduler_state)

    if USE_AMP:
        amp_scaler = torch.cuda.amp.GradScaler()

    if HOROVOD_RANK == 0:
        writer = SummaryWriter(log_dir)

    for epoch in range(start_epoch, 100):
        train(epoch)
        validate(epoch)
