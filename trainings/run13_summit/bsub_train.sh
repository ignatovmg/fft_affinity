#!/bin/bash
#BSUB -P BIP215
#BSUB -W 2:00
#BSUB -nnodes 20
#?SUB -q debug
#?SUB -J RunSim123
#?SUB -o RunSim123.%J
#?SUB -e RunSim123.%J

source /ccs/home/ignatovmg/projects/fft_affinity/activate_summit.sh

wdir=/ccs/home/ignatovmg/projects/fft_affinity/trainings/run13_summit/runs/run1
mkdir -p $wdir && cd $wdir
cp /ccs/home/ignatovmg/projects/fft_affinity/trainings/run13_summit/train.py .
jsrun -n $((6 * 20)) -a 1 -g 1 -c 4 python /ccs/home/ignatovmg/projects/fft_affinity/trainings/run13_summit/train.py
#jsrun -n 2 -r 1 -a 1 -g 6 -c 4 python /ccs/home/ignatovmg/projects/alphadock/alphadock/train.py