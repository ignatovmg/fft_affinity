import traceback
import itertools
import numpy as np
import prody
from mol_grid import GridMaker
from mol_grid.loggers import logger
from path import Path
from torch.utils.data import Dataset
from functools import partial
from sblu.ft import read_rotations
import torch

import utils_loc
from amino_acids import residue_bonds_noh, FUNCTIONAL_GROUPS, ATOM_TYPES
import rec_feats

logger.setLevel('INFO')


DTYPE_FLOAT = np.float32
DTYPE_INT = np.int32

ELEMENTS = {x[1]: x[0] for x in enumerate(['I', 'S', 'F', 'N', 'C', 'CL', 'BR', 'O', 'P'])}

HYBRIDIZATIONS = {
    'S': 0,
    'SP': 1,
    'SP2': 2,
    'SP3': 3,
    'SP3D': 4,
    'SP3D2': 5
}

MAX_VALENCE = 7

MAX_HS = 3

MAX_DEGREE = 5

CHIRALITY = {
    'CHI_TETRAHEDRAL_CW': 0,
    'CHI_TETRAHEDRAL_CCW': 1
}

BOND_TYPE = {'AROMATIC': 0, 'SINGLE': 1, 'DOUBLE': 2, 'TRIPLE': 3}

FRAG_DIST_MIN = 1
FRAG_DIST_MAX = 20
FRAG_DIST_BIN = 0.5

REC_DIST_MIN = 1
REC_DIST_MAX = 40
REC_DIST_BIN = 0.5


def atom_to_vector(atom:dict):
    vec = [0] * len(ELEMENTS)
    vec[ELEMENTS[atom['GetSymbol'].upper()]] = 1

    # chirality
    #new_vec = [0] * (len(CHIRALITY) + 1)
    #new_vec[CHIRALITY.get(str(atom.GetChiralTag()), len(CHIRALITY))] = 1
    #vec += new_vec

    # formal charge
    fcharge = atom['GetFormalCharge']
    new_vec = [0] * 3
    if fcharge < 0:
        new_vec[0] = 1
    elif fcharge > 0:
        new_vec[1] = 1
    else:
        new_vec[2] = 1
    vec += new_vec

    # aromaticity
    new_vec = [0, 0]
    new_vec[int(atom['GetIsAromatic'])] = 1
    vec += new_vec

    # degree
    new_vec = [0] * (MAX_DEGREE + 1)
    new_vec[int(min(atom['GetTotalDegree'], MAX_DEGREE))] = 1
    vec += new_vec

    # num Hs
    new_vec = [0] * (MAX_HS + 1)
    new_vec[int(min(atom['GetTotalNumHs'], MAX_HS))] = 1
    vec += new_vec

    # valence
    new_vec = [0] * (MAX_VALENCE + 1)
    new_vec[int(min(atom['GetTotalValence'], MAX_VALENCE))] = 1
    vec += new_vec

    # in ring flag
    new_vec = [0, 0]
    new_vec[int(atom['IsInRing'])] = 1
    vec += new_vec

    return np.array(vec, dtype=DTYPE_FLOAT)


def bond_to_vector(bond):
    # bond type
    vec = [0] * len(BOND_TYPE)
    vec[BOND_TYPE[bond['GetBondType']]] = 1

    # in ring
    new_vec = [0] * 2
    new_vec[bond['IsInRing']] = 1
    vec += new_vec
    return np.array(vec, dtype=DTYPE_FLOAT)


def dist_to_bins(dist, dmin, dmax, dbin):
    nbins = int((dmax - dmin) // dbin + 1)
    out = np.zeros(nbins)
    out[min(int(max(0, dist - dmin) // dbin), len(out)-1)] = 1
    return out


def _lig_grid_config(ag, atom_ids=None):
    if atom_ids is None:
        atom_ids = range(len(ag))
    return [ag[x:x+1] for x in atom_ids]


def _make_lig_grid(lig_ag, atom_ids=None, **box_kwargs):
    maker = GridMaker(
        config=partial(_lig_grid_config, atom_ids=atom_ids),
        centering='com',
        **box_kwargs
    )
    return maker.make_grids(lig_ag)


def make_lig_graph(lig_dict, lig_ag, connect_all=True, self_edge=True, box_kwargs={}):
    mol_atoms = []
    node_features = []
    assert len(lig_dict['atoms']) == len(lig_ag)

    for idx, atom in enumerate(lig_dict['atoms']):
        if atom['GetSymbol'] == 'H':
            logger.warning(f'Ligand contains hydrogens (atom {idx}: make sure to run Chem.RemoveHs(mol)')
            continue
        node_features.append(atom_to_vector(atom))
        mol_atoms.append(atom['GetIdx'])
    node_features = np.stack(node_features, axis=0).astype(DTYPE_FLOAT)

    mol_elements = np.array([lig_dict['atoms'][idx]['GetSymbol'].upper() for idx in mol_atoms])
    pdb_elements = np.array([lig_ag.getElements()[idx].upper() for idx in mol_atoms])
    assert all(mol_elements == pdb_elements), f'Elements are different:\nRDkit: {mol_elements}\nPDB  : {pdb_elements}'

    coords = lig_ag.getCoords()[mol_atoms, :].astype(DTYPE_FLOAT)

    src, dst = [], []
    edge_features = []
    pairwise = np.zeros((len(mol_atoms), len(mol_atoms), 45), dtype=DTYPE_FLOAT)
    bonds_dict = {}
    for bond in lig_dict['bonds']:
        bonds_dict[(bond['GetBeginAtomIdx'], bond['GetEndAtomIdx'])] = bond
        bonds_dict[(bond['GetEndAtomIdx'], bond['GetBeginAtomIdx'])] = bond

    for i in mol_atoms:
        for j in mol_atoms:
            if i == j and not self_edge:
                continue
            edge = [mol_atoms.index(i), mol_atoms.index(j)]
            bond = bonds_dict.get((i, j))
            if not connect_all and bond is None:
                continue

            bond_feat = np.zeros(6)
            if bond is not None:
                bond_feat = bond_to_vector(bond)

            dist = np.sqrt(np.square(coords[edge[0]] - coords[edge[1]]).sum())
            dist_binned = dist_to_bins(dist, FRAG_DIST_MIN, FRAG_DIST_MAX, FRAG_DIST_BIN)

            src += edge
            dst += [edge[1], edge[0]]
            feats = np.concatenate([bond_feat, dist_binned])
            edge_features += [feats] * 2
            pairwise[i, j] = feats

    edge_features = np.stack(edge_features, axis=0).astype(DTYPE_FLOAT)
    
    lig_com = lig_ag.getCoords().mean(0)
    if 'box_center' not in box_kwargs:
        box_kwargs['box_center'] = lig_com
    grid = _make_lig_grid(lig_ag, atom_ids=mol_atoms, **box_kwargs)

    graph = {}
    graph['src'] = torch.tensor(src)
    graph['dst'] = torch.tensor(dst)
    graph['x'] = torch.tensor(coords)
    graph['f'] = torch.tensor(node_features)[..., None]
    graph['grid'] = torch.tensor(grid.grid.astype(DTYPE_FLOAT))
    graph['grid_origin'] = torch.tensor(np.tile(grid.origin, (coords.shape[0], 1)).astype(DTYPE_FLOAT))
    graph['grid_delta'] = torch.tensor(np.tile(grid.delta, (coords.shape[0], 1)).astype(DTYPE_FLOAT))
    graph['d'] = torch.tensor(coords[dst] - coords[src])
    graph['w'] = torch.tensor(edge_features)
    graph['atom_ids'] = torch.tensor(mol_atoms)
    return graph, grid, pairwise


def _rec_grid_config(ag):
    '''
    102 channels
    '''
    
    # elements
    ag = ag.stdaa.copy()
    selectors = [f'element {x}' for x in ['C', 'N', 'O', 'S', 'H']]
    
    # add sidechain atoms
    backbone = ['C', 'N', 'O', 'CA', 'OXT']
    sidechain_names = []
    for aa, x in residue_bonds_noh.items():
        for name in x.keys():
            if name not in backbone:
                sidechain_names.append((aa, name))
    selectors += [f'resname {aa} and name {name}' for aa, name in sidechain_names]
              
    # functional groups
    #for gname, aas in FUNCTIONAL_GROUPS.items():
    #    aa_to_names = {}
    #    for aa, atoms in aas.items():
    #        names = []
    #        for entry in atoms:
    #            for x in entry:
    #                names += x
    #        aa_to_names[aa] = names
    #    selectors.append(' or '.join([f'(resname {aa} and name {" ".join(names)})' for aa, names in aa_to_names.items()]))
        
    # backbone
    selectors += [f'backbone and name {x}' for x in ['C', 'N', 'CA', 'O', 'H']]
    
    return [ag.select(x) for x in selectors]


def _rec_grid_config_nofgroups(ag):
    '''
    102 channels
    '''

    # elements
    ag = ag.stdaa.copy()
    selectors = [f'element {x}' for x in ['C', 'N', 'O', 'S', 'H']]

    # add sidechain atoms
    backbone = ['C', 'N', 'O', 'CA', 'OXT']
    sidechain_names = []
    for aa, x in residue_bonds_noh.items():
        for name in x.keys():
            if name not in backbone:
                sidechain_names.append((aa, name))
    selectors += [f'resname {aa} and name {name}' for aa, name in sidechain_names]

    # backbone
    selectors += [f'backbone and name {x}' for x in ['C', 'N', 'CA', 'O', 'H']]

    # donors
    donors = []
    acceptors = []
    for resi, atoms in ATOM_TYPES.items():
        for atom, props in atoms.items():
            if props['donor']:
                donors.append((resi, atom))
            if props['acceptor']:
                acceptors.append((resi, atom))
    selectors.append(' or '.join([f'(resname {resi} and name {atom})' for resi, atom in donors]))
    selectors.append(' or '.join([f'(resname {resi} and name {atom})' for resi, atom in acceptors]))

    return [ag.select(x) for x in selectors]


def make_protein_grids(rec_ag, **box_kwargs):
    grid_maker_rec = GridMaker(config=_rec_grid_config, centering='coe', **box_kwargs)
    rec_grid = grid_maker_rec.make_grids(rec_ag)
    return rec_grid


def _rec_atom_types(ag):
    pair_to_id = {}
    cur_id = 0
    for resi, atoms in ATOM_TYPES.items():
        for atom, props in atoms.items():
            pair_to_id[(resi, atom)] = cur_id
            cur_id += 1

    types = []
    for resi, atom in zip(ag.getResnames(), ag.getNames()):
        types.append(pair_to_id[(resi, atom)])
    return types


def _rec_grid_config_new(ag):
    atom_types = ATOM_TYPES
    ag_atoms = set(zip(ag.getResnames(), ag.getNames()))

    # Individual atom types (203)
    selectors = []
    atom_list = []
    donors = []
    acceptors = []
    for resi, atoms in atom_types.items():
        for atom, props in atoms.items():
            atom_list.append((resi, atom))
            if props['donor']:
                donors.append((resi, atom))
            if props['acceptor']:
                acceptors.append((resi, atom))

            if atom.startswith('H') or atom in ['C', 'CA', 'N', 'O', 'H']:
                continue
            selectors.append(ag.select(f'resname {resi} and name {atom}'))

    atom_list = set(atom_list)
    assert ag_atoms.issubset(atom_list), f'Receptor has unknown atoms: {ag_atoms - atom_list}'

    # functional groups (7)
    for gname, aas in FUNCTIONAL_GROUPS.items():
        aa_to_names = {}
        for aa, atoms in aas.items():
            names = []
            for entry in atoms:
                for x in entry:
                    names += x
            aa_to_names[aa] = names
        selectors.append(ag.select(' or '.join([f'(resname {aa} and name {" ".join(names)})' for aa, names in aa_to_names.items()])))

    # backbone (5)
    selectors += [ag.select(f'name {x}') for x in ['C', 'CA', 'N', 'O', 'H']]

    # elements (5)
    selectors += [ag.select(f'element {x}') for x in ['C', 'N', 'O', 'S', 'H']]

    # donor / acc (2)
    selectors.append(ag.select(' or '.join([f'(resname {resi} and name {atom})' for resi, atom in donors])))
    selectors.append(ag.select(' or '.join([f'(resname {resi} and name {atom})' for resi, atom in acceptors])))

    return selectors


def _get_rec_charges(ag):
    charges = []
    for resi, atom in zip(ag.getResnames(), ag.getNames()):
        charges.append(ATOM_TYPES[resi][atom]['charge'])
    return charges


def make_protein_grids_new(rec_ag, **box_kwargs):
    grid_maker_rec = GridMaker(config=_rec_grid_config_new, centering='coe', **box_kwargs)
    rec_grid = grid_maker_rec.make_grids(rec_ag)

    # add a charge grid, based on charges in topology file
    charges = _get_rec_charges(rec_ag)
    charge_grid_maker = GridMaker(config=lambda x: [x.all], centering='coe', **box_kwargs)
    charge_grid = charge_grid_maker.make_grids(rec_ag, weights=charges)
    assert np.all(charge_grid.origin == rec_grid.origin)
    rec_grid.grid = np.concatenate([rec_grid.grid, charge_grid.grid])

    return rec_grid


def make_surface_mask(rec_ag, sasa=None, **box_kwargs):
    if sasa is None:
        sasa = calc_sasa(rec_ag, normalize=False)
    return GridMaker(atom_radius=7, config=lambda x: [x], mode='sphere', **box_kwargs).make_grids(rec_ag, weights=sasa)


class DockingDataset(Dataset):
    def __init__(
        self,
        dataset_dir,
        json_file,
        subset=None,
        num_conformers=4,
        conf_id=0,
        rot_file='../../prms/rot70k.0.0.6.jm.mol2',
        rec_box=(90, 90, 90),
        lig_box=(30, 30, 30),
        cell=1.0,
        random_rotation=True,
        random_state=12345,
        sample_to_size=1000,
        shuffle=False
    ):

        self.dataset_dir = Path(dataset_dir).abspath()
        self.subset = subset
        self.json_file = self.dataset_dir.joinpath(json_file)
        self.data = utils_loc.read_json(self.json_file)
        if subset is not None:
            self.data = [v for k, v in enumerate(self.data) if k in subset]

        if False:
            _data = []
            print('Before filter:', len(self.data))
            for x in self.data:
                ag = prody.parsePDB(self.dataset_dir / 'data' / x['sdf_id'] / 'unbound_nmin.pdb')
                box = (ag.getCoords().max(0) - ag.getCoords().min(0)) / cell
                #if np.any(box > 65):
                if np.prod(box) > 70**3:
                    continue
                _data.append(x)
            self.data = _data
            print('After filter:', len(self.data))

        self.num_conformers = num_conformers
        self.random = np.random.default_rng(random_state)

        self.rec_box = rec_box
        self.lig_box = lig_box
        self.cell = cell
        self.random_rotation = random_rotation
        self.random_state = random_state

        self.rotations = read_rotations(rot_file, limit=10000)
        self.random.shuffle(self.rotations)

        if shuffle:
            self.random.shuffle(self.data)

        if sample_to_size is not None:
            probs = np.array([x['weight'] for x in self.data])
            probs /= probs.sum()
            self.data = self.random.choice(self.data, size=sample_to_size, replace=False, p=probs)

        self.conf_id = conf_id

    def __len__(self):
        return len(self.data)

    def _rotate_ligand(self, ag, rot_mat):
        ag = ag.copy()
        coords = ag.getCoords()
        coords = np.dot(coords - coords.mean(0), rot_mat.T) + coords.mean(0)
        ag._setCoords(coords, overwrite=True)
        return ag

    def _calc_rmsd_square(self, crd, ref_crd):
        return np.power(crd - ref_crd, 2).sum(1).mean()

    def _get_rmsd_map(self, mob_crd, crys_crd, rec_grid, lig_grid, symmetries=None):
        cell = self.cell

        cor_shape = np.array(rec_grid.grid.shape)[1:] + np.array(lig_grid.grid.shape)[1:]
        mob_crd_aligned = mob_crd + rec_grid.origin - lig_grid.origin - cell * (np.array(lig_grid.grid.shape)[1:] - 1)
        vec_grid = np.indices(cor_shape.astype(int)) * cell
        vec_grid = np.rollaxis(vec_grid, 0, 4)
        
        if symmetries is None:
            assert mob_crd.shape == crys_crd.shape
            symmetries = [range(crys_crd.shape[0])]

        rmsd_grids = []
        for sym in symmetries:
            crys_crd_cur = crys_crd[list(sym)]
            rmsd_aligned = self._calc_rmsd_square(mob_crd_aligned, crys_crd_cur)
            rmsd_grid = (vec_grid**2).sum(-1) + rmsd_aligned + 2 * np.dot(vec_grid, (mob_crd_aligned - crys_crd_cur).mean(0))
            rmsd_grids.append(rmsd_grid)
        rmsd_grid = np.stack(rmsd_grids).min(0)

        return np.sqrt(rmsd_grid)

    def _get_box_size_from_rec(self, rec_ag, padding=5.0):
        crd = rec_ag.getCoords()
        box = (crd.max(0) - crd.min(0) + padding * 2)
        box = self.cell * (box // self.cell + 1)
        return box

    def _get_box_size_from_lig(self, lig_ag, padding=5.0):
        crd = lig_ag.getCoords()
        box = (np.max(np.stack([crd.max(0) - crd.mean(0), crd.mean(0) - crd.min(0)]), 0) + padding) * 2
        box = self.cell * (box // self.cell + 1)
        return box

    def _get_item(self, ix):
        item = self.data[ix]
        case_dir = self.dataset_dir / 'data' / item['sdf_id']
        frag_name = Path(item['selected_fragment']['mol_path']).stripext() + '_ah'
        rec_ag = prody.parsePDB(case_dir / 'unbound_nmin.pdb')
        lig_ag = prody.parsePDB(case_dir / frag_name + '.pdb').heavy.copy()
        lig_ag_orig = lig_ag.copy()
        rmsd_ag_crys = prody.parsePDB(case_dir / 'crys_lig_ah.pdb').heavy.copy()

        json_dir = self.dataset_dir / 'frag_jsons'
        lig_dict = utils_loc.read_json(json_dir / item['sdf_id'] + '.' + frag_name + '.atoms.json')
        lig_matches_to_crys = utils_loc.read_json(json_dir / item['sdf_id'] + '.' + frag_name + '.crys_matches.json')
        sasa = np.loadtxt(case_dir / 'unbound_sasa.txt')
        #sasa = calc_sasa(rec_ag, normalize=False)

        assert len(lig_ag) == len(lig_dict['atoms'])

        if self.conf_id != 0:
            ligand_rotation = self.rotations[ix % len(self.rotations)]
            lig_ag = self._rotate_ligand(lig_ag, ligand_rotation)
        else:
            ligand_rotation = np.eye(3)

        if self.random_rotation:
            complex_rotation = self.rotations[(ix + 123) % len(self.rotations)]
            tr = prody.Transformation(complex_rotation, np.array([0, 0, 0]))
            rec_ag = tr.apply(rec_ag)
            lig_ag = tr.apply(lig_ag)
            lig_ag_orig = tr.apply(lig_ag_orig)
            rmsd_ag_crys = tr.apply(rmsd_ag_crys)

        if self.rec_box is None:
            rec_grid = rec_feats.make_protein_grids(rec_ag, cell=self.cell, padding=7.0, mode='point')
        else:
            rec_grid = rec_feats.make_protein_grids(rec_ag, cell=self.cell, box_size=self.rec_box, mode='point')
        sasa_grid = make_surface_mask(rec_ag, sasa=sasa, box_shape=rec_grid.grid.shape[1:], box_origin=rec_grid.origin, cell=self.cell)

        rec_atom_types = _rec_atom_types(rec_ag)
        rec_ag_aln = rec_ag.calpha.getSequence()  # TODO: replace with the real alignment
        rec_ref_aln = rec_ag.calpha.getSequence()
        rec_features = rec_feats.target_rec_featurize(rec_ag, rec_ag_aln, rec_ref_aln)

        # derive lig box size from crystal ligand so that all rotated ligand grids are the same size
        if self.lig_box is None:
            lig_box_size = self._get_box_size_from_lig(lig_ag_orig, padding=7.0)
        else:
            lig_box_size = self.lig_box
        G, lig_grid, lig_pairwise = make_lig_graph(lig_dict, lig_ag, box_kwargs={'cell': self.cell, 'box_size': lig_box_size, 'mode': 'point'})
        
        # calc rmsd grid
        #lig_rd_noh = Chem.MolFromMolFile(case_dir / frag_name + '.mol', removeHs=True)
        symmetries = [[x[idx] for idx in G['atom_ids']] for x in lig_matches_to_crys]
        rmsd_map = self._get_rmsd_map(lig_ag.getCoords()[G['atom_ids']], rmsd_ag_crys.getCoords(), rec_grid, lig_grid, symmetries)
        
        if ix % 400 == 0:
            dumpdir = Path('grids').mkdir_p()
            prody.writePDB(dumpdir / 'rec.pdb', rec_ag)
            prody.writePDB(dumpdir / 'lig.pdb', lig_ag)
            prody.writePDB(dumpdir / 'lig_crys.pdb', lig_ag_orig)

        sample = {
            'id': ix,
            'rmsd_map': rmsd_map,
            'ligand_rotation': ligand_rotation,
            'complex_rotation': complex_rotation,
            'rec_grid': rec_grid.grid.astype(DTYPE_FLOAT),
            'rec_grid_origin': rec_grid.origin.astype(DTYPE_FLOAT),
            'rec_grid_delta': rec_grid.delta.astype(DTYPE_FLOAT),
            'rec_atom_types': np.array(rec_atom_types).astype(DTYPE_INT),
            'sasa_grid': sasa_grid.grid.astype(DTYPE_FLOAT),
            'src': G['src'],
            'dst': G['dst'],
            'coords': G['x'],
            'node_features': G['f'],
            'lig_grid': G['grid'],
            'lig_grid_origin': G['grid_origin'][0],
            'lig_grid_delta': G['grid_delta'][0],
            'lig_pairwise': lig_pairwise,
            'edge_features': G['w'],
            'num_nodes': len(G['atom_ids']),
            'num_edges': len(G['src']),
            'conf_id': self.conf_id
        }

        #for k, v in sample.items():
        #    if isinstance(v, torch.Tensor) or isinstance(v, np.ndarray):
        #        print(k, ":", v.shape)

        return sample

    def __getitem__(self, ix):
        try:
            return self._get_item(ix)
        except Exception:
            item = self.data[ix]
            case_dir = self.dataset_dir / 'data' / item['sdf_id']
            frag_name = Path(item['selected_fragment']['mol_path']).stripext() + '_ah'
            print(f'Error processing {case_dir}: {frag_name}')
            raise


def main():
    ds = DockingDataset('dataset', 'train_split/fragments/1673_train_nonhom_rmsdcut4.json', num_conformers=1)
    #print(ds[0]['affinity_class'])
    item = ds[0]
    print('node_features', item['node_features'].shape)
    print('edge_features', item['edge_features'].shape)
    print('rec_grid', item['rec_grid'].shape)


if __name__ == '__main__':
    main()

