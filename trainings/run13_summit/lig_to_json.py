from path import Path
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Geometry import Point3D
from tqdm import tqdm
from utils_loc import read_json, write_json


def lig_to_dict(mol_rd: Chem.Mol):
    atoms = []
    coords = mol_rd.GetConformer(0).GetPositions()

    for idx, atom in enumerate(mol_rd.GetAtoms()):
        atoms.append({
            'GetIdx': atom.GetIdx(),
            'GetSymbol': atom.GetSymbol(),
            'GetFormalCharge': atom.GetFormalCharge(),
            'GetIsAromatic': atom.GetIsAromatic(),
            'GetTotalDegree': atom.GetTotalDegree(),
            'GetTotalNumHs': atom.GetTotalNumHs(),
            'GetTotalValence': atom.GetTotalValence(),
            'IsInRing': atom.IsInRing(),
            'coords': coords[idx].tolist()
        })

    bonds = []
    for bond in mol_rd.GetBonds():
        bonds.append({
            'GetBeginAtomIdx': bond.GetBeginAtomIdx(),
            'GetEndAtomIdx': bond.GetBeginAtomIdx(),
            'GetBondType': str(bond.GetBondType()),
            'IsInRing': bond.IsInRing()
        })

    return {
        'atoms': atoms,
        'bonds': bonds
    }


def main():
    outdir = Path('dataset/frag_jsons').mkdir_p()
    case_list = read_json('dataset/train_split/fragments/2194_train.json') + read_json('dataset/train_split/fragments/valid.json') + read_json('dataset/train_split/fragments/renin.json')
    for item in tqdm(case_list):
        case_dir = Path('dataset/data') / item['sdf_id']
        frag_name = Path(item['selected_fragment']['mol_path']).stripext() + '_ah'
        frag_rd = Chem.MolFromMolFile(case_dir / frag_name + '.mol', removeHs=True)
        assert frag_rd.GetNumAtoms() == frag_rd.GetNumHeavyAtoms()
        crys_rd = Chem.MolFromMolFile(case_dir / 'crys_lig_ah.mol', removeHs=True)

        out_json = outdir / item['sdf_id'] + '.' + frag_name + '.atoms.json'
        write_json(lig_to_dict(frag_rd), out_json)

        matches = crys_rd.GetSubstructMatches(frag_rd, uniquify=False)
        assert len(matches) > 0
        match_json = outdir / item['sdf_id'] + '.' + frag_name + '.crys_matches.json'
        write_json(matches, match_json, indent=None)


if __name__ == '__main__':
    main()

