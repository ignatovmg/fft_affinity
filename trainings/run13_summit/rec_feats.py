import numpy as np
import prody
from mol_grid import GridMaker
from mol_grid.loggers import logger
from collections import OrderedDict
import torch

import utils_loc
from amino_acids import d_3aa, ATOM_TYPES

logger.setLevel('INFO')


DTYPE_FLOAT = np.float32

DTYPE_INT = np.int32

AATYPE = sorted(d_3aa.keys()) + ['X', '-']

REC_DISTOGRAM = {
    'min': 3.25,
    'max': 51,
    'num_bins': 39
}

RESIGRAM_MAX = 32


def _dmat_to_distogram(dmat, dmin, dmax, num_bins, mask=None):
    shape = dmat.shape
    dmat = dmat.copy().flatten()
    dgram = np.zeros((len(dmat), num_bins))
    bin_size = (dmax - dmin) / num_bins
    dmat -= dmin
    bin_ids = (dmat // bin_size).astype(int)
    bin_ids[bin_ids < 0] = 0
    bin_ids[bin_ids >= num_bins] = num_bins - 1
    dgram[np.arange(len(dgram)), bin_ids] = 1.0
    dgram = dgram.reshape(*shape, num_bins)

    if mask is not None:
        mx, my = np.where(mask)
        dgram[mx, my] = 0

    return dgram


def _cbeta_atom(residue):
    if residue is None:
        return None
    name = 'CA' if residue.getResname() == 'GLY' else 'CB'
    cbeta = residue.select('name ' + name)
    if cbeta is not None:
        return cbeta[0]
    return None


def _ag_to_features(rec_ag, ag_aln, tar_aln):
    feats_1d = OrderedDict(
        aatype=[],
        crd_mask=[],
        crd_beta_mask=[],
        crd_beta=[],
        torsions=[],
        torsions_alt=[],
        torsions_mask=[],
        resi=[],
    )

    residues = list(rec_ag.getHierView().iterResidues())
    assert len(residues) == len(ag_aln.replace('-', ''))

    ag_resi = 0
    tar_resi = 0
    for a, b in zip(ag_aln, tar_aln):
        if b != '-':
            feats_1d['aatype'].append(a)
            residue = None
            if a != '-':
                residue = residues[ag_resi]
            feats_1d['crd_mask'].append(residue is not None)
            cbeta = _cbeta_atom(residue)
            feats_1d['crd_beta_mask'].append(cbeta is not None)
            feats_1d['crd_beta'].append(None if cbeta is None else cbeta.getCoords())
            feats_1d['resi'].append(tar_resi)
            tar_resi += 1

        if a != '-':
            ag_resi += 1

    return feats_1d


def _rec_literal_to_numeric(rec_dict_literal):
    rec_dict = rec_dict_literal

    aatype = rec_dict['aatype']
    rec_dict['aatype'] = [np.zeros(len(AATYPE), dtype=DTYPE_FLOAT) for _ in aatype]
    for i, vec in enumerate(rec_dict['aatype']):
        vec[AATYPE.index(aatype[i])] = 1

    crd = np.stack([np.zeros(3) if x is None else x for x in rec_dict['crd_beta']])
    dmat = utils_loc.calc_dmat(crd, crd)
    dgram = _dmat_to_distogram(dmat, REC_DISTOGRAM['min'], REC_DISTOGRAM['max'], REC_DISTOGRAM['num_bins'])

    # cbeta_mask 2d
    crd_beta_mask = np.array(rec_dict['crd_beta_mask'])
    crd_beta_mask_2d = np.outer(crd_beta_mask, crd_beta_mask)

    # residue id 2d
    resi_1d = np.array(rec_dict['resi'])
    resi_2d = resi_1d[None, :] - resi_1d[:, None]
    resi_2d = _dmat_to_distogram(resi_2d, -RESIGRAM_MAX, RESIGRAM_MAX + 1, RESIGRAM_MAX * 2 + 1)

    # mask distogram
    nob_x, nob_y = np.where(crd_beta_mask_2d)
    dgram[nob_x, nob_y] = 0.0

    return {
        'aatype': np.stack(rec_dict['aatype']),
        'crd_mask': np.array(rec_dict['crd_mask']),
        'crd_beta_mask': np.array(rec_dict['crd_beta_mask']),
        'crd_beta': crd,
        'resi_1d': resi_1d,
        'resi_2d': resi_2d,
        'distogram_2d': dgram,
        'crd_beta_mask_2d': crd_beta_mask_2d.astype(DTYPE_FLOAT)
    }


def target_rec_featurize(ag, ag_aln, ref_aln):
    rec_feats = _rec_literal_to_numeric(_ag_to_features(ag, ag_aln, ref_aln))
    rec_1d = np.concatenate([rec_feats['aatype'], rec_feats['crd_mask'][..., None], rec_feats['crd_beta_mask'][..., None]], axis=-1)
    #rec_2d = np.concatenate([rec_feats['distogram_2d'], rec_feats['crd_beta_mask_2d'][..., None]], axis=-1)

    extra = np.tile(rec_feats['aatype'], (rec_feats['aatype'].shape[0], 1, 1))
    rec_2d = np.concatenate([
        rec_feats['distogram_2d'],
        rec_feats['crd_beta_mask_2d'][..., None],
        extra,
        extra.transpose([1, 0, 2]),
    ], axis=2)

    return {
        'rec_1d': rec_1d,
        'rec_2d': rec_2d,
        'relpos_2d': rec_feats['resi_2d']
    }


def rec_grid_config(ag):
    atom_types = ATOM_TYPES
    ag_atoms = set(zip(ag.getResnames(), ag.getNames()))

    # Individual atom types
    selectors = []
    atom_list = []
    donors = []
    acceptors = []
    for resi, atoms in atom_types.items():
        for atom, props in atoms.items():
            atom_list.append((resi, atom))
            if props['donor']:
                donors.append((resi, atom))
            if props['acceptor']:
                acceptors.append((resi, atom))
            if atom.startswith('H') or atom in ['C', 'CA', 'N', 'O', 'H']:
                continue
            selectors.append(ag.select(f'resname {resi} and name {atom}'))

    atom_list = set(atom_list)
    assert ag_atoms.issubset(atom_list), f'Receptor has unknown atoms: {ag_atoms - atom_list}'

    # backbone (5)
    selectors += [ag.select(f'name {x}') for x in ['C', 'CA', 'N', 'O', 'H']]

    # elements (5)
    selectors += [ag.select(f'element {x}') for x in ['C', 'N', 'O', 'S', 'H']]

    # donor / acc (2)
    selectors.append(ag.select(' or '.join([f'(resname {resi} and name {atom})' for resi, atom in donors])))
    selectors.append(ag.select(' or '.join([f'(resname {resi} and name {atom})' for resi, atom in acceptors])))

    # residues
    # TODO: commenting this out because seemingly it's too large for GPU
    #       - the process gets killed on zeblock
    #selectors += list(ag.getHierView().iterResidues())

    return selectors


def make_protein_grids(rec_ag, **box_kwargs):
    grid_maker_rec = GridMaker(config=rec_grid_config, centering='coe', **box_kwargs)
    rec_grid = grid_maker_rec.make_grids(rec_ag)
    return rec_grid