import os
import subprocess
import json
import prody
import contextlib
import tempfile
import shutil
import numpy as np

import Bio
from Bio.SubsMat import MatrixInfo as matlist
from Bio.pairwise2 import format_alignment

import logging
import logging.config

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'default': {
            'format': '[%(levelname)s] %(asctime)s %(funcName)s [pid %(process)d] - %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',
        }
    },
    'loggers': {
        'console': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        }
    }
}

logging.config.dictConfig(LOGGING)
logger = logging.getLogger('console')


@contextlib.contextmanager
def isolated_filesystem(dir=None, remove=True):
    """A context manager that creates a temporary folder and changes
    the current working directory to it for isolated filesystem tests.
    """
    cwd = os.getcwd()
    if dir is None:
        t = tempfile.mkdtemp(prefix='fft_ml-')
    else:
        t = dir
    os.chdir(t)
    try:
        yield t
    except Exception as e:
        logger.error(f'Temporary files are in {t}')
        raise
    else:
        os.chdir(cwd)
        if remove:
            try:
                shutil.rmtree(t)
            except (OSError, IOError):
                pass
    finally:
        os.chdir(cwd)


def get_file_handler(filename, mode='w', level='DEBUG'):
    h = logging.FileHandler(filename, mode=mode)
    h.setFormatter(logging.Formatter('[%(levelname)s] %(asctime)s - %(message)s'))
    h.setLevel(level)
    return h


def safe_read_ag(ag) -> prody.Atomic:
    if isinstance(ag, prody.Atomic):
        return ag
    elif isinstance(ag, str):
        return prody.parsePDB(ag)
    else:
        raise RuntimeError(f"Can't read atom group, 'ag' has wrong type {type(ag)}")


def check_output(call, **kwargs):
    try:
        logger.debug('Running command:\n' + ' '.join(call))
        output = subprocess.check_output(call, **kwargs)
        logger.debug('Command output:\n' + output.decode('utf-8'))
    except subprocess.CalledProcessError as e:
        logger.debug('Command output:\n' + e.output.decode('utf-8'))
        raise
    return output.decode('utf-8')


def write_json(data, path, indent=4):
    with open(path, 'w') as f:
        json.dump(data, f, indent=indent)


def read_json(path):
    with open(path, 'r') as f:
        return json.load(f)


def tmp_file(**kwargs):
    handle, fname = tempfile.mkstemp(**kwargs)
    os.close(handle)
    return fname


def apply_prody_transform(coords, tr):
    return np.dot(coords, tr.getRotation().T) + tr.getTranslation()


def make_list(obj):
    if not (isinstance(obj, tuple) or isinstance(obj, list)):
        obj = [obj]
    return obj


def global_align(s1, s2):
    aln = Bio.pairwise2.align.globalds(s1, s2, matlist.blosum62, -14.0, -4.0)
    return aln


def calc_d2mat(crd1, crd2):
    return np.square(crd1[:, None, :] - crd2[None, :, :]).sum(2)


def calc_dmat(crd1, crd2):
    return np.sqrt(calc_d2mat(crd1, crd2))
