import torch
import torch.optim as optim
from torch import nn
import logging
import sys
from copy import deepcopy
from path import Path
import math


HOROVOD = True
HOROVOD_RANK = 0
if HOROVOD:
    import horovod.torch as hvd
    if __name__ == '__main__':
        hvd.init()
        HOROVOD_RANK = hvd.rank()


class Toy(nn.Module):
    def __init__(self):
        super().__init__()
        self.layers = nn.Linear(1, 1, bias=False)
        #self.layers.weight.fill_(1.0)

    def forward(self, x):
        return self.layers(x)


input = torch.full((1, 1), hvd.rank(), dtype=torch.float32, device='cuda:0')
#input = hvd.allgather(input)
#print(input)
model = Toy().to('cuda:0')
loss = model(input)
allloss = hvd.allgather(loss)
print(allloss)
hvd.synchronize(allloss)

#if HOROVOD_RANK == 0:
#    allloss = hvd.allgather(loss)
#print(allloss.sum()) #.backward()
#(allloss[2] + allloss[1]).backward()
rank_loss = torch.nn.MarginRankingLoss(margin=100)
rank_loss(allloss[2], allloss[1], torch.tensor([1], device='cuda:0')).backward()

for x in model.parameters():
    print(HOROVOD_RANK, x.grad)
