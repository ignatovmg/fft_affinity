import torch
import torch.nn.functional as F
import torch.optim
import prody
import numpy as np
from path import Path
from torch.utils.data import DataLoader

torch.cuda.manual_seed_all(123456)
torch.manual_seed(123456)
np.random.seed(123456)

from fft_ml.training import engines
from fft_ml.loggers import logger
from fft_ml import utils
from fft_ml.training.metrics import BaseMetrics
from mol_grid import loggers

from dataset import LigandDataset
from model import NetSE3


def get_ranking_loss(num_decoys, num_true):
    rank_loss = torch.nn.MarginRankingLoss(margin=100)
    
    def loss_fn(y_pred, y_true):
        x1x2 = []
        labels = []
        
        for y_pred_batch, y_true_batch in zip(y_pred, y_true):
            # select ~num_decoys decoys with label != 0
            frac = min(num_decoys / y_pred_batch.shape[0], 1.0)
            #print('y_pred_batch', y_pred_batch.shape)
            mask_0 = y_true_batch == 0
            #print('y_pred_batch', y_pred_batch.shape)
            mask = (torch.rand_like(y_pred_batch) < frac) * (~mask_0)
            #print('mask', mask.shape)
            
            # randomly select num_true decoys with label 0
            exclude_ids = torch.randperm(max(0, mask_0.sum() - num_true))
            mask_0[mask_0.nonzero()[exclude_ids]] = False
            mask[mask_0] = True
            
            y_pred_batch = y_pred_batch[mask]
            y_true_batch = y_true_batch[mask]
            
            pairs = torch.cartesian_prod(y_pred_batch[y_true_batch == 0], y_pred_batch[y_true_batch == 1])
            x1x2.append(pairs)
            labels.append(-torch.ones(pairs.shape[0], dtype=int, device=y_pred_batch.device))
            
        x1x2 = torch.cat(x1x2)
        labels = torch.cat(labels)
        return rank_loss(x1x2[:, 0], x1x2[:, 1], labels)
    
    return loss_fn


class inTopN(BaseMetrics):
    def __init__(self, ntop):
        BaseMetrics.__init__(self)
        self.nhits = 0
        self.nsamples = 0
        self.ntop = ntop

    def reset(self):
        self.nhits = 0
        self.nsamples = 0

    def batch(self, prediction, target):
        nhits = 0
        #print('prediction', prediction.shape)
        #print('target', target.shape)
        #print((target == 0)[0].sum())
        #print((target == 1)[0].sum())
        #print(prediction)
        for i in range(prediction.shape[0]):
            nhits += (target[i, np.argsort(prediction[i])[:self.ntop]] == 0).any()
        nsamples = prediction.shape[0]
        self.nhits += nhits
        self.nsamples += nsamples
        return nhits / float(nsamples)

    def epoch(self):
        return self.nhits / float(self.nsamples)


def train(
        dataset_dir,
        train_set,
        valid_set,
        max_epoch,
        device_name,
        load_epoch=None,
        batch_size=32,
        ncores=4,
        ngpu=1,
        nsamples=None,
        margin=100,
        lr=0.0001,
        weight_decay=0.00001,
        model=NetSE3(95, 51, 20, 20, 10, 3),
        ray_tune=False,
        ray_checkpoint_dir=None,
        output_dir='train'):

    loggers.logger.setLevel('INFO')
    for k, v in locals().items():
        logger.info("{:20s} = {}".format(str(k), str(v)))

    prody.confProDy(verbosity='error')

    logger.info('Getting device..')
    device = engines.get_device(device_name)
    subset = None if nsamples is None else list(range(nsamples))

    logger.info('Creating train dataset..')
    train_data = utils.read_json(Path(dataset_dir) / train_set)
    train_subset = np.where([x['affinity'] is not None for x in train_data])[0]
    print(len(train_subset))
    train_set = LigandDataset(dataset_dir, train_set, subset=train_subset, random_rotation=True)
    train_loader = DataLoader(dataset=train_set, batch_size=batch_size, num_workers=ncores, shuffle=True)

    logger.info('Creating validation dataset..')
    valid_data = utils.read_json(Path(dataset_dir) / valid_set)
    valid_subset = np.where([x['affinity'] is not None for x in valid_data])[0]
    print(len(valid_subset))
    valid_set = LigandDataset(dataset_dir, valid_set, subset=valid_subset, random_rotation=True)
    valid_loader = DataLoader(dataset=valid_set, batch_size=batch_size, num_workers=ncores, shuffle=False)

    num_channels = train_set[0]['rec_grid'].shape[0]

    optimizer = torch.optim.Adam
    optimizer_params = {'lr': lr, 'weight_decay': weight_decay}
    
    losses = [get_ranking_loss(10000000, 100), torch.nn.CrossEntropyLoss(ignore_index=-1)]

    logger.info('Started training..')
    trainer = engines.Trainer(None,
                              max_epoch,
                              train_loader,
                              model,
                              optimizer,
                              optimizer_params,
                              losses,
                              device,
                              x_keys=['rec_grid', 'lig_grid'],
                              y_key=['prob_map', 'affinity_class'],
                              metrics_dict=[{'top1': inTopN(1), 'top10': inTopN(10), 'top100': inTopN(100), 'top1000': inTopN(1000)}, {}],
                              #metrics_dict=None,
                              test_loader=valid_loader,
                              save_model=True,
                              load_epoch=load_epoch,
                              use_scheduler=True,
                              scheduler_params={'factor': 0.1, 'patience': 5},
                              ngpu=ngpu,
                              every_niter=50,
                              ray_tune=ray_tune,
                              ray_checkpoint_dir=ray_checkpoint_dir,
                              early_stopping=50,
                              collect_garbage_every_n_iter=3,
                              output_dir=Path(output_dir).mkdir_p())
    trainer.run()


def _last_epoch(dir):
    last_epoch = sorted([int(x.stripext().split('_')[-1]) for x in Path(dir).glob('checkpoint*')])
    return None if len(last_epoch) == 0 else last_epoch[-1]


def main():
    import sys

    outdir = Path(sys.argv[1]).mkdir_p()
    gpu_id = int(sys.argv[2])

    # get last computed epoch
    last_epoch = _last_epoch(outdir)

    model = NetSE3(95, 52, -1, 32, 32,
                   kernel_size=5,
                   num_middle_layers=5,
                   num_postfft_layers=3,
                   num_dense_layers=3,
                   middle_activation=F.relu,
                   final_activation=F.tanh)

    train('dataset',
          'train_split/train.json',
          'train_split/valid.json',
          max_epoch=200,
          device_name=f'cuda:{gpu_id}',
          load_epoch=last_epoch,
          batch_size=1,
          ncores=0,
          ngpu=1,
          nsamples=None,
          lr=0.0001,
          weight_decay=0.0000001,
          model=model,
          ray_tune=False,
          ray_checkpoint_dir=None,
          output_dir=outdir)


if __name__ == '__main__':
    main()
