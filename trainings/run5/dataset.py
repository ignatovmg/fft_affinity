from path import Path
import prody
from functools import partial

import numpy as np
import prody
import itertools
import random
import mdtraj as md
import os
import tempfile
from mol_grid import grid_maker
from path import Path
from rdkit import Chem
from rdkit.Chem import AllChem
from sblu.ft import apply_ftresult
from scipy.spatial.transform import Rotation
from torch.utils.data import Dataset
from collections import OrderedDict

from fft_ml import utils
from fft_ml.dataset.amino_acids import residue_bonds_noh

_ELEMENTS = {x[1]: x[0] for x in enumerate(['I', 'S', 'F', 'N', 'C', 'CL', 'BR', 'O', 'H', 'X'])}
_HYBRIDIZATIONS = {x: i for i, x in enumerate(Chem.rdchem.HybridizationType.names.keys())}
_FORMAL_CHARGE = {-1: 0, 0: 1, 1: 2}
_VALENCE = {x: x - 1 for x in range(1, 7)}
_NUM_HS = {x: x for x in range(5)}
_DEGREE = {x: x - 1 for x in range(1, 5)}


def _atom_to_vector(atom):
    vec = [0] * len(_ELEMENTS)
    vec[_ELEMENTS.get(atom.GetSymbol().upper(), _ELEMENTS['X'])] = 1

    new_vec = [0] * len(_HYBRIDIZATIONS)
    new_vec[_HYBRIDIZATIONS[str(atom.GetHybridization())]] = 1
    vec += new_vec

    new_vec = [0] * len(_FORMAL_CHARGE)
    try:
        new_vec[_FORMAL_CHARGE[atom.GetFormalCharge()]] = 1
    except:
        pass
    vec += new_vec

    new_vec = [0, 0]
    new_vec[int(atom.GetIsAromatic())] = 1
    vec += new_vec

    new_vec = [0] * len(_DEGREE)
    try:
        new_vec[_DEGREE[atom.GetTotalDegree()]] = 1
    except:
        pass
    vec += new_vec

    new_vec = [0] * len(_NUM_HS)
    try:
        new_vec[_NUM_HS[atom.GetTotalNumHs()]] = 1
    except:
        pass
    vec += new_vec

    new_vec = [0] * len(_VALENCE)
    try:
        new_vec[_VALENCE[atom.GetTotalValence()]] = 1
    except:
        pass
    vec += new_vec

    new_vec = [0, 0]
    new_vec[int(atom.IsInRing())] = 1
    vec += new_vec

    # 12 bins for Gasteiger charge: [-0.6, +0.6]
    new_vec = [0] * 12
    gast = float(atom.GetProp('_GasteigerCharge'))
    new_vec[int(min(max(gast + 0.6, 0), 1.1) * 10)] = 1
    vec += new_vec

    return np.array(vec, dtype=int)


def _mol_to_grid(mol, ag, mol_atoms=None):
    if mol_atoms is None:
        mol_atoms = [x.GetIdx() for x in mol.GetAtoms()]
    # assert len(mol_atoms[mol_atoms]) == len(ag[mol_atoms])

    mol_elements = np.array([x.GetSymbol().upper() for x in mol.GetAtoms()])
    pdb_elements = np.array([x.upper() for x in ag.getElements()])
    assert all(mol_elements[:len(pdb_elements)] == pdb_elements)

    AllChem.ComputeGasteigerCharges(mol, throwOnParamFailure=True)
    atom_ids_per_channel = None

    for atom_idx in mol_atoms:
        atom = mol.GetAtomWithIdx(atom_idx)
        channels = _atom_to_vector(atom)
        if atom_ids_per_channel is None:
            atom_ids_per_channel = [list() for _ in channels]

        for ch_id in np.where(channels == 1)[0]:
            atom_ids_per_channel[ch_id].append(atom_idx)

    selections = [ag[atoms] if len(atoms) > 0 else None for atoms in atom_ids_per_channel]
    return selections


def _rec_grid_config(ag):
    ag = ag.select('heavy and protein')
    backbone = ['C', 'N', 'O', 'CA', 'OXT']
    elements = ['C', 'N', 'O', 'S']
    names = list(itertools.chain(*[[(aa, atom) for atom in x.keys() if atom not in backbone]
                                   for aa, x in residue_bonds_noh.items()]))
    return [ag.select(f'resname {aa} and name {atom}') for aa, atom in names] + \
           [ag.select(f'protein and name {atom}') for atom in backbone[:-1]] + \
           [ag.select(f'(not water) and element {e}') for e in elements]


def _get_rec_grid_maker(box_size, box_center=None, cell=0.8):
    return grid_maker.GridMaker(
        cell=cell,
        padding=0,
        atom_radius=2.0,
        config=_rec_grid_config,
        sasa_penalty=None,
        fill_value=1,
        box_size=box_size,
        box_center=box_center,
        gaussians=True,
        centering='coe'
    )


def _get_lig_grid_maker(box_shape, rdkit_mol, sasa_penalty=None, mol_atoms=None, box_center=None, cell=0.8):
    return grid_maker.GridMaker(
        cell=cell,
        padding=0,
        atom_radius=2.0,
        config=partial(_mol_to_grid, rdkit_mol, mol_atoms=mol_atoms),
        sasa_penalty=sasa_penalty,
        fill_value=1,
        box_shape=box_shape,
        box_center=box_center,
        gaussians=True,
        centering='com'
    )


def make_rec_lig_grids(rec_ag, lig_ag, lig_rdkit, box_size, mol_atoms=None):
    cell = 1.25

    grid_maker_rec = _get_rec_grid_maker(box_size, cell=cell)
    rec_grid = grid_maker_rec.make_grids(rec_ag)
    rec_grid_center = rec_grid.get_center()

    grid_maker_lig = _get_lig_grid_maker(rec_grid.grid.shape[1:], lig_rdkit, box_center=rec_grid_center, mol_atoms=mol_atoms, cell=cell)
    lig_grid = grid_maker_lig.make_grids(lig_ag)
    return rec_grid, lig_grid


class LigandDataset(Dataset):
    def __init__(self,
                 dataset_dir,
                 json_file,
                 subset=None,
                 box_size=(80, 80, 80),
                 affinity_bins=(-10, 0, 1, 2, 3, 10),
                 random_rotation=True,
                 random_state=12345):

        self.dataset_dir = Path(dataset_dir).abspath()
        if not self.dataset_dir.exists():
            raise OSError(f'Directory {self.dataset_dir} does not exist')

        self.subset = subset

        self.json_file = self.dataset_dir.joinpath(json_file)
        if not self.json_file.exists():
            raise OSError(f'File {self.json_file} does not exist')

        self.data = utils.read_json(self.json_file)

        if subset is not None:
            self.data = [v for k, v in enumerate(self.data) if k in subset]

        self.box_size = box_size
        self.affinity_bins = affinity_bins

        self.random_rotation = random_rotation
        self.random_state = random_state
        if self.random_rotation:
            self.rotations = Rotation.random(len(self), random_state=random_state)
        self.random = random.Random(random_state)

    def __len__(self):
        return len(self.data)

    def _get_affinity(self, affinity):
        if affinity is None:
            return None
        if affinity['type'] not in ['Ki', 'Kd', 'IC50', 'EC50']:
            return None
        if affinity['unit'] != 'nM':
            return None
        affinity = affinity['value']
        if affinity <= 10e-9:
            return self.affinity_bins[0]
        return np.log10(affinity)

    def _get_affinity_class(self, log10):
        if log10 is None:
            return -1
        for i in range(len(self.affinity_bins)-1):
            if self.affinity_bins[i] <= log10 < self.affinity_bins[i+1]:
                return i

    def _get_probability_map(self, rec_grid, some_ag):
        map_shape = np.array(rec_grid.grid.shape[-3:]).astype(int) * 2 - 1
        delta = rec_grid.delta[0]
        max_rmsd = 2.0

        # make a grid with one gaussian with std = max_rmsd / 2 at the center
        pmap = grid_maker.GridMaker(
            cell=delta,
            padding=0,
            atom_radius=max_rmsd,
            config=lambda x: [x[:1]],
            box_center=some_ag[:1].getCoords()[0, :],
            box_shape=map_shape
        ).make_grids(some_ag[:1])

        pmap = pmap.grid
        #pmap /= pmap.sum()
        pmap = (~(pmap > 0.0)).astype(int)
        return pmap

    def __getitem__(self, ix):
        item = self.data[ix]
        case_dir = self.dataset_dir / 'data' / item['sdf_id']
        rec_ag = prody.parsePDB(case_dir / 'unbound_clean.pdb')
        crys_lig_rd = Chem.MolFromMolFile(case_dir / 'lig_ah.mol', removeHs=True)
        crys_lig_ag = utils.mol_to_ag(crys_lig_rd)

        if self.random_rotation:
            rotmat = self.rotations[ix].as_matrix()
            tr = prody.Transformation(rotmat, np.array([0, 0, 0]))
            rec_ag = tr.apply(rec_ag)
            crys_lig_ag = tr.apply(crys_lig_ag)
        else:
            rotmat = np.eye(3)

        rec_grid, lig_grid = make_rec_lig_grids(rec_ag, crys_lig_ag, crys_lig_rd, self.box_size)

        affinity_value = self._get_affinity(item['affinity'])
        affinity_class = self._get_affinity_class(affinity_value)
        prob_map = self._get_probability_map(rec_grid, rec_ag)

        #rec_grid.grid = rec_grid.grid[:5]
        #rec_grid.save('tmp/rec.list')

        #lig_grid.grid = lig_grid.grid[:5]
        #lig_grid.save('tmp/lig.list')
        
        #print('0:', (prob_map==0).sum())
        #print('1:', (prob_map==1).sum())

        sample = {
            'id': ix,
            'case': item['sdf_id'],
            'prob_map': prob_map.reshape(-1),
            'affinity_class': int(affinity_class),
            'rec_grid': rec_grid.grid.astype(np.float32),
            'rec_origin': rec_grid.origin.astype(np.float32),
            'rec_delta': rec_grid.delta.astype(np.float32),
            'lig_grid': lig_grid.grid.astype(np.float32),
            'lig_origin': lig_grid.origin.astype(np.float32),
            'lig_delta': lig_grid.delta.astype(np.float32)
        }
        #print('True class', affinity_class)
        return sample


def main():
    ds = LigandDataset('dataset', 'train_split/train.json')
    print(ds[0]['affinity_class'])
    print(set(ds[0]['prob_map']))


if __name__ == '__main__':
    main()

