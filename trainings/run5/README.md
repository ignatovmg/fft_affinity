---
Works with https://bitbucket.org/ignatovmg/fft_ml commit 5721ff4

Same as run4 but added back affinity, changed patience, added extra layers after fft and added random rotation.
