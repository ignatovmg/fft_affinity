from ray import tune
from ray.tune.schedulers import ASHAScheduler

from path import Path
import torch.nn.functional as F
import torch
import numpy as np

from train import train
from model import NetSE3

DATASET_DIR = Path('dataset').abspath()


def trainable(config, checkpoint_dir=None):
    if config['middle_act'] == 'relu':
        middle_act = F.relu
    elif config['middle_act'] == 'tanh':
        middle_act = F.tanh
    else:
        raise RuntimeError('Unknown activation')

    if config['final_act'] == 'relu':
        final_act = F.relu
    elif config['final_act'] == 'tanh':
        final_act = F.tanh
    elif config['final_act'] is None:
        final_act = None
    else:
        raise RuntimeError('Unknown activation')

    model = NetSE3(95, 33,
                   config['mid_size'],
                   config['mid_size'],
                   config['final_size'],
                   kernel_size=5,
                   n_middle_layers=config['num_layers'],
                   dense_depth=1,
                   middle_activation=middle_act,
                   final_activation=final_act)

    train(Path('details').mkdir_p().realpath(),
          DATASET_DIR,
          'set_pan5class_train.json',
          'set_pan5class_valid.json',
          max_epoch=1000,
          batch_size=1,
          ncores=0,
          ngpu=1,
          lr=config['lr'],
          weight_decay=config['wd'],
          model=model,
          ray_tune=True,
          dataset_kwargs={'atom_radius': config['atom_radius']}
          )


if __name__ == '__main__':
    space = {
        "mid_size": tune.choice([32, 64, 128]),
        "final_size": tune.choice([32, 64, 128]),
        "num_layers": tune.choice([3, 4, 5, 7, 10]),
        "middle_act": tune.choice(['tanh', 'relu']),
        "final_act": tune.choice(['tanh', 'relu']),
        "lr": tune.choice([0.001, 0.0001, 0.00001]),
        "wd": tune.choice([0.00001, 0.000001, 0.0000001]),
        "atom_radius": tune.choice([1.5, 2.0, 2.5])
    }

    best_so_far = [
        {'mid_size': 0,
         'final_size': 0,
         'depth': 2,
         'middle_act': 1,
         'final_act': 0,
         'lr': 1,
         'wd': 2}
    ]

    scheduler = ASHAScheduler(
        time_attr='epoch',
        metric='ray_pairs',
        mode='max',
        max_t=150,
        grace_period=10
    )

    round_name = 'round1'

    analysis = tune.run(
        trainable,
        config=space,
        num_samples=200,
        resources_per_trial={"cpu": 2, "gpu": 1},
        scheduler=scheduler,
        # search_alg=HyperOptSearch(space, metric='CorrectPairsFrac', mode='max', points_to_evaluate=best_so_far),
        local_dir="./tuning",
        name=round_name,
        max_failures=2,
        resume=False,
        raise_on_failed_trial=False
    )

    print(analysis)
    print("Best config: ", analysis.get_best_config(metric="ray_pairs", mode="max"))
    df = analysis.dataframe(metric="ray_pairs", mode="max")
    df.to_csv(f'./tuning/{round_name}/best.csv')
