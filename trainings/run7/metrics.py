import torch
from pytorch_lightning.metrics import Metric


class RankingCorrectPairsFraction(Metric):
    def __init__(self, dist_sync_on_step=False, compute_on_step=True):
        super().__init__(dist_sync_on_step=dist_sync_on_step, compute_on_step=compute_on_step)

        self.add_state("num_correct_pairs", default=torch.tensor(0), dist_reduce_fx="sum")
        self.add_state("num_pairs", default=torch.tensor(0), dist_reduce_fx="sum")

    def update(self, preds, target):
        num_correct_pairs = 0
        num_pairs = 0

        # for each sample in batch
        for pred, tar in zip(preds, target[0]):
            for i in range(len(tar)):
                for j in range(i + 1, len(tar)):
                    if tar[i] == tar[j]:
                        continue
                    if (pred[i] - pred[j]) * (tar[i] - tar[j]) > 0:
                        num_correct_pairs += 1
                    num_pairs += 1
                    
        self.num_correct_pairs += num_correct_pairs
        self.num_pairs += num_pairs

    def compute(self):
        return self.num_correct_pairs.float() / self.num_pairs
    

class TopN(Metric):
    def __init__(self, topn, target_label=0, dist_sync_on_step=False, compute_on_step=True):
        super().__init__(dist_sync_on_step=dist_sync_on_step, compute_on_step=compute_on_step)

        self.topn = topn
        self.target_label = target_label
        self.add_state("correct", default=torch.tensor(0), dist_reduce_fx="sum")
        self.add_state("total", default=torch.tensor(0), dist_reduce_fx="sum")

    def update(self, preds, target):
        correct = 0
        total = 0
        
        for pred, tar in zip(preds, target[0]):
            correct += sum([x[1] == self.target_label for x in sorted(zip(pred, tar))[:self.topn]])
            total += self.topn
            
        self.correct += correct
        self.total += total

    def compute(self):
        return self.correct.float() / self.total
