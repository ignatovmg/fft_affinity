from path import Path
import prody
from functools import partial

import numpy as np
import prody
import itertools
import random
import mdtraj as md
import os
import tempfile
from mol_grid import grid_maker, Grid
from path import Path
from rdkit import Chem
from rdkit.Chem import AllChem
from sblu.ft import apply_ftresult
from scipy.spatial.transform import Rotation
from torch.utils.data import Dataset
from collections import OrderedDict

from sblu.ft import read_rotations, FTRESULT_DTYPE, read_ftresults, apply_ftresult, apply_ftresults_atom_group
from sblu.cli.docking.cmd_cluster import cluster

from fft_ml import utils
from fft_ml.dataset.amino_acids import residue_bonds_noh

DTYPE_FLOAT = np.float32
DTYPE_INT = np.int32

_ELEMENTS = {x[1]: x[0] for x in enumerate(['I', 'S', 'F', 'N', 'C', 'CL', 'BR', 'O', 'P', 'UNK'])}
_HYBRIDIZATIONS = {x: i for i, x in enumerate(Chem.rdchem.HybridizationType.names.keys())}
_FORMAL_CHARGE = {-1: 0, 0: 1, 1: 2}
_VALENCE = {x: x - 1 for x in range(1, 7)}
_NUM_HS = {x: x for x in range(5)}
_DEGREE = {x: x - 1 for x in range(1, 5)}


def _atom_to_vector(atom):
    vec = [0] * (len(_ELEMENTS) + 1)
    vec[_ELEMENTS.get(atom.GetSymbol().upper(), _ELEMENTS['UNK'])] = 1

    # total density of all atoms
    vec[-1] = 1

    new_vec = [0] * len(_HYBRIDIZATIONS)
    new_vec[_HYBRIDIZATIONS[str(atom.GetHybridization())]] = 1
    vec += new_vec

    new_vec = [0] * len(_FORMAL_CHARGE)
    new_vec[_FORMAL_CHARGE[atom.GetFormalCharge()]] = 1
    vec += new_vec

    new_vec = [0, 0]
    new_vec[int(atom.GetIsAromatic())] = 1
    vec += new_vec

    #new_vec = [0] * len(_DEGREE)
    #new_vec[_DEGREE[atom.GetTotalDegree()]] = 1
    #vec += new_vec
    vec += [atom.GetTotalDegree()]

    #new_vec = [0] * len(_NUM_HS)
    #new_vec[_NUM_HS[atom.GetTotalNumHs()]] = 1
    #vec += new_vec
    vec += [atom.GetTotalNumHs()]

    new_vec = [0] * len(_VALENCE)
    new_vec[_VALENCE[atom.GetTotalValence()]] = 1
    vec += new_vec

    new_vec = [0, 0]
    new_vec[int(atom.IsInRing())] = 1
    vec += new_vec

    # 12 bins for Gasteiger charge: [-0.6, +0.6]
    #new_vec = [0] * 12
    #gast = float(atom.GetProp('_GasteigerCharge'))
    #new_vec[int(min(max(gast + 0.6, 0), 1.1) * 10)] = 1
    #vec += new_vec
    vec += [float(atom.GetProp('_GasteigerCharge'))]

    return np.array(vec, dtype=int)


def _mol_to_grid(mol, ag, mol_atoms=None):
    ag = ag.heavy.copy()  # TODO: rerun mol file generation to include Hs
    if mol_atoms is None:
        mol_atoms = [x.GetIdx() for x in mol.GetAtoms()]
    # assert len(mol_atoms[mol_atoms]) == len(ag[mol_atoms])

    mol_elements = np.array([x.GetSymbol().upper() for x in mol.GetAtoms()])
    pdb_elements = np.array([x.upper() for x in ag.getElements()])
    assert all(mol_elements[:len(pdb_elements)] == pdb_elements)

    AllChem.ComputeGasteigerCharges(mol, throwOnParamFailure=True)
    atom_ids_per_channel = None

    for atom_idx in mol_atoms:
        atom = mol.GetAtomWithIdx(atom_idx)
        channels = _atom_to_vector(atom)
        if atom_ids_per_channel is None:
            atom_ids_per_channel = [list() for _ in channels]

        for ch_id in np.where(channels == 1)[0]:
            atom_ids_per_channel[ch_id].append(atom_idx)

    selections = [ag[atoms] if len(atoms) > 0 else None for atoms in atom_ids_per_channel]
    return selections


def _rec_grid_config(ag):
    ag = ag.select('heavy and protein')
    backbone = ['C', 'N', 'O', 'CA', 'OXT']
    elements = ['C', 'N', 'O', 'S']
    names = list(itertools.chain(*[[(aa, atom) for atom in x.keys() if atom not in backbone]
                                   for aa, x in residue_bonds_noh.items()]))
    return [ag.select(f'resname {aa} and name {atom}') for aa, atom in names] + \
           [ag.select(f'name {atom}') for atom in backbone[:-1]] + \
           [ag.select(f'element {e}') for e in elements]


def _get_rec_grid_maker(box_size, box_center=None, cell=0.8):
    return grid_maker.GridMaker(
        cell=cell,
        padding=0,
        atom_radius=2.0,
        config=_rec_grid_config,
        sasa_penalty=None,
        fill_value=1,
        box_size=box_size,
        box_center=box_center,
        gaussians=True,
        centering='coe'
    )


def _get_lig_grid_maker(box_shape, rdkit_mol, sasa_penalty=None, mol_atoms=None, box_center=None, cell=0.8):
    return grid_maker.GridMaker(
        cell=cell,
        padding=0,
        atom_radius=2.0,
        config=partial(_mol_to_grid, rdkit_mol, mol_atoms=mol_atoms),
        sasa_penalty=sasa_penalty,
        fill_value=1,
        box_shape=box_shape,
        box_center=box_center,
        gaussians=True,
        centering='com'
    )


def make_rec_lig_grids(rec_ag, lig_ag, lig_rdkit, box_size, mol_atoms=None):
    cell = 1.25

    grid_maker_rec = _get_rec_grid_maker(box_size, cell=cell)
    rec_grid = grid_maker_rec.make_grids(rec_ag)

    grid_maker_lig = _get_lig_grid_maker(rec_grid.grid.shape[1:], lig_rdkit, mol_atoms=mol_atoms, cell=cell)
    lig_grid = grid_maker_lig.make_grids(lig_ag)
    return rec_grid, lig_grid


def _cluster_models(ag, radius):
    rmsd_matrix = []
    for i in range(ag.numCoordsets()):
        ag.setACSIndex(i)
        prody.alignCoordsets(ag)
        rmsd_matrix.append(prody.calcRMSD(ag))
    rmsd_matrix = np.stack(rmsd_matrix)
    return cluster(rmsd_matrix, radius, 1, ag.numCoordsets())


def sample_ligand(lig_rd, cluster_radius=3.0, max_clusters=3, num_confs=10, num_threads=1):
    lig_rd = AllChem.AddHs(lig_rd)
    AllChem.EmbedMultipleConfs(lig_rd, numConfs=num_confs, numThreads=num_threads)
    energies = AllChem.MMFFOptimizeMoleculeConfs(lig_rd, numThreads=num_threads, maxIters=1000)

    conf_coords = np.stack([lig_rd.GetConformer(x).GetPositions() for x in range(lig_rd.GetNumConformers())])
    lig_ag = utils.mol_to_ag(lig_rd)
    lig_ag._setCoords(conf_coords, overwrite=True)

    clusters = _cluster_models(lig_ag.heavy.copy(), cluster_radius)[:max_clusters]
    lig_ag._setCoords(conf_coords[[x for x, _ in clusters]], overwrite=True)
    AllChem.RemoveHs(lig_rd)

    return lig_ag.heavy.copy()


class LigandDataset(Dataset):
    def __init__(self,
                 dataset_dir,
                 json_file,
                 subset=None,
                 nn_rmsd=2.0,
                 num_rotations=1,
                 num_conformers=1,
                 rot_file='rot70k.0.0.6.jm.mol2',
                 max_heavy_atoms=30,
                 box_size=(80, 80, 80),
                 affinity_bins=(-10, 0, 1, 2, 3, 10),
                 random_rotation=True,
                 random_state=12345):

        self.dataset_dir = Path(dataset_dir).abspath()
        if not self.dataset_dir.exists():
            raise OSError(f'Directory {self.dataset_dir} does not exist')

        self.subset = subset

        self.json_file = self.dataset_dir.joinpath(json_file)
        if not self.json_file.exists():
            raise OSError(f'File {self.json_file} does not exist')

        self.data = utils.read_json(self.json_file)
        if max_heavy_atoms is not None:
            self.data = [x for x in self.data if x['natoms_heavy'] <= max_heavy_atoms]

        if subset is not None:
            self.data = [v for k, v in enumerate(self.data) if k in subset]

        self.nn_rmsd = nn_rmsd
        self.num_rotations = num_rotations
        self.num_conformers = num_conformers
        self.rotations = read_rotations(rot_file, limit=1000)
        self.box_size = box_size
        self.affinity_bins = affinity_bins

        self.random_rotation = random_rotation
        self.random_state = random_state
        self.random = random.Random(random_state)

    def __len__(self):
        return len(self.data)

    def _get_affinity(self, affinity):
        if affinity is None:
            return None
        if affinity['type'] not in ['Ki', 'Kd', 'IC50', 'EC50']:
            return None
        if affinity['unit'] != 'nM':
            return None
        affinity = affinity['value']
        if affinity <= 10e-9:
            return self.affinity_bins[0]
        return np.log10(affinity)

    def _get_affinity_class(self, log10):
        if log10 is None:
            return -1
        for i in range(len(self.affinity_bins)-1):
            if self.affinity_bins[i] <= log10 < self.affinity_bins[i+1]:
                return i

    def _calc_rmsd_square(self, crd, ref_crd):
        return np.power(crd - ref_crd, 2).sum(1).mean()

    def _get_rmsd_map(self, lig_ag, crys_lig_ag, rec_grid, lig_grid, interface_atoms=None):
        cell = rec_grid.delta[0]
        if interface_atoms is None:
            interface_atoms = range(len(lig_ag))

        crd = lig_ag.getCoords()[interface_atoms]
        crys_crd = crys_lig_ag.getCoords()[interface_atoms]

        assert all([x == y for x, y in zip(rec_grid.grid.shape[1:], lig_grid.grid.shape[1:])]), \
            'Rec and lig have different shapes'
        shape = np.array(rec_grid.grid.shape)[1:]

        crd_aligned = crd + rec_grid.origin - lig_grid.origin - cell * (shape - 1)
        rmsd_aligned = self._calc_rmsd_square(crd_aligned, crys_crd)

        vec_grid = np.indices(list((shape * 2 - 1).astype(int))) * cell
        vec_grid = np.rollaxis(vec_grid, 0, 4)
        rmsd_grid = (vec_grid**2).sum(-1) + rmsd_aligned + 2 * np.dot(vec_grid, (crd_aligned - crys_crd).mean(0))
        return np.sqrt(rmsd_grid)

    def _rotate_ligand_around_com(self, ag, rot_mat):
        ag = ag.copy()
        coords = ag.getCoords()
        coords = np.dot(coords - coords.mean(0), rot_mat.T) + coords.mean(0)
        ag._setCoords(coords, overwrite=True)
        return ag

    def _get_ligand_models(self, lig_rd, lig_ag):
        assert lig_rd.GetNumAtoms() == len(lig_ag)
        lig_models = [lig_ag.copy()]

        if self.num_conformers > 1:
            lig_confs = sample_ligand(lig_rd, max_clusters=self.num_conformers - 1)
            for crd in lig_confs.getCoordsets():
                lig_conf = lig_ag.copy()
                lig_conf._setCoords(crd, overwrite=True)
                lig_models.append(lig_conf)

            # add copies if not enough conformers was sampled
            cur_count = len(lig_models)
            i = 0
            while cur_count < self.num_conformers:
                lig_models.append(lig_models[i % cur_count].copy())
                i += 1

        if self.num_rotations > 1:
            for modeli in range(len(lig_models)):
                lig_model = lig_models[modeli]
                for roti in range(self.num_rotations - 1):
                    rotmat = self.random.choice(self.rotations)
                    lig_models.append(self._rotate_ligand_around_com(lig_model, rotmat))

        return lig_models

    def _get_sub_box(self, rmsd_grid, radius, cell_size):
        center = np.stack(np.where(rmsd_grid <= self.nn_rmsd)).mean(1)
        max_crd = np.min(np.stack([ np.ceil(center + radius / cell_size), rmsd_grid.shape]), 0).astype(DTYPE_INT)
        min_crd = np.max(np.stack([np.floor(center - radius / cell_size), [0, 0, 0]]), 0).astype(DTYPE_INT)
        return min_crd, max_crd

    def __getitem__(self, ix):
        item = self.data[ix]
        case_dir = self.dataset_dir / 'data' / item['sdf_id']
        rec_ag = prody.parsePDB(case_dir / 'unbound_clean.pdb')
        crys_lig_rd = Chem.MolFromMolFile(case_dir / 'lig_ah.mol', removeHs=True)
        crys_lig_ag = utils.mol_to_ag(crys_lig_rd)

        lig_models = self._get_ligand_models(crys_lig_rd, crys_lig_ag)

        if self.random_rotation:
            rotmat = self.random.choice(self.rotations)
            tr = prody.Transformation(rotmat, np.array([0, 0, 0]))
            rec_ag = tr.apply(rec_ag)
            crys_lig_ag = tr.apply(crys_lig_ag)
            for modeli in range(len(lig_models)):
                lig_models[modeli] = tr.apply(lig_models[modeli])
        else:
            rotmat = np.eye(3)

        rmsd_maps = []
        lig_grids = []
        rec_grid = None
        for lig_model in lig_models:
            rec_grid, lig_grid = make_rec_lig_grids(rec_ag, lig_model, crys_lig_rd, self.box_size)
            rmsd_maps.append(self._get_rmsd_map(lig_model, crys_lig_ag, rec_grid, lig_grid))
            lig_grids.append(lig_grid)

        if False:
            outdir = Path('tmp').mkdir_p()
            prody.writePDB(outdir / 'rec.pdb', rec_ag)
            rec_grid.grid = rec_grid.grid[-4:-1]
            rec_grid.save(outdir / 'rec_grid.list')
            prody.writePDB(outdir / f'lig_crys.pdb', crys_lig_ag)
            for i in range(len(lig_models)):
                prody.writePDB(outdir / f'lig{i}.pdb', lig_models[i])
                lig_grid = lig_grids[i]
                lig_grid.grid = lig_grid.grid[3:6]
                lig_grid.save(outdir / f'lig_grid{i}.list')

                rmsd_grid = rmsd_maps[i]
                rmsd_grid = Grid(rmsd_grid[None, :], origin=rec_grid.origin - np.array(rec_grid.grid.shape[1:]) * rec_grid.delta / 2, delta=rec_grid.delta)
                rmsd_grid.save(outdir / f'rmsd_grid{i}.list')

        affinity_value = self._get_affinity(item['affinity'])
        affinity_class = self._get_affinity_class(affinity_value)

        sample = {
            'id': ix,
            'case': item['sdf_id'],
            'rmsd_maps': np.stack(rmsd_maps).astype(DTYPE_FLOAT),
            'affinity_class': int(affinity_class),
            'rec_grid': rec_grid.grid.astype(DTYPE_FLOAT),
            #'rec_origin': rec_grid.origin.astype(np.float32),
            #'rec_delta': rec_grid.delta.astype(np.float32),
            'lig_grids': np.stack([x.grid for x in lig_grids]).astype(DTYPE_FLOAT),
            #'lig_origin': lig_grid.origin.astype(np.float32),
            #'lig_delta': lig_grid.delta.astype(np.float32)
        }
        #print('True class', affinity_class)
        return sample


def main():
    ds = LigandDataset('dataset', 'train_split/train.json', rot_file='../../prms/rot70k.0.0.6.jm.mol2', num_rotations=2, num_conformers=2)
    ds[0]
    #print(ds[0]['affinity_class'])
    #print(set(ds[0]['prob_map']))


if __name__ == '__main__':
    main()

