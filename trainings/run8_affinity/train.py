import torch
import torch.nn.functional as F
import torch.optim
import prody
import numpy as np
from functools import partial
from path import Path
from torch.utils.data import DataLoader, DistributedSampler
from torch.optim.lr_scheduler import ReduceLROnPlateau

from mol_grid import loggers as mol_loggers

import pytorch_lightning as pl
from pytorch_lightning.loggers import CSVLogger, TensorBoardLogger
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from pytorch_lightning.metrics import Accuracy, Precision, Recall, AUROC, ConfusionMatrix
#from pytorch_lightning.plugins.training_type.rpc_sequential import RPCSequentialPlugin
from pytorch_lightning.plugins import DeepSpeedPlugin

from ray.tune.integration.pytorch_lightning import TuneReportCallback
from ray import tune

np.random.seed(123456)
pl.seed_everything(123456)

#from utils_loc import logger
from dataset import DockingDataset
from model import NetSE3
from metrics import TopN


def _init_metrics():
    return torch.nn.ModuleDict({
        'Accuracy': Accuracy(dist_sync_on_step=True), 
        'Precision': Precision(dist_sync_on_step=True), 
        'Recall': Recall(dist_sync_on_step=True), 
        'ConfusionMatrix': ConfusionMatrix(num_classes=2, dist_sync_on_step=True),
        #'AUC': AUROC(pos_label=1, compute_on_step=False)
    })


class LitModel(pl.LightningModule):
    def __init__(self, model, loss, lr=1e-4, factor=0.5, patience=10):
        super().__init__()
        self.lr = lr
        self.factor = factor
        self.patience = patience

        self.model = model
        self.loss = loss
        self.train_metrics = _init_metrics()
        self.valid_metrics = _init_metrics()

    def forward(self, *x):
        return self.model(*x)
    
    def _step(self, batch, batch_idx):
        y_true = batch['binding']
        y_pred = self(batch)
        print('ids', batch['id'])
        print('y_true', y_true)
        return y_pred, y_true, batch['duplicate']
    
    def _step_end(self, outputs, mode='train'):
        y_pred = outputs[0]
        y_true = outputs[1]
        loss, pred, label = self.loss(y_pred, y_true, outputs[2])
        self.log(f'{mode}_loss', loss, on_step=False, on_epoch=True, prog_bar=True, logger=True)
        
        metrics = self.train_metrics if mode == 'train' else self.valid_metrics
        if len(metrics) > 0:
            for k, v in metrics.items():
                self.log(f'{mode}_' + k, v(torch.sigmoid(pred), label.type(torch.int64)), on_step=False, on_epoch=True, prog_bar=True, logger=True)
        return loss

    def training_step(self, batch, batch_idx):
        return self._step(batch, batch_idx)
    
    def training_step_end(self, outputs):
        return self._step_end(outputs, 'train')

    def validation_step(self, batch, batch_idx):
        return self._step(batch, batch_idx)
    
    def validation_step_end(self, outputs):
        print(outputs)
        return self._step_end(outputs, 'valid')

    def configure_optimizers(self):
        opt = torch.optim.Adam(self.parameters(), lr=self.lr)
        scheduler = ReduceLROnPlateau(opt, 'min', factor=self.factor, patience=self.patience)
        return {
            'optimizer': opt,
            'lr_scheduler': scheduler,
            'monitor': 'valid_loss'
        }
    
    
def loss_fn():
    def fun(y_pred, y_true, duplicates):
        label = y_true[:1]
        pred = y_pred[~duplicates, 0].mean(0, keepdim=True)
        print('Final pred', pred)
        
        #pos_weight = torch.ones(1, dtype=label.dtype, device=label.device) * 100
        bce = torch.nn.BCEWithLogitsLoss() #pos_weight=pos_weight)
        return bce(pred, label), pred, label
    return fun


def train(
        outdir,
        dataset_dir,
        train_set,
        valid_set,
        max_epoch=200,
        batch_size=8,
        ncores=0,
        model=None,
        ray_tune=False
):
    early_stop_patience = 200
    sched_patience = 5
    
    #for k, v in locals().items():
    #    logger.info("{:20s} = {}".format(str(k), str(v)))

    prody.confProDy(verbosity='error')

    #logger.info('Creating train dataset..')
    train_set = DockingDataset(dataset_dir, train_set, num_conformers=batch_size, shuffle=True, upsample=True)
    train_loader = DataLoader(dataset=train_set, batch_size=batch_size, num_workers=ncores, shuffle=False) #, sampler=DistributedSampler(train_set, shuffle=False))

    #logger.info('Creating validation dataset..')
    valid_set = DockingDataset(dataset_dir, valid_set, num_conformers=batch_size)
    valid_loader = DataLoader(dataset=valid_set, batch_size=batch_size, num_workers=ncores, shuffle=False) #, sampler=DistributedSampler(valid_set, shuffle=False))
    
    #loss = torch.nn.BCEWithLogitsLoss() #, torch.nn.CrossEntropyLoss(ignore_index=-1)]
    loss = loss_fn()

    if ray_tune:
        outdir = tune.get_trial_dir()

    #logger.info('Started training..')
    model = LitModel(
        model,
        loss,
        patience=sched_patience
    )

    loggers = {TensorBoardLogger(outdir + '/logs', 'tb'), CSVLogger(outdir + '/logs', 'csv')}
    checkpoint_callback = ModelCheckpoint(dirpath=outdir + '/checkpoints', monitor='valid_loss', filename='checkpoint-{epoch:02d}-{valid_loss:.2f}', save_top_k=2, mode='max', save_last=True)
    early_stopping = EarlyStopping(monitor='valid_loss', min_delta=0.00, patience=early_stop_patience, verbose=True, mode='min')
    tune_callback = TuneReportCallback({'ray_valid_loss': 'valid_loss'}, on='validation_end')
    
    trainer = pl.Trainer(
        precision=32,
        gpus=4,
        accelerator='dp',
        #plugins=[DeepSpeedPlugin(cpu_offload=False)],
        default_root_dir=outdir,
        #log_gpu_memory='all',
        logger=loggers,
        callbacks=[checkpoint_callback, early_stopping] + ([] if not ray_tune else [tune_callback]),
        log_every_n_steps=1,
        fast_dev_run=False,
        max_epochs=max_epoch,
        #overfit_batches=5,
        deterministic=True,
        #profiler='simple',
        val_check_interval=0.33,
        #limit_val_batches=1000
        #resume_from_checkpoint=outdir + '/checkpoints/last.ckpt',
        replace_sampler_ddp=False
    )
    trainer.fit(model, train_loader, valid_loader)


def main():
    import sys
    outdir = Path(sys.argv[1]).mkdir_p()

    model = NetSE3()
    pytorch_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print('Num params:', pytorch_total_params)

    train(outdir,
          'dataset',
          'train_split/train.json',
          'train_split/valid.json',
          model=model)
    
    #from dataset
    #ag = prody.parsePDB('ADRB2_set/recs/3p0g_nmin.pdb')
    


if __name__ == '__main__':
    main()
