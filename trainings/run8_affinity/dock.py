from model import NetSE3
from dataset import _rec_grid_config
from train import LitModel

from mol_grid import grid_maker, Grid
import subprocess
import pandas as pd

import contextlib
import torch
import torch.nn.functional as F
import prody
from rdkit import Chem
from rdkit.Chem import AllChem
from functools import partial
import numpy as np
import gc
import json
import os
from tqdm import tqdm
from path import Path
from collections import Counter

from sblu.ft import read_rotations, FTRESULT_DTYPE, read_ftresults, apply_ftresult, apply_ftresults_atom_group
from sblu.rmsd import calc_rmsd, interface, pwrmsd
from sblu.cli.docking.cmd_cluster import cluster

from dataset import make_protein_grids


def _load_model(checkpoint, device):
    model = NetSE3(102, None, 128, 128,
                   kernel_size=5,
                   num_middle_layers=5,
                   num_postfft_layers=0,
                   num_dense_layers=3,
                   num_rolls=2,
                   middle_activation=F.relu,
                   final_activation=F.tanh,
                   use_batchnorm=False,
                   resnet=False
                   )
    lit = LitModel.load_from_checkpoint(checkpoint, model=model, loss=None, x_keys=None, y_keys=None)
    model = lit.model
    model.to(device)
    model.eval()
    return model


def _grid_to_tensor(grid, device):
    return torch.tensor(grid.astype(np.float32), device=device, requires_grad=False)


def _correlate(rec_grid, lig_grid, net, device):
    rec_torch = _grid_to_tensor(rec_grid, device)
    lig_torch = _grid_to_tensor(lig_grid, device)
    energy_grid = net(rec_torch, lig_torch[:, None])
    energy_grid = energy_grid.view(energy_grid.shape[0], -1)
    print(energy_grid.shape)
    del rec_torch
    del lig_torch
    return energy_grid


def _select_top_energy(energy_grid, rec_grid, lig_grid, ntop=1):
    cor_shape = [x * 2 - 1 for x in rec_grid.grid.shape[1:]]
    energies = []
    tvs = []

    for min_idx in energy_grid.argsort()[:ntop]:
        min_idx = min_idx.item()
        min_energy = energy_grid[min_idx].item()
        min_idx3d = np.unravel_index(min_idx, cor_shape)
        cor_shift = (np.array(min_idx3d) - np.array(rec_grid.grid.shape[1:])) * lig_grid.delta
        tv = rec_grid.origin - lig_grid.origin + cor_shift

        energies.append(min_energy)
        tvs.append(tv)

    return energies, tvs


def _write_ft_results(fname, ft_results):
    with open(fname, 'w') as f:
        for x in ft_results:
            f.write(f'{x[0]:<4d} {x[1][0]:<8f} {x[1][1]:<8f} {x[1][2]:<8f} {x[2]:<8f}\n')


def _cluster_models(ag, radius):
    rmsd_matrix = []
    for i in range(ag.numCoordsets()):
        ag.setACSIndex(i)
        prody.alignCoordsets(ag)
        rmsd_matrix.append(prody.calcRMSD(ag))
    rmsd_matrix = np.stack(rmsd_matrix)
    return cluster(rmsd_matrix, radius, 1, ag.numCoordsets())


def _make_grid(rec_ag, box_size):
    return make_protein_grids(rec_ag, box_size, cell=2.0)


def _get_ag_dims(ag):
    crd = ag.getCoords()
    return crd.max(0) - crd.min(0)


def dock(rec_pdb, lig_pdb, rot_file, num_rots, checkpoint, device='cuda:0', num_poses=1000, num_ft_per_rotation=1, batch_size=1):
    rec_ag = prody.parsePDB(rec_pdb)
    lig_ag = prody.parsePDB(lig_pdb)
    prody.writePDB('rec.pdb', rec_ag)
    prody.writePDB('lig.pdb', lig_ag)

    rec_dims = _get_ag_dims(rec_ag)
    lig_dims = _get_ag_dims(lig_ag)
    box_size = np.stack([rec_dims, lig_dims]).max(0) + 4
    print(box_size)
    if np.any(box_size > 80):
        return

    rec_grid = _make_grid(rec_ag, box_size)

    rots = read_rotations(rot_file, num_rots)
    rots = np.insert(rots, 0, np.eye(3, 3), axis=0)

    net = _load_model(checkpoint, device)

    ft_results = []
    lig_ag_rotated = lig_ag.copy()
    for rot_id_start in tqdm(range(len(rots))[::batch_size]):
        lig_grids = []
        # prepare a batch of ligand grids
        for rot_mat in rots[rot_id_start:rot_id_start+batch_size]:
            lig_coords = lig_ag.getCoords()
            lig_coords = np.dot(lig_coords - lig_coords.mean(0), rot_mat.T) + lig_coords.mean(0)
            lig_ag_rotated._setCoords(lig_coords, overwrite=True)
            lig_grid = _make_grid(lig_ag_rotated, box_size)
            lig_grids.append(lig_grid)

        # get energy grid batch
        energy_grid_batch = _correlate(np.stack([rec_grid.grid] * len(lig_grids)), np.stack([x.grid for x in lig_grids]), net, device)

        # select top poses for each rotation in the batch
        for batch_id, energy_grid in enumerate(energy_grid_batch):
            rot_id = rot_id_start + batch_id
            min_energies, tvs = _select_top_energy(energy_grid, rec_grid, lig_grids[batch_id], ntop=num_ft_per_rotation)
            for min_energy, tv in zip(min_energies, tvs):
                ft_results.append((rot_id, tuple(tv), min_energy))

        del energy_grid_batch
        torch.cuda.empty_cache()
        gc.collect()

    ft_results = sorted(ft_results, key=lambda x: x[2])
    _write_ft_results('ft.000.00', ft_results)
    ft_results = read_ftresults('ft.000.00')
    lig_ft_ag = apply_ftresults_atom_group(lig_ag, ft_results[:num_poses], rots)
    prody.writePDB('lig_ft.pdb', lig_ft_ag)

    interface_atoms = interface(rec_ag.calpha.getCoords(), lig_ag.calpha.getCoords(), 10)
    rmsd = calc_rmsd(lig_ft_ag, lig_ag, interface=interface_atoms)
    np.savetxt('rmsd.txt', rmsd)

    pwrmsd_ = pwrmsd(lig_ft_ag.calpha)
    np.savetxt('pwrmsd.txt', pwrmsd_.flatten())

    clusters = cluster(pwrmsd_, 9, 1, 100)
    centers = [x[0] for x in clusters]
    np.savetxt('rmsd_clus.txt', rmsd[centers])
    
    lig_clus_ag = lig_ag.copy()
    lig_clus_ag._setCoords(lig_ft_ag.getCoordsets()[centers], overwrite=True)
    prody.writePDB('lig_clus.pdb', lig_clus_ag)


def main():
    df = pd.read_csv('../dataset/benchmark5.5/Table_BM5.5.csv')
    counter = 0
    for i, row in df.iterrows():
        if row['Cat.'][0] == 'A':
            continue
        if counter >= 20:
            break
        case = row['Complex'][:4]
        rec_pdb = f'../dataset/benchmark5.5/structures/{case}_r_b.pdb'
        lig_pdb = f'../dataset/benchmark5.5/structures/{case}_l_b.pdb'
        rot_file = '../dataset/prms/rot70k.0.0.6.jm.mol2'
        wdir = Path(f'{case}_{row["Cat."]}_{row["Difficulty"][:3]}').rmtree_p().mkdir_p()
        oldpwd = Path.getcwd()
        with cwd(wdir):
            dock(
                oldpwd / rec_pdb,
                oldpwd / lig_pdb,
                oldpwd / rot_file,
                10000,
                oldpwd / '../run/checkpoints/checkpoint-epoch=140-valid_loss=0.04.ckpt',
                'cuda:0',
                num_ft_per_rotation=1,
                batch_size=2
            )
        if not (wdir / 'rmsd.txt').exists():
            wdir.rmtree()
        counter += 1


@contextlib.contextmanager
def cwd(path):
    oldpwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)


if __name__ == '__main__':
    main()
