from model import NetSE3
from dataset import _rec_grid_config
from train import LitModel

from mol_grid import grid_maker, Grid
import subprocess
import pandas as pd

import contextlib
import torch
import torch.nn.functional as F
import prody
from rdkit import Chem
from rdkit.Chem import AllChem
from functools import partial
import numpy as np
import gc
import json
import os
from tqdm import tqdm
from path import Path
from collections import Counter

from sblu.ft import read_rotations, FTRESULT_DTYPE, read_ftresults, apply_ftresult, apply_ftresults_atom_group
from sblu.rmsd import calc_rmsd, interface, pwrmsd
from sblu.cli.docking.cmd_cluster import cluster

from dataset import make_protein_grids, make_surface_mask, make_lig_graph, DTYPE_FLOAT, DTYPE_INT
import utils_loc


def _load_model(checkpoint, device):
    model = NetSE3(trunk_egrid=False)
    lit = LitModel.load_from_checkpoint(checkpoint, model=model, loss=None)
    model = lit.model
    model.to(device)
    model.eval()
    return model


def _to_tensor(x, device):
    return torch.tensor(x, device=device, requires_grad=False)


def _grid_to_tensor(grid, device):
    return _to_tensor(grid.astype(np.float32), device)


def _select_top_energy(energy_grid, rec_grid, lig_grid, ntop=1, radius=3):
    cor_shape = list(energy_grid.shape[-3:]) #[x * 2 - 1 for x in rec_grid.grid.shape[1:]]
    cor_center = [x // 2 for x in cor_shape]
    energies = []
    tvs = []

    mask_grid = np.zeros(energy_grid.shape[-3:])
    energy_grid = energy_grid.flatten()
    for min_idx in energy_grid.argsort():
        if len(tvs) >= ntop:
            break

        min_idx = min_idx.item()
        min_idx3d = np.unravel_index(min_idx, cor_shape)
        
        #if mask_grid[min_idx3d] > 0:
        #    continue
        #mask_grid[
        #    min(0, min_idx3d[0]-radius):min_idx3d[0]+radius, 
        #    min(0, min_idx3d[1]-radius):min_idx3d[1]+radius, 
        #    min(0, min_idx3d[2]-radius):min_idx3d[2]+radius
        #] = 1
        
        min_energy = energy_grid[min_idx].item()
        cor_shift = (np.array(min_idx3d) - np.array(cor_center)) * lig_grid.delta
        tv = rec_grid.origin - lig_grid.origin + cor_shift

        dists = [np.power(tv - x, 2).sum() for x in tvs]
        if len(dists) > 0 and any([x < radius**2 for x in dists]):
            continue

        energies.append(min_energy)
        tvs.append(tv)

    return energies, tvs


def _write_ft_results(fname, ft_results):
    with open(fname, 'w') as f:
        for x in ft_results:
            f.write(f'{x[0]:<4d} {x[1][0]:<8f} {x[1][1]:<8f} {x[1][2]:<8f} {x[2]:<8f}\n')


def _get_ag_dims(ag):
    crd = ag.getCoords()
    return crd.max(0) - crd.min(0)


def _calc_symm_rmsd(ag, ref_cset_id, symmetries, mol_to_pdb_mapping=None):
    csets = ag.getCoordsets()
    ref_crd = csets[ref_cset_id]
    rmsds = []
    for m in symmetries:
        ref_atoms = range(ref_crd.shape[0])
        mob_atoms = list(m)

        if mol_to_pdb_mapping is not None:
            ref_atoms = [mol_to_pdb_mapping[x] for x in ref_atoms if x in mol_to_pdb_mapping]
            mob_atoms = [mol_to_pdb_mapping[x] for x in mob_atoms if x in mol_to_pdb_mapping]

        rmsd = np.power(ref_crd[None, ref_atoms, :] - csets[:, mob_atoms, :], 2).sum((1, 2)) / ref_crd.shape[0]
        rmsds.append(rmsd)
    rmsds = np.sqrt(np.stack(rmsds).min(0))
    return rmsds


def _boltzman_clustering(ag, energies, symmetries, radius, min_size=1, max_clusters=None):
    energies = np.array(energies)
    unused = np.array(range(len(energies)))

    clusters = []
    while len(unused) > 0:
        center = unused[np.argmin(energies[unused])]
        rmsd = _calc_symm_rmsd(ag, center, symmetries)
        members = unused[np.where(rmsd[unused] < radius)[0]]
        unused = unused[~np.isin(unused, members)]
        if len(members) >= min_size:
            clusters.append((center.item(), members.tolist()))
        if max_clusters is not None and len(clusters) == max_clusters:
            break

    return clusters


def dock(case_dir, rot_file, checkpoint, device='cpu', num_rots=0, tr_per_rot=5, num_poses=2500):
    case_dir = Path(case_dir)
    rec_ag = prody.parsePDB(case_dir / 'rec_unbound_nmin.pdb')
    lig_ag = prody.parsePDB(case_dir / 'frag_crys_ah.pdb')
    lig_rd = Chem.MolFromMolFile(case_dir / 'frag_crys_ah.mol', removeHs=False)
    lig_rd_noh = Chem.MolFromMolFile(case_dir / 'frag_crys_ah.mol', removeHs=True)
    symmetries = lig_rd.GetSubstructMatches(lig_rd_noh, uniquify=False)
    sasa = np.loadtxt(case_dir / 'sasa.txt')

    prody.writePDB('rec.pdb', rec_ag)
    prody.writePDB('lig.pdb', lig_ag)

    rec_dims = _get_ag_dims(rec_ag)
    box_size = rec_dims + 16
    print(box_size)
    if np.any(box_size > 90):
        return

    rots = read_rotations(rot_file, num_rots)
    rots = np.insert(rots, 0, np.eye(3, 3), axis=0)
    
    if True:
        rec_grid = make_protein_grids(rec_ag, box_size=box_size, cell=1.0, atom_radius=2.0)
        sasa_grid = make_surface_mask(rec_ag, sasa=sasa, box_shape=rec_grid.grid.shape[1:], box_origin=rec_grid.origin, cell=1.0)
    
        net = _load_model(checkpoint, device)

        ft_results = [list() for x in range(tr_per_rot)]
        lig_ag_rotated = lig_ag.copy()
        for rot_id in tqdm(range(len(rots))):
            rot_mat = rots[rot_id]
            lig_coords = lig_ag.getCoords()
            lig_coords = np.dot(lig_coords - lig_coords.mean(0), rot_mat.T) + lig_coords.mean(0)
            lig_ag_rotated._setCoords(lig_coords, overwrite=True)

            G, lig_grid = make_lig_graph(lig_rd, lig_ag_rotated, box_kwargs={'box_shape': rec_grid.grid.shape[-3:], 'cell': 1.0, 'atom_radius': 2.0})
            sample = {
                'id': _to_tensor([123 if rot_id > 0 else 0], device),  # to make model save grids for rot_id = 0
                'rec_grid': _grid_to_tensor(rec_grid.grid.astype(DTYPE_FLOAT), device)[None],
                'rec_grid_origin': _to_tensor(rec_grid.origin.astype(DTYPE_FLOAT), device)[None],
                'rec_grid_delta': _to_tensor(rec_grid.delta.astype(DTYPE_FLOAT), device)[None],
                'sasa_grid': _grid_to_tensor(sasa_grid.grid.astype(DTYPE_FLOAT), device)[None],
                'src': _to_tensor(G.edges()[0], device)[None],
                'dst': _to_tensor(G.edges()[1], device)[None],
                'coords': _to_tensor(G.ndata['x'], device)[None],
                'node_features': _to_tensor(G.ndata['f'], device)[None],
                'lig_grid': _to_tensor(G.ndata['grid'], device)[None],
                'lig_grid_origin': _to_tensor(G.ndata['grid_origin'][0], device)[None],
                'lig_grid_delta': _to_tensor(G.ndata['grid_delta'][0], device)[None],
                'edge_features': _to_tensor(G.edata['w'], device)[None],
                'num_nodes': _to_tensor(G.num_nodes(), device)[None],
                'num_edges': _to_tensor(G.num_edges(), device)[None]
            }

            # get energy grid batch
            energy_grid = net(sample)

            # select top poses for each rotation
            min_energies, tvs = _select_top_energy(energy_grid.detach(), rec_grid, lig_grid, ntop=tr_per_rot)
            for ft_, min_energy, tv in zip(ft_results, min_energies, tvs):
                ft_.append((rot_id, tuple(tv), min_energy))
                
            for x in sample.values():
                del x
            # important, otherwise memory overfills
            del energy_grid
            torch.cuda.empty_cache()
            gc.collect()

        for ft_id, ft_single_rot in enumerate(ft_results):
            ft_single_rot = sorted(ft_single_rot, key=lambda x: x[2])
            _write_ft_results(f'ft.000.{ft_id:02d}', ft_single_rot)

        ft_merged = []
        for x in ft_results:
            ft_merged += x
        ft_merged = sorted(ft_merged, key=lambda x: x[2])
        _write_ft_results(f'ft.merged', ft_merged)

    ft_merged = read_ftresults('ft.merged')
    lig_ft_ag = apply_ftresults_atom_group(lig_ag, ft_merged[:num_poses], rots)
    prody.writePDB('lig_ft.pdb', lig_ft_ag)

    rmsd = []
    for symm in symmetries:
        rmsd.append(calc_rmsd(lig_ft_ag.heavy, lig_ag.heavy[list(symm)]))
    rmsd = np.stack(rmsd).min(0)
    np.savetxt('rmsd.txt', rmsd)

    #pwrmsd_ = pwrmsd(lig_ft_ag.heavy)
    #np.savetxt('pwrmsd.txt', pwrmsd_.flatten())

    #clusters = cluster(pwrmsd_, 3, 1, 100)
    clusters = _boltzman_clustering(lig_ft_ag.heavy, [x[2] for x in ft_merged[:num_poses]], symmetries, 3)
    utils_loc.write_json(clusters, 'clusters_bzman.json')
    
    centers = [x[0] for x in clusters]
    np.savetxt('rmsd_clus_bzman.txt', rmsd[centers])
    
    lig_clus_ag = lig_ag.copy()
    lig_clus_ag._setCoords(lig_ft_ag.getCoordsets()[centers], overwrite=True)
    prody.writePDB('lig_clus_bzman.pdb', lig_clus_ag)


def main():
    dockdir = Path('docking_3rad').mkdir_p()
    for item in utils_loc.read_json('dataset/train_split/test.json'):
        case_name = Path(item['dir']).basename()
        case_dir = (Path('dataset') / 'data' / Path(item['dir']).basename()).abspath()
        wdir = (dockdir / case_name).mkdir_p()
        oldpwd = Path.getcwd()
        print(wdir)
        if (wdir / 'lig_clus_bzman.pdb').exists():
            continue
        with cwd(wdir):
            dock(
                case_dir,
                oldpwd / '../../prms/rot70k.0.0.6.jm.mol2',
                oldpwd / 'run/checkpoints/checkpoint-epoch=13-valid_Top1=0.00-v1.ckpt'
            )
        if not (wdir / 'rmsd.txt').exists():
            wdir.rmtree()


@contextlib.contextmanager
def cwd(path):
    oldpwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)


if __name__ == '__main__':
    main()
