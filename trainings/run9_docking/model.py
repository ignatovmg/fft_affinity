import torch
import torch.nn.functional as F
import torch.optim
import numpy as np
from path import Path

from se3cnn import SE3Convolution, SE3BNConvolution, SE3BatchNorm
from se3cnn.non_linearities import ScalarActivation

import dgl
from dgl.nn.pytorch import GraphConv, NNConv
from torch import nn
from torch.nn import functional as F
from typing import Dict, Tuple, List
import itertools

from equivariant_attention.modules import GConvSE3, GNormSE3, get_basis_and_r, GSE3Res, GMaxPooling, GAvgPooling
from equivariant_attention.fibers import Fiber

from mol_grid import Grid


class SE3Transformer(nn.Module):
    """SE(3) equivariant GCN with attention"""
    def __init__(
            self,
            num_layers: int,
            in_structure: List[Tuple[int,int]],
            num_channels: int,
            out_structure: List[Tuple[int,int]],
            num_degrees: int=4,
            edge_dim: int=4,
            div: float=4,
            n_heads: int=1,
            **kwargs
    ):
        super().__init__()
        # Build the network
        self.num_layers = num_layers
        self.num_channels = num_channels
        self.num_degrees = num_degrees
        self.edge_dim = edge_dim
        self.div = div
        self.n_heads = n_heads

        self.fibers = {'in': Fiber(structure=in_structure),
                       'mid': Fiber(num_degrees, self.num_channels),
                       'out': Fiber(structure=out_structure)}

        blocks = self._build_gcn(self.fibers)
        self.Gblock = blocks

    def _build_gcn(self, fibers):
        # Equivariant layers
        Gblock = []
        fin = fibers['in']
        for i in range(self.num_layers):
            Gblock.append(GSE3Res(fin, fibers['mid'], edge_dim=self.edge_dim, div=self.div, n_heads=self.n_heads))
            Gblock.append(GNormSE3(fibers['mid']))
            fin = fibers['mid']
        Gblock.append(GConvSE3(fibers['mid'], fibers['out'], self_interaction=True, edge_dim=self.edge_dim))

        return nn.ModuleList(Gblock)

    def forward(self, G, node_features: dict):
        # Compute equivariant weight basis from relative positions
        basis, r = get_basis_and_r(G, self.num_degrees-1)

        # encoder (equivariant layers)
        h = {k: G.ndata[v] for k, v in node_features.items()}
        for layer in self.Gblock:
            h = layer(h, G=G, r=r, basis=basis)
        return h
    
    
class GCNEmbedding(torch.nn.Module):
    def __init__(self, num_input_features, num_output_features, num_edge_features, num_layers=3, activation=F.relu):
        super(GCNEmbedding, self).__init__()
        
        edge_funcs = [torch.nn.Linear(num_edge_features, num_input_features * num_output_features)]
        for i in range(num_layers-1):
            edge_funcs.append(torch.nn.Linear(num_edge_features, num_output_features * num_output_features))
        self.edge_funcs = torch.nn.ModuleList(edge_funcs)
        
        layers = [NNConv(num_input_features, num_output_features, self.edge_funcs[0], 'mean')]
        for i in range(1, num_layers):
            layers.append(NNConv(num_output_features, num_output_features, self.edge_funcs[i], 'mean'))
        self.layers = torch.nn.ModuleList(layers)
        
        self.bnorm = torch.nn.ModuleList([torch.nn.BatchNorm1d(num_output_features) for i in range(num_layers)])
        
        self.act = activation
        
    def forward(self, G, node_features, edge_features):
        x = node_features
        for i, l in enumerate(self.layers):
            x = l(G, x, edge_features)
            x = self.bnorm[i](x)
            x = self.act(x)
        x = F.tanh(x)
        return x


def conv3d(R_in, R_out, kernel_size):
    assert kernel_size % 2 == 1
    common_block_params = {
        'size': kernel_size,
        'padding': kernel_size // 2,
        'stride': 1,
        'dyn_iso': True,
        'bias': None
    }
    return SE3Convolution(R_in, R_out, **common_block_params)


class BasicBlockSE3(torch.nn.Module):
    def __init__(self, inplanes, planes, kernel_size=5, downsample=None, use_batchnorm=False, activation=F.relu):
        super(BasicBlockSE3, self).__init__()

        layers = [conv3d([(inplanes, 0)], [(planes, 0)], kernel_size)]
        if use_batchnorm:
            layers.append(torch.nn.BatchNorm3d(planes))

        layers.append(ScalarActivation([(planes, activation)], bias=False))

        layers.append(conv3d([(planes, 0)], [(planes, 0)], kernel_size))
        if use_batchnorm:
            layers.append(torch.nn.BatchNorm3d(planes))

        self.layers = torch.nn.ModuleList(layers)

        self.downsample = downsample

    def forward(self, x):
        identity = x
        out = x

        #if self.stride > 1:
        #    out = F.avg_pool3d(out, 3, stride=1, padding=1)

        for l in self.layers:
            #print(out.shape)
            out = l(out)

        if self.downsample is not None:
            identity = self.downsample(identity)

        out += identity
        #out = self.act(out)
        return out
    
    
def _to_numpy(t):
    return t.detach().cpu().numpy()


class NetSE3(torch.nn.Module):
    def __init__(
        self,
        num_rec_features=102,
        num_lig_features=33,
        num_lig_edge_features=9,
        num_transformer_layers=1,
        num_middle_channels=64,
        num_output_channels=64,
        num_cnn_layers=5,
        num_postfft_layers=0,
        num_dense_layers=3,
        middle_activation=F.relu,
        final_activation=torch.tanh,
        use_batchnorm=False,
        resnet=False,
        trunk_egrid=True
    ):

        super(NetSE3, self).__init__()
        self.__dict__.update({k: v for k, v in locals().items() if k != 'self'})

        #self.transformer = SE3Transformer(
        #    num_transformer_layers,
        #    [(num_lig_features, 0)],
        #    num_lig_features,
        #    [(num_middle_channels, 0)],
        #    num_degrees=4,
        #    edge_dim=num_lig_edge_features,
        #    div=4,
        #    n_heads=1
        #)
        
        self.lig_embed = GCNEmbedding(num_lig_features, num_middle_channels, num_lig_edge_features)

        self.rec = self._make_block(
            num_rec_features,
            num_middle_channels,
            num_output_channels,
            num_cnn_layers,
            middle_activation,
            final_activation,
            resnet=resnet,
            use_batchnorm=use_batchnorm,
            kernel_size=7
        )

        self.lig = self._make_block(
            num_middle_channels,
            num_middle_channels,
            num_output_channels,
            num_cnn_layers,
            middle_activation,
            final_activation,
            resnet=resnet,
            use_batchnorm=use_batchnorm,
            kernel_size=5
        )

        #self.dense1 = self._make_dense(num_output_channels, 1, num_dense_layers)
        self.dense1 = self._make_block(
            num_output_channels,
            num_output_channels,
            1,
            3,
            F.relu,
            None,
            False,
            False,
            1
        )

    def _make_block(
        self,
        input_channels,
        middle_channels,
        output_channels,
        num_layers,
        middle_act,
        final_act,
        resnet,
        use_batchnorm,
        kernel_size
    ):
        layers = []
        embedding_size = input_channels

        for i in range(num_layers-1):
            in_size = embedding_size if i == 0 else middle_channels
            if not resnet:
                layers.append(conv3d([(in_size, 0)], [(middle_channels, 0)], kernel_size))
                if use_batchnorm:
                    layers.append(torch.nn.BatchNorm3d(middle_channels))
                layers.append(ScalarActivation([(middle_channels, middle_act)], bias=False))
            else:
                layers.append(BasicBlockSE3(in_size, middle_channels, kernel_size, activation=middle_act, use_batchnorm=use_batchnorm))
                layers.append(ScalarActivation([(middle_channels, middle_act)], bias=False))

        layers.append(conv3d([(middle_channels, 0)], [(output_channels, 0)], kernel_size))
        if final_act is not None:
            layers.append(ScalarActivation([(output_channels, final_act)], bias=False))

        return torch.nn.Sequential(*layers)

    @staticmethod
    def _make_dense(in_size, out_size, n_layers):
        if n_layers == 1:
            return torch.nn.Linear(in_size, out_size, bias=False)
        if n_layers > 1:
            layers = []
            for x in range(n_layers - 1):
                layers += [torch.nn.Linear(in_size, in_size, bias=False), torch.nn.ReLU()]
            layers += [torch.nn.Linear(in_size, out_size, bias=False)]
            return torch.nn.Sequential(*layers)
        raise RuntimeError('Wrong dense layer depth')

    @staticmethod
    def _correlate3d_torch18(a, b):
        a = F.pad(a, (0, a.shape[-1] - 1, 0, a.shape[-2] - 1, 0, a.shape[-3] - 1))
        b = F.pad(b, (b.shape[-1] - 1, 0, b.shape[-2] - 1, 0, b.shape[-3] - 1, 0))
        b = b.flip((-3, -2, -1))

        a1 = torch.fft.fftn(a, dim=[-3, -2, -1], norm='backward')
        b1 = torch.fft.fftn(b, dim=[-3, -2, -1], norm='backward')
        inv = torch.fft.ifftn(a1 * b1, dim=[-3, -2, -1], norm='backward')
        return inv.real

    @staticmethod
    def _correlate3d_torch16(a, b):
        a = F.pad(a, (0, a.shape[-1] - 1, 0, a.shape[-2] - 1, 0, a.shape[-3] - 1))
        b = F.pad(b, (b.shape[-1] - 1, 0, b.shape[-2] - 1, 0, b.shape[-3] - 1, 0))
        b = b.flip((-3, -2, -1))

        a1 = torch.rfft(a, 3, normalized=False, onesided=False)
        b1 = torch.rfft(b, 3, normalized=False, onesided=False)

        p = torch.zeros_like(a1)
        p[..., 0] = a1[..., 0] * b1[..., 0] - a1[..., 1] * b1[..., 1]
        p[..., 1] = a1[..., 0] * b1[..., 1] + a1[..., 1] * b1[..., 0]
        inv = torch.irfft(p, 3, normalized=False, onesided=False)
        return inv

    @staticmethod
    def _make_graphs(args):
        graphs = []
        for i in range(args['num_nodes'].shape[0]):
            G = dgl.graph((args['src'][i][:args['num_edges'][i]], args['dst'][i][:args['num_edges'][i]]))
            G.ndata['x'] = args['coords'][i][:args['num_nodes'][i]]
            G.ndata['f'] = args['node_features'][i][:args['num_nodes'][i]]
            G.edata['d'] = G.ndata['x'][G.edges()[1]] - G.ndata['x'][G.edges()[0]]
            G.edata['w'] = args['edge_features'][i][:args['num_edges'][i]]
            graphs.append(G)
        return graphs

    def forward(self, args):
        rec_grids = self.rec(args['rec_grid'])

        graphs = self._make_graphs(args)
        #print(graphs)

        lig_features = []
        for g in graphs:
            #lig_features.append(self.transformer(g, {'0': 'f'})['0'])
            lig_features.append(self.lig_embed(g, g.ndata['f'][..., 0], g.edata['w']))
        lig_features = torch.stack(lig_features)
        
        if torch.any(args['id'] % 100 == 0):
            print('node_features')
            print(args['node_features'][..., 0])
            print('lig_features')
            print(lig_features)
        
        lig_grids_input = torch.einsum('baxyz,baf->bfxyz', args['lig_grid'], lig_features)
        
        lig_grids = self.lig(lig_grids_input)
        
        sasa_mask = args['sasa_grid'] #self.sasa_cnn(args['sasa_grid'])

        cor_grids = self._correlate3d_torch18(rec_grids * sasa_mask, lig_grids)

        # apply fc layers
        energy_grids = self.dense1(cor_grids)

        print('Energy grid', energy_grids.min(), energy_grids.median(), energy_grids.max())
        #print(self.a)
        #print(self.b)
        #print(self.log_coef)
        
        for bi, idx in enumerate(args['id']):
            if idx % 400 == 0:
                grid_dir = Path('grids').mkdir_p() 
                grid_delta = _to_numpy(args['rec_grid_delta'][bi])
                
                rec_shape_numpy = np.array(args['rec_grid'][bi].shape[1:])
                grid = _to_numpy(energy_grids[bi])
                # energy_origin = rec_center - (rec_shape - 1) * cell
                grid_origin = _to_numpy(args['rec_grid_origin'][bi]) + rec_shape_numpy * grid_delta * 0.5 - (rec_shape_numpy - 1) * grid_delta
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'energy_grid.list')

                if 'rmsd_map' in args:
                    grid = _to_numpy(args['rmsd_map'][bi][None])
                    grid_origin[0] += int(rec_grids.shape[-3] / 2)
                    grid_origin[1] += int(rec_grids.shape[-2] / 2)
                    grid_origin[2] += int(rec_grids.shape[-1] / 2)
                    Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'rmsd_grid.list')
                
                grid = _to_numpy(args['rec_grid'][bi][:10])
                grid_origin = _to_numpy(args['rec_grid_origin'][bi])
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'rec_precnn.list')
                
                grid = _to_numpy(rec_grids[bi][:10])
                grid_origin = _to_numpy(args['rec_grid_origin'][bi])
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'rec_cnn.list')
                
                grid = _to_numpy(lig_grids_input[bi][:10])
                grid_origin = _to_numpy(args['lig_grid_origin'][bi])
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'lig_precnn.list')
                
                grid = _to_numpy(lig_grids[bi][:10])
                grid_origin = _to_numpy(args['lig_grid_origin'][bi])
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'lig_cnn.list')
                
                grid = _to_numpy(args['sasa_grid'][bi])
                grid_origin = _to_numpy(args['rec_grid_origin'][bi])
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'sasa_precnn.list')
                
                grid = _to_numpy(sasa_mask[bi])
                grid_origin = _to_numpy(args['rec_grid_origin'][bi])
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'sasa_cnn.list')

        if self.trunk_egrid:
            ts = rec_grids.shape[-3:]
            energy_grids = energy_grids[:, :, int(ts[0]/2):int(ts[0]/2)+ts[0], int(ts[1]/2):int(ts[1]/2)+ts[1], int(ts[2]/2):int(ts[2]/2)+ts[2]]
        return energy_grids


def _example():
    from dataset import DockingDataset
    from torch.utils.data import DataLoader

    def _correlate1d_torch18(a, b):
        a = F.pad(a, (0, a.shape[-1] - 1))
        b = F.pad(b, (b.shape[-1] - 1, 0))
        b = b.flip((-1))

        a1 = torch.fft.fftn(a, dim=[-1], norm='backward')
        b1 = torch.fft.fftn(b, dim=[-1], norm='backward')
        inv = torch.fft.ifftn(a1 * b1, dim=[-1], norm='backward')
        return inv.real

    a = torch.ones(4)
    b = torch.ones(4)
    print(_correlate1d_torch18(a, b))
    #from mol_grid import Grid
    #print(cor.shape)
    #Grid(cor.cpu().detach().sum(1).numpy(), origin=np.array([0., 0., 0.]), delta=np.array([1., 1., 1.])).save('tmp/grid.list')


if __name__ == '__main__':
    _example()
