import contextlib
import os

import gc
import numpy as np
import prody
import torch
import subprocess
from path import Path
from rdkit import Chem
from sblu.ft import read_rotations, read_ftresults, apply_ftresults_atom_group
from sblu.rmsd import calc_rmsd
from tqdm import tqdm
import pybel
from rdkit.Chem import AllChem
import traceback
from collections import OrderedDict
import tarfile

import utils_loc
from dataset import make_protein_grids, make_protein_grids_new, make_surface_mask, make_lig_graph, DTYPE_FLOAT
from model import NetSE3
from train import LitModel


@contextlib.contextmanager
def cwd(path):
    oldpwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)


def _load_model(checkpoint, device):
    model = NetSE3()
    lit = LitModel.load_from_checkpoint(checkpoint, model=model, loss=None)
    model = lit.model
    model.to(device)
    model.eval()
    return model


def _to_tensor(x, device):
    return torch.tensor(x, device=device, requires_grad=False)


def _grid_to_tensor(grid, device):
    return _to_tensor(grid.astype(np.float32), device)


def _get_ag_dims(ag):
    crd = ag.getCoords()
    return crd.max(0) - crd.min(0)


def _score(out_json, rec_pdb, lig_mols, checkpoint, device='cuda:0'):
    rec_ag = prody.parsePDB(rec_pdb)
    sasa = None

    rec_grid = make_protein_grids(rec_ag, cell=1.0, atom_radius=2.0, padding=7, mode='point')
    sasa_grid = make_surface_mask(rec_ag, sasa=sasa, box_shape=rec_grid.grid.shape[1:], box_origin=rec_grid.origin, cell=1.0)
    net = _load_model(checkpoint, device)

    scores = OrderedDict()
    for lig_mol in tqdm(lig_mols):
        _add_hs('tmp_ah.mol', lig_mol)
        lig_rd = Chem.MolFromMolFile('tmp_ah.mol', removeHs=False)
        lig_ag = utils_loc.mol_to_ag(lig_rd)
        G, lig_grid = make_lig_graph(
            lig_rd, lig_ag, hydrogens='all',
            box_kwargs={'cell': 1.0, 'box_shape': rec_grid.grid.shape[-3:], 'box_origin': rec_grid.origin, 'mode': 'point'}
        )
        sample = {
            'id': _to_tensor([123], device),
            'rec_grid': _grid_to_tensor(rec_grid.grid.astype(DTYPE_FLOAT), device)[None],
            'rec_grid_origin': _to_tensor(rec_grid.origin.astype(DTYPE_FLOAT), device)[None],
            'rec_grid_delta': _to_tensor(rec_grid.delta.astype(DTYPE_FLOAT), device)[None],
            'sasa_grid': _grid_to_tensor(sasa_grid.grid.astype(DTYPE_FLOAT), device)[None],
            'src': _to_tensor(G.edges()[0], device)[None],
            'dst': _to_tensor(G.edges()[1], device)[None],
            'coords': _to_tensor(G.ndata['x'], device)[None],
            'node_features': _to_tensor(G.ndata['f'], device)[None],
            'lig_grid': _to_tensor(G.ndata['grid'], device)[None],
            'lig_grid_origin': _to_tensor(G.ndata['grid_origin'][0], device)[None],
            'lig_grid_delta': _to_tensor(G.ndata['grid_delta'][0], device)[None],
            'edge_features': _to_tensor(G.edata['w'], device)[None],
            'num_nodes': _to_tensor(G.num_nodes(), device)[None],
            'num_edges': _to_tensor(G.num_edges(), device)[None]
        }

        #
        rec_fwd = net.transform_rec(sample)
        lig_fwd = net.transform_lig(sample)
        terms = (rec_fwd * lig_fwd).sum([2,3,4], keepdim=True)
        #print(terms.shape)
        score = net.dense1(terms).item()
        #print(score)
        scores[lig_mol] = score

        # important, otherwise memory overfills
        del rec_fwd
        del lig_fwd
        del terms
        torch.cuda.empty_cache()
        gc.collect()

    utils_loc.write_json(scores, out_json)


def _add_hs(out_mol, in_mol):
    # add hydrogens
    mol = next(pybel.readfile('mol', in_mol))
    mol.addh()
    mol.localopt('mmff94', steps=500)
    mol.write('mol', out_mol, overwrite=True)

    # fix back the coordinates
    mol = Chem.MolFromMolFile(out_mol, removeHs=False)
    ag = utils_loc.mol_to_ag(mol)
    ref_ag = utils_loc.mol_to_ag(Chem.MolFromMolFile(in_mol, removeHs=True))
    tr = prody.calcTransformation(ag.heavy, ref_ag.heavy)
    ag = tr.apply(ag)
    prody.writePDB(Path(out_mol).stripext() + '.pdb', ag)

    utils_loc.change_mol_coords(mol, ag.getCoords())
    AllChem.ComputeGasteigerCharges(mol, throwOnParamFailure=True)

    Chem.MolToMolFile(mol, out_mol)


def _check_output(call, **kwargs):
    try:
        output = subprocess.check_output(call, **kwargs)
    except subprocess.CalledProcessError as e:
        print('Running command:', ' '.join(call))
        print('Command output:\n' + e.output.decode('utf-8'))
        raise


def _prepare_pdb(out_prefix, pdb, prefix='charmm_param'):
    _check_output(['sblu', 'pdb', 'prep',
                   '--psfgen', 'psfgen',
                   '--no-minimize',
                   '--out-prefix', out_prefix,
                   '--prm', f'{prefix}.prm',
                   '--rtf', f'{prefix}.rtf',
                   pdb])
    nmin_pdb = Path(out_prefix + '.pdb')
    nmin_psf = nmin_pdb.stripext() + '.psf'
    if not nmin_pdb.exists():
        raise RuntimeError('Cannot prepare', pdb)
    return nmin_pdb, nmin_psf


def met_bm_main():
    oldpwd = Path(__file__).abspath().dirname()
    try:
        Path('sampled').rmtree_p()
        with tarfile.open('sampled.tar.gz') as t:
            t.extractall('sampled')

        lig_mols = sorted(Path('sampled').abspath().glob('*/lig.mol'))
        with cwd('new_cnn'):
            _prepare_pdb('rec_nmin', 'rec.pdb', prefix=oldpwd / 'dataset/prms/charmm_param')
            _score(
                'new_cnn.json',
                'rec_nmin.pdb',
                lig_mols,
                #oldpwd / 'ripper_frag_run/checkpoints/checkpoint-epoch=24-valid_loss=1.88-valid_Top1=0.32-step=27918.ckpt',
                #oldpwd / 'zeblok_frag_run/checkpoints/checkpoint-epoch=11-valid_loss=1.55-valid_Top1=0.25-step=13651.ckpt',
                #oldpwd / 'zeblok_frag_polar_run/checkpoints/checkpoint-epoch=18-valid_loss=1.69-valid_Top1=0.26-step=22077.ckpt',
                #oldpwd / 'zeblok_frag_polar_recnew_run/checkpoints/checkpoint-epoch=24-valid_loss=3.95-valid_Top1=0.19-step=29049.ckpt',
                oldpwd / 'zeblok_frag_ah_070121_run/checkpoints/checkpoint-epoch=12-valid_loss=1.73-valid_Top1=0.22-step=15105.ckpt',
                #oldpwd / 'zeblok_frag_polar_070621_run/checkpoints/checkpoint-epoch=15-valid_loss=1.68-valid_Top1=0.22-step=18591.ckpt',
                device='cuda:0',
            )
    except:
        with open('new_cnn/exc', 'w') as f:
            f.write(traceback.format_exc())
        traceback.print_exc()
    finally:
        Path('sampled').rmtree_p()


if __name__ == '__main__':
    for d in sorted(Path('met_bm').glob('????_?')):
        with cwd(d):
            met_bm_main()
