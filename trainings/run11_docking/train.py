import torch
import torch.nn.functional as F
import torch.optim
import prody
import numpy as np
import traceback
from functools import partial
from path import Path
from torch.utils.data import DataLoader, DistributedSampler
from torch.optim.lr_scheduler import ReduceLROnPlateau

from mol_grid import loggers as mol_loggers

import pytorch_lightning as pl
from pytorch_lightning.loggers import CSVLogger, TensorBoardLogger
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from pytorch_lightning.metrics import Accuracy, Precision, Recall, AUROC, ConfusionMatrix
#from pytorch_lightning.plugins.training_type.rpc_sequential import RPCSequentialPlugin
from pytorch_lightning.plugins import DeepSpeedPlugin

from ray.tune.integration.pytorch_lightning import TuneReportCallback
from ray import tune

np.random.seed(123456)
pl.seed_everything(123456)

#from utils_loc import logger
from dataset import DockingDataset
from model import NetSE3
from metrics import TopN, RMSDCounter

CUDA_EMPTY_CACHE_EVERY = 1
BATCH_SIZE = 4
TRAIN_SPLIT_DIR = Path('train_split/fragments')


def _init_metrics(suffix=''):
    return torch.nn.ModuleDict({
        'Top1' + suffix: TopN(1, dist_sync_on_step=True),
        'Top1000' + suffix: TopN(1000, dist_sync_on_step=True),
        'RMSDCounter' + suffix: RMSDCounter([(0, 1), (0, 2), (0, 4), (0, 10), (10, 1000)])
    })


class LitModel(pl.LightningModule):
    def __init__(self, model, loss, lr=1e-4, factor=0.5, patience=10):
        super().__init__()
        self.lr = lr
        self.factor = factor
        self.patience = patience

        self.model = model
        self.loss = loss
        self.train_metrics = {} #_init_metrics()
        self.valid_metrics = _init_metrics()
        self.renin_metrics = _init_metrics('_renin')

    def forward(self, *x):
        return self.model(*x)

    def train_dataloader(self):
        epoch = self.trainer.current_epoch
        batch_size = BATCH_SIZE
        train_set = DockingDataset('dataset', TRAIN_SPLIT_DIR / 'train.json', num_conformers=batch_size, shuffle_data=True, random_state=(epoch+1)*100)
        return DataLoader(dataset=train_set, batch_size=batch_size, num_workers=0, shuffle=False)

    def val_dataloader(self):
        epoch = self.trainer.current_epoch
        batch_size = BATCH_SIZE
        valid = DockingDataset('dataset', TRAIN_SPLIT_DIR / 'valid.json', num_conformers=batch_size, shuffle_data=False, random_state=(epoch+1)*100)
        loader1 = DataLoader(dataset=valid, batch_size=batch_size, num_workers=0, shuffle=False)
        valid = DockingDataset('dataset', TRAIN_SPLIT_DIR / 'renin.json', num_conformers=batch_size, shuffle_data=False, random_state=(epoch+1)*100)
        loader2 = DataLoader(dataset=valid, batch_size=batch_size, num_workers=0, shuffle=False)
        return [loader1, loader2]

    def _step(self, batch, batch_idx, dataloader_idx=0):
        y_true = batch['rmsd_map']
        y_pred = self(batch)
        #print(y_pred)
        return y_pred, y_true, batch_idx, dataloader_idx

    def _step_end(self, outputs, mode='train'):
        y_pred = outputs[0]
        y_true = outputs[1]
        batch_idx = outputs[2]
        
        loss, pred, label = self.loss(y_pred, y_true)
        self.log(f'{mode}_loss', loss, on_step=mode=='train', on_epoch=True, prog_bar=True, logger=True, add_dataloader_idx=False)
        
        if mode == 'train':
            metrics = self.train_metrics 
        elif mode == 'valid' :
            metrics = self.valid_metrics
        elif mode == 'renin':
            metrics = self.renin_metrics
            
        if len(metrics) > 0:
            for k, v in metrics.items():
                if k.startswith('RMSDCounter'):
                    res = v(y_pred, y_true)
                    for ri, (rmin, rmax) in enumerate(v.rmsd_ranges):
                        self.log(f'{mode}_{k}_{rmin:.0f}-{rmax:.0f}', res[ri], on_step=mode=='train', on_epoch=True, prog_bar=True, logger=True, add_dataloader_idx=False)
                else:
                    self.log(f'{mode}_{k}', v(y_pred, y_true), on_step=mode=='train', on_epoch=True, prog_bar=True, logger=True, add_dataloader_idx=False)
        if batch_idx[0] % CUDA_EMPTY_CACHE_EVERY == 0:
            torch.cuda.empty_cache()
        return loss

    def training_step(self, batch, batch_idx):
        return self._step(batch, batch_idx)

    def training_step_end(self, outputs):
        return self._step_end(outputs, 'train')

    def validation_step(self, batch, batch_idx, dataloader_idx):
        return self._step(batch, batch_idx, dataloader_idx)

    def validation_step_end(self, outputs):    
        mode = 'valid' if outputs[-1][0] == 0 else 'renin'
        return self._step_end(outputs, mode)

    def configure_optimizers(self):
        opt = torch.optim.Adam(self.parameters(), lr=self.lr)
        scheduler = ReduceLROnPlateau(opt, 'min', factor=self.factor, patience=self.patience)
        return {
            'optimizer': opt,
            'lr_scheduler': scheduler,
            'monitor': 'valid_loss'
        }


def get_ranking_loss(num_decoys=None, num_true=None, nn_rmsd=2.0, upsample_within_radius=None, upsample_factor: int=10):
    rank_loss = torch.nn.MarginRankingLoss(margin=100)

    def loss_fn(y_pred, rmsd_map):
        rmsd_map = rmsd_map.flatten()
        y_true = (rmsd_map > nn_rmsd) #.type(y_pred.dtype)
        y_true_batch = y_true
        y_pred_batch = y_pred.flatten()

        # select ~num_decoys decoys with label != 0
        frac = min(num_decoys / y_pred_batch.shape[0], 1.0) if num_decoys is not None else 1.0
        mask_0 = y_true_batch == 0
        mask = (torch.rand_like(y_pred_batch) < frac) * (~mask_0)
        # zeros are false, ones are partially true

        # randomly select num_true decoys with label 0
        num_0 = mask_0.sum()
        assert num_0 > 0, 'No near-native samples are present'
        if num_true is not None and num_0 > num_true:
            exclude_ids = torch.randperm(num_0 - num_true)
            mask_0[mask_0.nonzero()[exclude_ids]] = False
        mask[mask_0] = True

        ids_to_use = torch.nonzero(mask)[:, 0]
        print('selected samples:', len(ids_to_use))
        
        if upsample_within_radius is not None:
            reweight_ids = torch.nonzero(mask * (rmsd_map <= upsample_within_radius) * (rmsd_map > nn_rmsd))[:, 0]
            print('reweighted samples:', len(reweight_ids))
            ids_to_use = torch.cat([ids_to_use] + [reweight_ids] * (upsample_factor - 1))

        y_pred_batch = y_pred_batch[ids_to_use]
        y_true_batch = y_true_batch[ids_to_use]
        print('y_pred_batch', y_pred_batch.shape)
        print('num nn', (y_true_batch == 0).sum())

        pairs = torch.cartesian_prod(y_pred_batch[y_true_batch == 0], y_pred_batch[y_true_batch == 1])
        print('pairs', pairs.shape)
        
        labels = -torch.ones(pairs.shape[0], dtype=int, device=y_pred_batch.device)
        return rank_loss(pairs[:, 0], pairs[:, 1], labels), y_pred_batch, y_true_batch

    return loss_fn


def train(
        outdir,
        dataset_dir,
        train_set,
        valid_set,
        max_epoch=200,
        batch_size=4,
        ncores=0,
        model=None,
        ray_tune=False
):
    early_stop_patience = 200
    sched_patience = 2

    prody.confProDy(verbosity='error')

    #train_set = DockingDataset(dataset_dir, train_set, num_conformers=batch_size, shuffle=False)
    #train_loader = DataLoader(dataset=train_set, batch_size=batch_size, num_workers=ncores, shuffle=False) #, sampler=DistributedSampler(train_set, shuffle=False))

    #valid_set = DockingDataset(dataset_dir, valid_set, num_conformers=batch_size)
    #valid_loader = DataLoader(dataset=valid_set, batch_size=batch_size, num_workers=ncores, shuffle=False) #, sampler=DistributedSampler(valid_set, shuffle=False))

    if ray_tune:
        outdir = tune.get_trial_dir()

    model = LitModel(
        model,
        get_ranking_loss(num_true=50, upsample_within_radius=7.0, upsample_factor=20),
        patience=sched_patience
    )

    loggers = {TensorBoardLogger(outdir + '/logs', 'tb'), CSVLogger(outdir + '/logs', 'csv')}
    checkpoint_callback = ModelCheckpoint(dirpath=outdir + '/checkpoints', monitor='valid_loss', filename='checkpoint-{epoch:02d}-{valid_loss:.2f}-{valid_Top1:.2f}-{step}', save_top_k=200, mode='min', save_last=True)
    early_stopping = EarlyStopping(monitor='valid_loss', min_delta=0.00, patience=early_stop_patience, verbose=True, mode='min')
    tune_callback = TuneReportCallback({'ray_valid_loss': 'valid_loss'}, on='validation_end')
    
    trainer = pl.Trainer(
        precision=32,
        gpus=BATCH_SIZE,
        accelerator='dp',
        #plugins=[DeepSpeedPlugin(cpu_offload=False)],
        default_root_dir=outdir,
        #log_gpu_memory='all',
        logger=loggers,
        callbacks=[checkpoint_callback, early_stopping] + ([] if not ray_tune else [tune_callback]),
        log_every_n_steps=1,
        fast_dev_run=False,
        max_epochs=max_epoch,
        #overfit_batches=5,
        deterministic=True,
        #profiler='simple',
        #val_check_interval=0.5,
        reload_dataloaders_every_epoch=True,
        
        #limit_val_batches=2,
        #limit_train_batches=2,
        #num_sanity_val_steps=0,
        
        #resume_from_checkpoint=outdir + '/checkpoints/last.ckpt',
        replace_sampler_ddp=False
    )
    
    try:
        trainer.fit(model)
    except:
        with open(outdir / 'exception', 'w') as f:
            f.write(traceback.format_exc())
        raise


def main():
    import sys
    outdir = Path(sys.argv[1]).mkdir_p()

    model = NetSE3() #num_output_channels=32) #(num_cnn_layers=3)
    pytorch_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print('Num params:', pytorch_total_params)

    train(outdir,
          'dataset',
          TRAIN_SPLIT_DIR / 'train.json',
          TRAIN_SPLIT_DIR / 'valid.json',
          model=model)
    
    #from dataset
    #ag = prody.parsePDB('ADRB2_set/recs/3p0g_nmin.pdb')


if __name__ == '__main__':
    main()
