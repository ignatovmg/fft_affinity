import contextlib
import os

import gc
import numpy as np
import prody
import torch
import subprocess
from path import Path
from rdkit import Chem
from sblu.ft import read_rotations, read_ftresults, apply_ftresults_atom_group
from sblu.rmsd import calc_rmsd
from tqdm import tqdm
import pybel
from rdkit.Chem import AllChem
import traceback

import utils_loc
from dataset import make_protein_grids, make_protein_grids_new, make_surface_mask, make_lig_graph, DTYPE_FLOAT
from model import NetSE3
from train import LitModel


def _load_model(checkpoint, device):
    model = NetSE3()
    lit = LitModel.load_from_checkpoint(checkpoint, model=model, loss=None)
    model = lit.model
    model.to(device)
    model.eval()
    return model


def _to_tensor(x, device):
    return torch.tensor(x, device=device, requires_grad=False)


def _grid_to_tensor(grid, device):
    return _to_tensor(grid.astype(np.float32), device)


def _assert_equal_elements_ag_ag(ag1, ag2):
    ag_e = [x.upper() for x in ag1.getElements()]
    mol_e = [x.upper() for x in ag2.getElements()]
    assert all([x == y for x, y in zip(ag_e, mol_e)]), \
        f'Elements are different:\n{ag_e}\n{mol_e}'


def _assert_equal_elements_ag_rd(ag, mol_rdkit):
    ag_e = [x.upper() for x in ag.getElements()]
    mol_e = [x.GetSymbol().upper() for x in mol_rdkit.GetAtoms()]
    assert all([x == y for x, y in zip(ag_e, mol_e)]), \
        f'Elements are different:\n{ag_e}\n{mol_e}'


def _select_top_energy(energy_grid, rec_grid, lig_grid, ntop=1, radius=3):
    cor_shape = list(energy_grid.shape[-3:])
    lig_shape = np.array(lig_grid.grid.shape[1:])
    energies = []
    tvs = []

    energy_grid = energy_grid.flatten()
    for min_idx in energy_grid.argsort():
        if len(tvs) >= ntop:
            break

        min_idx = min_idx.item()
        min_idx3d = np.unravel_index(min_idx, cor_shape)

        #if mask_grid[min_idx3d] > 0:
        #    continue
        #mask_grid[
        #    min(0, min_idx3d[0]-radius):min_idx3d[0]+radius,
        #    min(0, min_idx3d[1]-radius):min_idx3d[1]+radius,
        #    min(0, min_idx3d[2]-radius):min_idx3d[2]+radius
        #] = 1

        min_energy = energy_grid[min_idx].item()
        tv = rec_grid.origin - lig_grid.origin + ((1 - lig_shape) + min_idx3d) * lig_grid.delta

        dists = [np.power(tv - x, 2).sum() for x in tvs]
        if len(dists) > 0 and any([x < radius**2 for x in dists]):
            continue

        energies.append(min_energy)
        tvs.append(tv)

    return energies, tvs


def _select_top_energy_square(energy_grid, rec_grid, lig_grid, ntop=1, radius=3):
    cor_shape = list(energy_grid.shape[-3:])
    lig_shape = np.array(lig_grid.grid.shape[1:])
    energies = []
    tvs = []
    rad_cells = (radius / lig_grid.delta).astype(int)
    print(rad_cells)

    energy_grid = energy_grid[0, 0]
    while len(tvs) < ntop:
        min_idx = torch.argmin(energy_grid).item()
        min_idx3d = np.unravel_index(min_idx, cor_shape)

        min_energy = energy_grid[tuple(min_idx3d)].item()
        tv = rec_grid.origin - lig_grid.origin + ((1 - lig_shape) + min_idx3d) * lig_grid.delta

        energies.append(min_energy)
        tvs.append(tv)

        energy_grid[
            min(0, min_idx3d[0]-rad_cells[0]):min_idx3d[0]+rad_cells[0],
            min(0, min_idx3d[1]-rad_cells[1]):min_idx3d[1]+rad_cells[1],
            min(0, min_idx3d[2]-rad_cells[2]):min_idx3d[2]+rad_cells[2]
        ] = 10000

    return energies, tvs


def _write_ft_results(fname, ft_results):
    with open(fname, 'w') as f:
        for x in ft_results:
            f.write(f'{x[0]:<4d} {x[1][0]:<8f} {x[1][1]:<8f} {x[1][2]:<8f} {x[2]:<8f}\n')


def _get_ag_dims(ag):
    crd = ag.getCoords()
    return crd.max(0) - crd.min(0)


def _calc_symm_rmsd(ag, ref_cset_id, symmetries):
    csets = ag.getCoordsets()
    ref_crd = csets[ref_cset_id]
    rmsds = []
    ele = ag.getElements()
    for m in symmetries:
        ref_atoms = list(symmetries[0])
        mob_atoms = list(m)
        assert all(ele[i] == ele[j] for i, j in zip(ref_atoms, mob_atoms)), f'Elements are different for symmetry {m}'

        rmsd = np.power(ref_crd[None, ref_atoms, :] - csets[:, mob_atoms, :], 2).sum((1, 2)) / ref_crd.shape[0]
        rmsds.append(rmsd)
    rmsds = np.sqrt(np.stack(rmsds).min(0))
    return rmsds


def _boltzman_clustering(ag, energies, symmetries, radius, min_size=1, max_clusters=None):
    energies = np.array(energies)
    unused = np.array(range(len(energies)))

    clusters = []
    while len(unused) > 0:
        center = unused[np.argmin(energies[unused])]
        rmsd = _calc_symm_rmsd(ag, center, symmetries)
        members = unused[np.where(rmsd[unused] < radius)[0]]
        unused = unused[~np.isin(unused, members)]
        if len(members) >= min_size:
            clusters.append((center.item(), members.tolist()))
        if max_clusters is not None and len(clusters) == max_clusters:
            break

    return clusters


def dock(case_dir, rot_file, checkpoint, **kwargs):
    case_dir = Path(case_dir)
    _dock(case_dir / 'rec_unbound_nmin.pdb', case_dir / 'frag_crys_ah.mol', rot_file, checkpoint, **kwargs)


def _dock(rec_pdb, frag_mol, rot_file, checkpoint, device='cuda:0', num_rots=500, tr_per_rot=5, num_poses=2500, crys_mol=None, clus_radius=3, include_zero_rotation=True):
    rec_ag = prody.parsePDB(rec_pdb)
    lig_rd = Chem.MolFromMolFile(frag_mol, removeHs=False)
    lig_ag = utils_loc.mol_to_ag(lig_rd)
    lig_rd_noh = Chem.MolFromMolFile(frag_mol, removeHs=True)
    symmetries = lig_rd.GetSubstructMatches(lig_rd_noh, uniquify=False)
    sasa = None #calc_sasa(rec_ag, normalize=False)

    if crys_mol is None:
        lig_ag_crys = lig_ag.copy()
        crys_matches = symmetries
    else:
        lig_rd_crys = Chem.MolFromMolFile(crys_mol, removeHs=False)
        lig_ag_crys = utils_loc.mol_to_ag(lig_rd_crys)
        crys_matches = lig_rd_crys.GetSubstructMatches(lig_rd_noh, uniquify=False)
        # print(crys_matches)
        #_assert_equal_elements(lig_ag.heavy, lig_ag_crys.heavy)

    prody.writePDB('rec.pdb', rec_ag)
    prody.writePDB('lig.pdb', lig_ag)

    rots = read_rotations(rot_file, num_rots)
    if include_zero_rotation:
        rots = np.insert(rots, 0, np.eye(3, 3), axis=0)

    if True:
        rec_grid = make_protein_grids(rec_ag, cell=1.0, atom_radius=2.0, padding=7, mode='point')
        assert not np.any(np.array(rec_grid.grid.shape[-3:]) > 100)

        sasa_grid = make_surface_mask(rec_ag, sasa=sasa, box_shape=rec_grid.grid.shape[1:], box_origin=rec_grid.origin, cell=1.0)
    
        net = _load_model(checkpoint, device)

        ft_results = [list() for x in range(tr_per_rot)]
        lig_ag_rotated = lig_ag.copy()
        for rot_id in tqdm(range(len(rots))):
            rot_mat = rots[rot_id]
            lig_coords = lig_ag.getCoords()
            lig_coords = np.dot(lig_coords - lig_coords.mean(0), rot_mat.T) + lig_coords.mean(0)
            lig_ag_rotated._setCoords(lig_coords, overwrite=True)
            G, lig_grid = make_lig_graph(lig_rd, lig_ag_rotated, hydrogens='all', box_kwargs={'cell': 1.0, 'atom_radius': 2.0, 'padding': 7.0, 'mode': 'point'})
            sample = {
                'id': _to_tensor([123 if rot_id > 0 else 123], device),  # to make model save grids for rot_id = 0
                'rec_grid': _grid_to_tensor(rec_grid.grid.astype(DTYPE_FLOAT), device)[None],
                'rec_grid_origin': _to_tensor(rec_grid.origin.astype(DTYPE_FLOAT), device)[None],
                'rec_grid_delta': _to_tensor(rec_grid.delta.astype(DTYPE_FLOAT), device)[None],
                'sasa_grid': _grid_to_tensor(sasa_grid.grid.astype(DTYPE_FLOAT), device)[None],
                'src': _to_tensor(G.edges()[0], device)[None],
                'dst': _to_tensor(G.edges()[1], device)[None],
                'coords': _to_tensor(G.ndata['x'], device)[None],
                'node_features': _to_tensor(G.ndata['f'], device)[None],
                'lig_grid': _to_tensor(G.ndata['grid'], device)[None],
                'lig_grid_origin': _to_tensor(G.ndata['grid_origin'][0], device)[None],
                'lig_grid_delta': _to_tensor(G.ndata['grid_delta'][0], device)[None],
                'edge_features': _to_tensor(G.edata['w'], device)[None],
                'num_nodes': _to_tensor(G.num_nodes(), device)[None],
                'num_edges': _to_tensor(G.num_edges(), device)[None]
            }

            # get energy grid batch
            energy_grid = net(sample)

            # select top poses for each rotation
            min_energies, tvs = _select_top_energy(energy_grid.detach(), rec_grid, lig_grid, ntop=tr_per_rot)
            for ft_, min_energy, tv in zip(ft_results, min_energies, tvs):
                ft_.append((rot_id, tuple(tv), min_energy))
                
            for x in sample.values():
                del x
            # important, otherwise memory overfills
            del energy_grid
            torch.cuda.empty_cache()
            gc.collect()

        for ft_id, ft_single_rot in enumerate(ft_results):
            ft_single_rot = sorted(ft_single_rot, key=lambda x: x[2])
            _write_ft_results(f'ft.000.{ft_id:02d}', ft_single_rot)

        ft_merged = []
        for x in ft_results:
            ft_merged += x
        ft_merged = sorted(ft_merged, key=lambda x: x[2])
        _write_ft_results(f'ft.merged', ft_merged)

    ft_merged = read_ftresults('ft.merged')
    lig_ft_ag = apply_ftresults_atom_group(lig_ag, ft_merged[:num_poses], rots)
    prody.writePDB('lig_ft.pdb', lig_ft_ag)

    #pwrmsd_ = pwrmsd(lig_ft_ag.heavy)
    #np.savetxt('pwrmsd.txt', pwrmsd_.flatten())

    #clusters = cluster(pwrmsd_, 3, 1, 100)
    clusters = _boltzman_clustering(lig_ft_ag, [x[2] for x in ft_merged[:num_poses]], symmetries, clus_radius)
    utils_loc.write_json(clusters, 'clusters_bzman.json')

    centers = [x[0] for x in clusters]
    lig_clus_ag = lig_ag.copy()
    lig_clus_ag._setCoords(lig_ft_ag.getCoordsets()[centers], overwrite=True)
    prody.writePDB('lig_clus_bzman.pdb', lig_clus_ag)

    rmsd = []
    for match in crys_matches:
        ag1 = lig_ft_ag
        ag2 = lig_ag_crys
        #_assert_equal_elements_ag_ag(ag1, ag2)
        assert np.all(ag1.getElements()[list(symmetries[0])] == ag2.getElements()[list(match)])
        rmsd.append(prody.calcRMSD(ag2.getCoords()[list(match)], ag1.getCoordsets()[:, list(symmetries[0]), :]))
    rmsd = np.stack(rmsd).min(0)
    np.savetxt('rmsd.txt', rmsd)

    np.savetxt('rmsd_clus_bzman.txt', rmsd[centers])


def main_test():
    dockdir = Path('docking/test_ripper_point_run_again').mkdir_p()
    for item in utils_loc.read_json('dataset/train_split/full_ligand/test.json')[::5]:
        case_name = Path(item['sdf_id'])
        case_dir = (Path('dataset') / 'data' / item['sdf_id']).abspath()
        wdir = (dockdir / case_name)
        oldpwd = Path.getcwd()
        print(wdir)
        if (wdir / 'lig_clus_bzman.pdb').exists():
            print(wdir / 'lig_clus_bzman.pdb', 'already exists')
            continue
        wdir.rmtree_p()
        case_dir.copytree(wdir)
        with cwd(wdir):
            _dock(
                case_dir / 'unbound_nmin.pdb',
                case_dir / 'crys_lig_ah.mol',
                oldpwd / '../../prms/rot70k.0.0.6.jm.mol2',
                #oldpwd / 'ripper_upsample20/checkpoints/checkpoint-epoch=09-valid_loss=1.28-valid_Top1=0.29-step=7499.ckpt',
                #oldpwd / 'zeblok_run_upsampled20/checkpoints/checkpoint-epoch=10-valid_loss=1.23-valid_Top1=0.13-step=8249.ckpt',
                #oldpwd / 'ripper_run/checkpoints/checkpoint-epoch=10-valid_loss=1.44-valid_Top1=0.48-step=15707.ckpt',
                oldpwd / 'ripper_point_run_again/checkpoints/checkpoint-epoch=05-valid_loss=1.30-valid_Top1=0.50-step=8567.ckpt',
                crys_mol=case_dir / 'crys_lig_ah.mol',
                clus_radius=5
            )
        if not (wdir / 'rmsd.txt').exists():
            wdir.rmtree()


def _add_hs(out_mol, in_mol):
    # add hydrogens
    mol = next(pybel.readfile('mol', in_mol))
    mol.addh()
    mol.localopt('mmff94', steps=500)
    mol.write('mol', out_mol, overwrite=True)

    # fix back the coordinates
    mol = Chem.MolFromMolFile(out_mol, removeHs=False)
    ag = utils_loc.mol_to_ag(mol)
    ref_ag = utils_loc.mol_to_ag(Chem.MolFromMolFile(in_mol, removeHs=True))
    tr = prody.calcTransformation(ag.heavy, ref_ag.heavy)
    ag = tr.apply(ag)
    prody.writePDB(Path(out_mol).stripext() + '.pdb', ag)

    utils_loc.change_mol_coords(mol, ag.getCoords())
    AllChem.ComputeGasteigerCharges(mol, throwOnParamFailure=True)

    Chem.MolToMolFile(mol, out_mol)


def _check_output(call, **kwargs):
    try:
        output = subprocess.check_output(call, **kwargs)
    except subprocess.CalledProcessError as e:
        print('Running command:', ' '.join(call))
        print('Command output:\n' + e.output.decode('utf-8'))
        raise


def _prepare_pdb(out_prefix, pdb, prefix='charmm_param'):
    _check_output(['sblu', 'pdb', 'prep',
                   '--psfgen', 'psfgen',
                   '--no-minimize',
                   '--out-prefix', out_prefix,
                   '--prm', f'{prefix}.prm',
                   '--rtf', f'{prefix}.rtf',
                   pdb])
    nmin_pdb = Path(out_prefix + '.pdb')
    nmin_psf = nmin_pdb.stripext() + '.psf'
    if not nmin_pdb.exists():
        raise RuntimeError('Cannot prepare', pdb)
    return nmin_pdb, nmin_psf


def main_1ela_and_co():
    #dockdir = Path('docking/1ela_ripper_frag_run_epoch24').mkdir_p()
    dockdir = Path('docking/zeblok_frag_ah_070121_run_epoch31').mkdir_p()
    #dockdir = Path('docking/2ren_zeblok_frag_run_epoch11').mkdir_p()
    case_name = '2ren_probes'
    rec_pdb = Path('/home/ignatovmg/projects/ftmap_1.5/data/2ren/2ren.pdb').abspath()

    oldpwd = Path.getcwd()
    for probe_mol in sorted(Path('/home/ignatovmg/projects/ftmap_1.5/probes').glob('????.mol')):
    #for probe_mol in sorted(Path('/home/ignatovmg/projects/ftmap_1.5/data/1ela_frags/new_probes').glob('frag_*.mol')):
        wdir = ((dockdir / case_name).mkdir_p() / probe_mol.basename().stripext()).mkdir_p()
        print(wdir)
        if (wdir / 'lig_clus_bzman.pdb').exists():
            print(wdir / 'lig_clus_bzman.pdb', 'already exists')
            continue
        try:
            with cwd(wdir):
                _prepare_pdb('rec_nmin', rec_pdb, prefix=oldpwd / 'dataset/prms/charmm_param')
                _add_hs('lig_ah.mol', probe_mol)
                #_add_hs('lig_crys.mol', '/home/ignatovmg/projects/ftmap_1.5/data/1ela_frags/1ela.mol')
                _dock(
                    'rec_nmin.pdb',
                    'lig_ah.mol',
                    oldpwd / '../../prms/rot70k.0.0.6.jm.mol2',
                    #oldpwd / 'ripper_frag_run/checkpoints/checkpoint-epoch=24-valid_loss=1.88-valid_Top1=0.32-step=27918.ckpt',
                    #oldpwd / 'zeblok_frag_run/checkpoints/checkpoint-epoch=11-valid_loss=1.55-valid_Top1=0.25-step=13651.ckpt',
                    #oldpwd / 'zeblok_frag_polar_run/checkpoints/checkpoint-epoch=18-valid_loss=1.69-valid_Top1=0.26-step=22077.ckpt',
                    #oldpwd / 'zeblok_frag_polar_recnew_run/checkpoints/checkpoint-epoch=24-valid_loss=3.95-valid_Top1=0.19-step=29049.ckpt',
                    #oldpwd / 'zeblok_frag_ah_070121_run/checkpoints/checkpoint-epoch=12-valid_loss=1.73-valid_Top1=0.22-step=15105.ckpt',
                    #oldpwd / 'zeblok_frag_polar_070621_run/checkpoints/checkpoint-epoch=15-valid_loss=1.68-valid_Top1=0.22-step=18591.ckpt',
                    oldpwd / 'zeblok_frag_ah_070121_run/checkpoints/checkpoint-epoch=31-valid_loss=2.37-valid_Top1=0.14-step=37183.ckpt',
                    clus_radius=3,
                    device='cpu',
                    include_zero_rotation=False
                    #crys_mol='lig_crys.mol'
                )
        except:
            with open(wdir / 'exc', 'w') as f:
                f.write(traceback.format_exc())
            traceback.print_exc()


def main_1ela_and_2ren():
    #dockdir = Path('docking/zeblok_frag_ah_nofgroups_072921_epoch11').mkdir_p()
    dockdir = Path('docking/zeblok_frag_ah_070121_epoch11').mkdir_p()
    oldpwd = Path.getcwd()
    custom_dir = Path('dataset/ftmap_cases_frags').abspath()

    for name in ['2ren', '1ela']:
        rec_pdb = custom_dir / name / 'rec.pdb'
        for mode in ['custom', 'probes']:
            case_name = f'{name}_{mode}'
            if mode == 'custom':
                frags = sorted((custom_dir / name).glob('frag_*_ah.mol'))
            if mode == 'probes':
                frags = sorted(Path('/home/ignatovmg/projects/ftmap_1.5/probes').glob('????.mol'))
            for probe_mol in frags:
                wdir = ((dockdir / case_name).mkdir_p() / probe_mol.basename().stripext()).mkdir_p()
                print(wdir)
                if (wdir / 'lig_clus_bzman.pdb').exists():
                    print(wdir / 'lig_clus_bzman.pdb', 'already exists')
                    continue
                try:
                    with cwd(wdir):
                        _prepare_pdb('rec_nmin', rec_pdb, prefix=oldpwd / 'dataset/prms/charmm_param')
                        _add_hs('lig_ah.mol', probe_mol)
                        if mode == 'custom':
                            Path(custom_dir / name / 'lig_ah.mol').copy('lig_crys.mol')
                        _dock(
                            'rec_nmin.pdb',
                            'lig_ah.mol',
                            oldpwd / '../../prms/rot70k.0.0.6.jm.mol2',
                            #oldpwd / 'ripper_frag_run/checkpoints/checkpoint-epoch=24-valid_loss=1.88-valid_Top1=0.32-step=27918.ckpt',
                            #oldpwd / 'zeblok_frag_run/checkpoints/checkpoint-epoch=11-valid_loss=1.55-valid_Top1=0.25-step=13651.ckpt',
                            #oldpwd / 'zeblok_frag_polar_run/checkpoints/checkpoint-epoch=18-valid_loss=1.69-valid_Top1=0.26-step=22077.ckpt',
                            #oldpwd / 'zeblok_frag_polar_recnew_run/checkpoints/checkpoint-epoch=24-valid_loss=3.95-valid_Top1=0.19-step=29049.ckpt',
                            oldpwd / 'zeblok_frag_ah_070121_run/checkpoints/checkpoint-epoch=11-valid_loss=1.49-valid_Top1=0.20-step=13943.ckpt',
                            #oldpwd / 'zeblok_frag_polar_070621_run/checkpoints/checkpoint-epoch=15-valid_loss=1.68-valid_Top1=0.22-step=18591.ckpt',
                            #oldpwd / 'zeblok_frag_ah_070121_run/checkpoints/checkpoint-epoch=31-valid_loss=2.37-valid_Top1=0.14-step=37183.ckpt',
                            #oldpwd / 'zeblok_frag_ah_nofgroups_072921/checkpoints/checkpoint-epoch=18-valid_loss=1.94-valid_Top1=0.25-step=22077.ckpt',
                            #oldpwd / 'zeblok_frag_ah_nofgroups_072921/checkpoints/checkpoint-epoch=11-valid_loss=1.39-valid_Top1=0.23-step=13943.ckpt',
                            clus_radius=3,
                            device='cpu',
                            include_zero_rotation=False,
                            crys_mol=None if mode == 'probes' else 'lig_crys.mol'
                        )
                except Exception:
                    with open(wdir / 'exc', 'w') as f:
                        f.write(traceback.format_exc())
                    traceback.print_exc()


def main_ligand_docking():
    dockdir = Path('lig_docking_3rad_500rot').mkdir_p()
    for case_dir in sorted((Path('dataset').abspath() / 'test_ligand_docking').glob('????_???'))[2:]:
        for frag_mol in sorted(case_dir.glob('frag_*_ah.mol')):
            wdir = ((dockdir / case_dir.basename()).mkdir_p() / frag_mol.basename().stripext()).mkdir_p()
            oldpwd = Path.getcwd()
            print(wdir)
            if (wdir / 'lig_clus_bzman.pdb').exists():
                continue
            with cwd(wdir):
                _dock(
                    case_dir / 'rec_unbound_nmin.pdb',
                    frag_mol,
                    oldpwd / '../../prms/rot70k.0.0.6.jm.mol2',
                    oldpwd / 'ripper_upsample20/checkpoints/checkpoint-epoch=09-valid_loss=1.28-valid_Top1=0.29-step=7499.ckpt'
                )
            if not (wdir / 'rmsd.txt').exists():
                wdir.rmtree()


def main_fragalysis_docking():
    dockdir = Path('fragalysis_docking').mkdir_p()
    for case_dir in sorted(Path('/home/ignatovmg/projects/ligand_benchmark/fragalysis/data').abspath().glob('*')):
        for frag_mol in sorted(case_dir.glob('frag_crys_ah.mol')):
            wdir = ((dockdir / case_dir.basename()).mkdir_p() / frag_mol.basename().stripext()).mkdir_p()
            oldpwd = Path.getcwd()
            print(wdir)
            if (wdir / 'lig_clus_bzman.pdb').exists():
                continue
            with cwd(wdir):
                _dock(
                    case_dir / 'unbound_nmin.pdb',
                    frag_mol,
                    oldpwd / '../../prms/rot70k.0.0.6.jm.mol2',
                    oldpwd / 'ripper_upsample20/checkpoints/checkpoint-epoch=09-valid_loss=1.28-valid_Top1=0.29-step=7499.ckpt'
                )
            #if not (wdir / 'rmsd.txt').exists():
            #    wdir.rmtree()


def main_mpro_docking():
    dockdir = Path('docking/mpro_bound_ripper_run').mkdir_p()
    for case_dir in sorted(Path('/home/ignatovmg/projects/ligand_benchmark/fragalysis/data/Mpro').abspath().glob('*')):
        for frag_mol in sorted(case_dir.glob('frag_*.mol')):
            wdir = ((dockdir / case_dir.basename()).mkdir_p() / frag_mol.basename().stripext()).mkdir_p()
            oldpwd = Path.getcwd()
            print(wdir)
            if (wdir / 'lig_clus_bzman.pdb').exists():
                continue
            with cwd(wdir):
                _dock(
                    case_dir / 'bound_nmin.pdb',
                    frag_mol,
                    oldpwd / '../../prms/rot70k.0.0.6.jm.mol2',
                    #oldpwd / 'ripper_upsample20/checkpoints/checkpoint-epoch=09-valid_loss=1.28-valid_Top1=0.29-step=7499.ckpt',
                    #oldpwd / 'zeblok_run_upsampled20/checkpoints/checkpoint-epoch=10-valid_loss=1.23-valid_Top1=0.13-step=8249.ckpt',
                    oldpwd / 'ripper_run/checkpoints/checkpoint-epoch=10-valid_loss=1.44-valid_Top1=0.48-step=15707.ckpt',
                    crys_mol=case_dir / 'frag_crys_ah.mol',
                    clus_radius=5
                )
                for x in case_dir.glob('*'): x.copy('.')
            #if not (wdir / 'rmsd.txt').exists():
            #    wdir.rmtree()


@contextlib.contextmanager
def cwd(path):
    oldpwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)


def Q15118():
    dockdir = Path('docking/zeblok_frag_ah_070121_epoch11').mkdir_p()
    oldpwd = Path.getcwd()
    custom_dir = Path('dataset/kinases').abspath()
    rec_pdb = custom_dir / 'nsp13-x0029_0A_bound_cut.pdb'
    case_name = rec_pdb.basename().stripext()
    frags = sorted(Path('/home/ignatovmg/projects/ftmap_1.5/probes').glob('????.mol'))
    for probe_mol in frags:
        wdir = ((dockdir / case_name).mkdir_p() / probe_mol.basename().stripext()).mkdir_p()
        try:
            with cwd(wdir):
                _prepare_pdb('rec_nmin', rec_pdb, prefix=oldpwd / 'dataset/prms/charmm_param')
                _add_hs('lig_ah.mol', probe_mol)
                _dock(
                    'rec_nmin.pdb',
                    'lig_ah.mol',
                    oldpwd / '../../prms/rot70k.0.0.6.jm.mol2',
                    oldpwd / 'zeblok_frag_ah_070121_run/checkpoints/checkpoint-epoch=11-valid_loss=1.49-valid_Top1=0.20-step=13943.ckpt',
                    clus_radius=3,
                    device='cpu',
                    include_zero_rotation=False,
                    crys_mol=None
                )
        except Exception:
            with open(wdir / 'exc', 'w') as f:
                f.write(traceback.format_exc())
            traceback.print_exc()


def kinases():
    dockdir = Path('/home/ignatovmg/projects/kinases/known_ftmap').mkdir_p()
    frags = sorted(Path('/home/ignatovmg/projects/ftmap_1.5/probes').glob('????.mol'))
    oldpwd = Path.getcwd()

    for rec_pdb in Path('/home/ignatovmg/projects/kinases/known').glob('*AF_domain_?.pdb'):
        case_name = rec_pdb.basename().stripext()
        for probe_mol in frags:
            wdir = ((dockdir / case_name).mkdir_p() / probe_mol.basename().stripext()).mkdir_p()
            try:
                with cwd(wdir):
                    _prepare_pdb('rec_nmin', rec_pdb, prefix=oldpwd / 'dataset/prms/charmm_param')
                    _add_hs('lig_ah.mol', probe_mol)
                    _dock(
                        'rec_nmin.pdb',
                        'lig_ah.mol',
                        oldpwd / '../../prms/rot70k.0.0.6.jm.mol2',
                        oldpwd / 'zeblok_frag_ah_070121_run/checkpoints/checkpoint-epoch=11-valid_loss=1.49-valid_Top1=0.20-step=13943.ckpt',
                        clus_radius=3,
                        device='cpu',
                        include_zero_rotation=False,
                        crys_mol=None
                    )
            except Exception:
                with open(wdir / 'exc', 'w') as f:
                    f.write(traceback.format_exc())
                traceback.print_exc()


if __name__ == '__main__':
    kinases()
