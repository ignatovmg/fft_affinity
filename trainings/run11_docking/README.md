---
Works with https://github.com/ignatovmg/ml_env/commit/ff752a7fe99938b4c2e05c7960665363b33ed0d1

Docking of fragments

Notes:

Some updates: 
* trying dot atoms insteads of gaussians
* embedding for protein atom types

Dots atoms work better than gaussians, use mode='point' from now on.

New rec config seem to be working worse, trying to see why. Its has extra donor, acceptor, charge grids, 
so it's weird that this doesn't make training better

Try:
* polar hydrogens only for lig
* decrease number of layers from 5 to 3