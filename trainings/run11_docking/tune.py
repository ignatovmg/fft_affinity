from ray import tune
from ray.tune.schedulers import ASHAScheduler

from path import Path
import torch.nn.functional as F
import torch
import numpy as np

from train import train
from model import NetSE3

DATASET_DIR = Path('dataset').abspath()


def trainable(config, checkpoint_dir=None):
    if config['middle_act'] == 'relu':
        middle_act = F.relu
    elif config['middle_act'] == 'tanh':
        middle_act = F.tanh
    else:
        raise RuntimeError('Unknown activation')

    if config['final_act'] == 'relu':
        final_act = F.relu
    elif config['final_act'] == 'tanh':
        final_act = F.tanh
    elif config['final_act'] is None:
        final_act = None
    else:
        raise RuntimeError('Unknown activation')

    model = NetSE3(31,
                   config['mid_size'],
                   config['mid_size'],
                   config['final_size'],
                   kernel_size=5,
                   num_middle_layers=config['num_layers'],
                   num_postfft_layers=1,
                   num_dense_layers=3,
                   num_rolls=2,
                   middle_activation=middle_act,
                   final_activation=final_act,
                   use_batchnorm=config['use_batchnorm'],
                   resnet=config['resnet']
                   )

    train(
        Path('details').mkdir_p().realpath(),
        DATASET_DIR,
        'train_split/train.json',
        'train_split/valid.json',
        max_epoch=200,
        batch_size=1,
        ncores=0,
        margin=100,
        lr=config['lr'],
        model=model,
        ray_tune=True,
        dataset_kwargs={}
    )


if __name__ == '__main__':
    space = {
        "mid_size": tune.choice([64, 128]),
        "final_size": tune.choice([64, 128]),
        "num_layers": tune.choice([10, 20]),
        "middle_act": tune.choice(['tanh', 'relu']),
        "final_act": tune.choice(['tanh', None]),
        "lr": tune.choice([0.0001]),
        "use_batchnorm": tune.choice([True, False]),
        "resnet":  tune.choice([True, False])
    }

    scheduler = ASHAScheduler(
        time_attr='epoch',
        metric='ray_top1',
        mode='max',
        max_t=200,
        grace_period=10
    )

    round_name = 'round1'

    analysis = tune.run(
        trainable,
        config=space,
        num_samples=200,
        resources_per_trial={"cpu": 2, "gpu": 1},
        scheduler=scheduler,
        # search_alg=HyperOptSearch(space, metric='CorrectPairsFrac', mode='max', points_to_evaluate=best_so_far),
        local_dir="./tuning",
        name=round_name,
        max_failures=0,
        resume=False,
        raise_on_failed_trial=False
    )

    print(analysis)
    print("Best config: ", analysis.get_best_config(metric="ray_pairs", mode="max"))
    df = analysis.dataframe(metric="ray_pairs", mode="max")
    df.to_csv(f'./tuning/{round_name}/best.csv')
