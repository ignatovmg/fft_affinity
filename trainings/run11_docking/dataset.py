import traceback
import random
import itertools
import numpy as np
import prody
from mol_grid import GridMaker, calc_sasa
from mol_grid.loggers import logger
from path import Path
from scipy.spatial.transform import Rotation
from torch.utils.data import Dataset
from rdkit import Chem
from rdkit.Chem import AllChem
from functools import partial

import torch

import utils_loc
from amino_acids import residue_bonds_noh, FUNCTIONAL_GROUPS, ATOM_TYPES
from sblu.ft import read_rotations, FTRESULT_DTYPE, read_ftresults, apply_ftresult, apply_ftresults_atom_group
import dgl

logger.setLevel('INFO')


DTYPE_FLOAT = np.float32
DTYPE_INT = np.int32

_ELEMENTS = {x[1]: x[0] for x in enumerate(['I', 'S', 'F', 'N', 'C', 'CL', 'BR', 'O', 'P', 'H'])}
_HYBRIDIZATIONS = {x: i for i, x in enumerate(Chem.rdchem.HybridizationType.names.keys())}
_FORMAL_CHARGE = {-1: 0, 0: 1, 1: 2}
_VALENCE = {x: x - 1 for x in range(1, 7)}
_NUM_HS = {x: x for x in range(5)}
_DEGREE = {x: x - 1 for x in range(1, 5)}


def _atom_to_vector(atom):
    vec = [0] * (len(_ELEMENTS) + 1)
    vec[_ELEMENTS[atom.GetSymbol().upper()]] = 1

    # total density of all atoms
    vec[-1] = 1

    # hydrogen switch
    vec += [int(atom.GetSymbol() != 'H')]

    new_vec = [0] * len(_HYBRIDIZATIONS)
    new_vec[_HYBRIDIZATIONS[str(atom.GetHybridization())]] = 1
    vec += new_vec

    new_vec = [atom.GetFormalCharge()]
    vec += new_vec

    new_vec = [0, 0]
    new_vec[int(atom.GetIsAromatic())] = 1
    vec += new_vec

    vec += [atom.GetTotalDegree()]

    vec += [atom.GetTotalNumHs()]

    new_vec = [0] * len(_VALENCE)
    new_vec[_VALENCE[atom.GetTotalValence()]] = 1
    vec += new_vec

    new_vec = [0, 0]
    new_vec[int(atom.IsInRing())] = 1
    vec += new_vec

    # Gasteiger charge
    vec += [float(atom.GetProp('_GasteigerCharge'))]

    return np.array(vec, dtype=DTYPE_FLOAT)


def _bond_to_vector(bond):
    # bond type
    vec = [0] * 4
    vec[max(min(int(bond.GetBondTypeAsDouble())-1, 3), 0)] = 1

    # is conjugated
    new_vec = [0] * 2
    new_vec[bond.GetIsConjugated()] = 1
    vec += new_vec

    # in ring
    new_vec = [0] * 2
    new_vec[bond.IsInRing()] = 1
    vec += new_vec
    return np.array(vec, dtype=DTYPE_FLOAT)


def _lig_grid_config(ag, atom_ids=None):
    if atom_ids is None:
        atom_ids = range(len(ag))
    return [ag[x:x+1] for x in atom_ids]


def _make_lig_grid(lig_ag, atom_ids=None, **box_kwargs):
    maker = GridMaker(
        config=partial(_lig_grid_config, atom_ids=atom_ids),
        centering='com',
        **box_kwargs
    )
    return maker.make_grids(lig_ag)


def make_lig_graph(lig_rd, lig_ag, connect_all=True, self_edge=True, hydrogens='all', box_kwargs={}):
    AllChem.ComputeGasteigerCharges(lig_rd, throwOnParamFailure=True)
    assert hydrogens in ['all', 'polar', 'none']

    mol_atoms = []
    node_features = []
    for atom in lig_rd.GetAtoms():
        if hydrogens == 'none' and atom.GetSymbol() == 'H':
            continue
        if hydrogens == 'polar' and atom.GetSymbol() == 'H' and any([x.GetSymbol().upper() == 'C' for x in atom.GetNeighbors()]):
            continue
        node_features.append(_atom_to_vector(atom))
        mol_atoms.append(atom.GetIdx())
    node_features = np.stack(node_features, axis=0).astype(DTYPE_FLOAT)

    mol_elements = np.array([lig_rd.GetAtomWithIdx(idx).GetSymbol().upper() for idx in mol_atoms])
    pdb_elements = np.array([lig_ag.getElements()[idx].upper() for idx in mol_atoms])
    assert all(mol_elements == pdb_elements), f'Elements are different:\nRDkit: {mol_elements}\nPDB  : {pdb_elements}'

    src, dst = [], []
    edge_features = []
    for i in mol_atoms:
        for j in mol_atoms:
            if i == j and not self_edge:
                continue
            edge = [mol_atoms.index(i), mol_atoms.index(j)]
            bond = lig_rd.GetBondBetweenAtoms(i, j)
            if not connect_all and bond is None:
                continue

            feat = np.zeros(9)
            feat[-1] = 1  # always set last bit to one
            if bond is not None:
                feat[:8] = _bond_to_vector(bond)

            src += edge
            dst += [edge[1], edge[0]]
            edge_features += [feat] * 2

    edge_features = np.stack(edge_features, axis=0).astype(DTYPE_FLOAT)
    
    lig_com = lig_ag.getCoords().mean(0)
    if 'box_center' not in box_kwargs:
        box_kwargs['box_center'] = lig_com
    grid = _make_lig_grid(lig_ag, atom_ids=mol_atoms, **box_kwargs)
    coords = lig_ag.getCoords()[mol_atoms, :].astype(DTYPE_FLOAT)
    
    G = dgl.graph((src, dst))
    G.ndata['x'] = torch.tensor(coords)
    G.ndata['f'] = torch.tensor(node_features)[..., None]
    G.ndata['grid'] = torch.tensor(grid.grid.astype(DTYPE_FLOAT))
    G.ndata['grid_origin'] = torch.tensor(np.tile(grid.origin, (coords.shape[0], 1)).astype(DTYPE_FLOAT))
    G.ndata['grid_delta'] = torch.tensor(np.tile(grid.delta, (coords.shape[0], 1)).astype(DTYPE_FLOAT))
    G.edata['d'] = torch.tensor(coords[dst] - coords[src])
    G.edata['w'] = torch.tensor(edge_features)
    return G, grid


def _rec_grid_config(ag):
    '''
    102 channels
    '''
    
    # elements
    ag = ag.stdaa.copy()
    selectors = [f'element {x}' for x in ['C', 'N', 'O', 'S', 'H']]
    
    # add sidechain atoms
    backbone = ['C', 'N', 'O', 'CA', 'OXT']
    sidechain_names = []
    for aa, x in residue_bonds_noh.items():
        for name in x.keys():
            if name not in backbone:
                sidechain_names.append((aa, name))
    selectors += [f'resname {aa} and name {name}' for aa, name in sidechain_names]
              
    # functional groups
    for gname, aas in FUNCTIONAL_GROUPS.items():
        aa_to_names = {}
        for aa, atoms in aas.items():
            names = []
            for entry in atoms:
                for x in entry:
                    names += x
            aa_to_names[aa] = names
        selectors.append(' or '.join([f'(resname {aa} and name {" ".join(names)})' for aa, names in aa_to_names.items()]))
        
    # backbone
    selectors += [f'backbone and name {x}' for x in ['C', 'N', 'CA', 'O', 'H']]
    
    return [ag.select(x) for x in selectors]


def _rec_grid_config_nofgroups(ag):
    '''
    102 channels
    '''

    # elements
    ag = ag.stdaa.copy()
    selectors = [f'element {x}' for x in ['C', 'N', 'O', 'S', 'H']]

    # add sidechain atoms
    backbone = ['C', 'N', 'O', 'CA', 'OXT']
    sidechain_names = []
    for aa, x in residue_bonds_noh.items():
        for name in x.keys():
            if name not in backbone:
                sidechain_names.append((aa, name))
    selectors += [f'resname {aa} and name {name}' for aa, name in sidechain_names]

    # backbone
    selectors += [f'backbone and name {x}' for x in ['C', 'N', 'CA', 'O', 'H']]

    # donors
    donors = []
    acceptors = []
    for resi, atoms in ATOM_TYPES.items():
        for atom, props in atoms.items():
            if props['donor']:
                donors.append((resi, atom))
            if props['acceptor']:
                acceptors.append((resi, atom))
    selectors.append(' or '.join([f'(resname {resi} and name {atom})' for resi, atom in donors]))
    selectors.append(' or '.join([f'(resname {resi} and name {atom})' for resi, atom in acceptors]))

    return [ag.select(x) for x in selectors]


def make_protein_grids(rec_ag, **box_kwargs):
    grid_maker_rec = GridMaker(config=_rec_grid_config, centering='coe', **box_kwargs)
    rec_grid = grid_maker_rec.make_grids(rec_ag)
    return rec_grid


def _rec_atom_types(ag):
    pair_to_id = {}
    cur_id = 0
    for resi, atoms in ATOM_TYPES.items():
        for atom, props in atoms.items():
            pair_to_id[(resi, atom)] = cur_id
            cur_id += 1

    types = []
    for resi, atom in zip(ag.getResnames(), ag.getNames()):
        types.append(pair_to_id[(resi, atom)])
    return types


def _rec_grid_config_new(ag):
    atom_types = ATOM_TYPES
    ag_atoms = set(zip(ag.getResnames(), ag.getNames()))

    # Individual atom types (203)
    selectors = []
    atom_list = []
    donors = []
    acceptors = []
    for resi, atoms in atom_types.items():
        for atom, props in atoms.items():
            atom_list.append((resi, atom))
            if props['donor']:
                donors.append((resi, atom))
            if props['acceptor']:
                acceptors.append((resi, atom))

            if atom.startswith('H') or atom in ['C', 'CA', 'N', 'O', 'H']:
                continue
            selectors.append(ag.select(f'resname {resi} and name {atom}'))

    atom_list = set(atom_list)
    assert ag_atoms.issubset(atom_list), f'Receptor has unknown atoms: {ag_atoms - atom_list}'

    # functional groups (7)
    for gname, aas in FUNCTIONAL_GROUPS.items():
        aa_to_names = {}
        for aa, atoms in aas.items():
            names = []
            for entry in atoms:
                for x in entry:
                    names += x
            aa_to_names[aa] = names
        selectors.append(ag.select(' or '.join([f'(resname {aa} and name {" ".join(names)})' for aa, names in aa_to_names.items()])))

    # backbone (5)
    selectors += [ag.select(f'name {x}') for x in ['C', 'CA', 'N', 'O', 'H']]

    # elements (5)
    selectors += [ag.select(f'element {x}') for x in ['C', 'N', 'O', 'S', 'H']]

    # donor / acc (2)
    selectors.append(ag.select(' or '.join([f'(resname {resi} and name {atom})' for resi, atom in donors])))
    selectors.append(ag.select(' or '.join([f'(resname {resi} and name {atom})' for resi, atom in acceptors])))

    return selectors


def _get_rec_charges(ag):
    charges = []
    for resi, atom in zip(ag.getResnames(), ag.getNames()):
        charges.append(ATOM_TYPES[resi][atom]['charge'])
    return charges


def make_protein_grids_new(rec_ag, **box_kwargs):
    grid_maker_rec = GridMaker(config=_rec_grid_config_new, centering='coe', **box_kwargs)
    rec_grid = grid_maker_rec.make_grids(rec_ag)

    # add a charge grid, based on charges in topology file
    charges = _get_rec_charges(rec_ag)
    charge_grid_maker = GridMaker(config=lambda x: [x.all], centering='coe', **box_kwargs)
    charge_grid = charge_grid_maker.make_grids(rec_ag, weights=charges)
    assert np.all(charge_grid.origin == rec_grid.origin)
    rec_grid.grid = np.concatenate([rec_grid.grid, charge_grid.grid])

    return rec_grid


def make_surface_mask(rec_ag, sasa=None, **box_kwargs):
    if sasa is None:
        sasa = calc_sasa(rec_ag, normalize=False)
    return GridMaker(atom_radius=7, config=lambda x: [x], mode='sphere', **box_kwargs).make_grids(rec_ag, weights=sasa)
    

class DockingDataset(Dataset):
    def __init__(
        self,
        dataset_dir,
        json_file,
        subset=None,
        num_conformers=4,
        rot_file='../../prms/rot70k.0.0.6.jm.mol2',
        box_size=None, #(80, 80, 80),
        cell=1.0,
        atom_radius=2.0,
        random_rotation=True,
        random_state=12345,
        shuffle_data=False
    ):

        self.dataset_dir = Path(dataset_dir).abspath()
        if not self.dataset_dir.exists():
            raise OSError(f'Directory {self.dataset_dir} does not exist')

        self.subset = subset

        self.json_file = self.dataset_dir.joinpath(json_file)
        if not self.json_file.exists():
            raise OSError(f'File {self.json_file} does not exist')

        self.data = utils_loc.read_json(self.json_file)
        if subset is not None:
            self.data = [v for k, v in enumerate(self.data) if k in subset]

        _data = []
        print('Before filter:', len(self.data))
        for x in self.data:
            ag = prody.parsePDB(self.dataset_dir / 'data' / x['sdf_id'] / 'unbound_nmin.pdb')
            box = (ag.getCoords().max(0) - ag.getCoords().min(0)) / cell
            #if np.any(box > 65):
            if np.prod(box) > 70**3:
                continue
            _data.append(x)
        self.data = _data
        print('After filter:', len(self.data))

        self.num_conformers = num_conformers
        
        self.random = random.Random(random_state)
        
        self.box_size = box_size
        self.cell = cell
        self.atom_radius = atom_radius
        self.random_rotation = random_rotation
        self.random_state = random_state
        
        self.rotations = read_rotations(rot_file, limit=10000)
        self.random.shuffle(self.rotations)
        
        if shuffle_data:
            self.random.shuffle(self.data)
        self._split_data_dict_to_conformers()

    def __len__(self):
        return len(self.data)

    def _split_data_dict_to_conformers(self):
        data = []
        for x in self.data:
            for conf_id in range(self.num_conformers):
                new_item = x.copy()
                new_item['conf_id'] = conf_id
                data.append(new_item)
        self.data = data

    def _rotate_ligand(self, ag, rot_mat):
        ag = ag.copy()
        coords = ag.getCoords()
        coords = np.dot(coords - coords.mean(0), rot_mat.T) + coords.mean(0)
        ag._setCoords(coords, overwrite=True)
        return ag

    #def _get_rand_rotation(self):
    #    return self.random.choice(self.rotations)

    @staticmethod
    def _split_models(ag):
        buf = ag.copy()
        buf._setCoords(buf.getCoords(), overwrite=True)
        out_ags = []
        for x in ag.getCoordsets():
            out_ags.append(buf.copy())
            out_ags[-1]._setCoords(x, overwrite=True)
        return out_ags

    def _calc_rmsd_square(self, crd, ref_crd):
        return np.power(crd - ref_crd, 2).sum(1).mean()

    def _get_rmsd_map(self, mob_crd, crys_crd, rec_grid, lig_grid, symmetries=None):
        cell = self.cell
        
        cor_shape = np.array(rec_grid.grid.shape)[1:] + np.array(lig_grid.grid.shape)[1:]
        mob_crd_aligned = mob_crd + rec_grid.origin - lig_grid.origin - cell * (np.array(lig_grid.grid.shape)[1:] - 1)
        vec_grid = np.indices(cor_shape.astype(int)) * cell
        vec_grid = np.rollaxis(vec_grid, 0, 4)
        
        if symmetries is None:
            symmetries = [range(crys_crd.shape[0])]
            
        rmsd_grids = []
        for sym in symmetries:
            crys_crd_cur = crys_crd[list(sym)]
            rmsd_aligned = self._calc_rmsd_square(mob_crd_aligned, crys_crd_cur)
            rmsd_grid = (vec_grid**2).sum(-1) + rmsd_aligned + 2 * np.dot(vec_grid, (mob_crd_aligned - crys_crd_cur).mean(0))
            rmsd_grids.append(rmsd_grid)
        rmsd_grid = np.stack(rmsd_grids).min(0)
        
        return np.sqrt(rmsd_grid)
    
    def _get_box_size_from_rec(self, rec_ag, padding=5.0):
        crd = rec_ag.getCoords()
        box = (crd.max(0) - crd.min(0) + padding * 2)
        box = self.cell * (box // self.cell + 1)
        return box

    def _get_box_size_from_lig(self, lig_ag, padding=5.0):
        crd = lig_ag.getCoords()
        box = (np.max(np.stack([crd.max(0) - crd.mean(0), crd.mean(0) - crd.min(0)]), 0) + padding) * 2
        box = self.cell * (box // self.cell + 1)
        return box
    
    def _get_item(self, ix):
        item = self.data[ix]
        case_dir = self.dataset_dir / 'data' / item['sdf_id']
        frag_name = Path(item['selected_fragment']['mol_path']).stripext() + '_ah'
        print(case_dir)
        rec_ag = prody.parsePDB(case_dir / 'unbound_nmin.pdb')
        lig_ag = prody.parsePDB(case_dir / frag_name + '.pdb')
        lig_rd = Chem.MolFromMolFile(case_dir / frag_name + '.mol', removeHs=False)
        lig_ag_crys = lig_ag.copy()
        rmsd_ag_crys = prody.parsePDB(case_dir / 'crys_lig_ah.pdb')
        rmsd_rd_crys = Chem.MolFromMolFile(case_dir / 'crys_lig_ah.mol', removeHs=False)
        
        #symmetries = utils_loc.read_json(case_dir / 'symmetries.json')
        sasa = np.loadtxt(case_dir / 'unbound_sasa.txt')
        #sasa = calc_sasa(rec_ag, normalize=False)

        if item['conf_id'] != 0:
            ligand_rotation = self.rotations[ix % len(self.rotations)]
            lig_ag = self._rotate_ligand(lig_ag, ligand_rotation)
        else:
            ligand_rotation = np.eye(3)

        if self.random_rotation:
            complex_rotation = self.rotations[(ix // self.num_conformers) % len(self.rotations)]
            tr = prody.Transformation(complex_rotation, np.array([0, 0, 0]))
            rec_ag = tr.apply(rec_ag)
            lig_ag = tr.apply(lig_ag)
            lig_ag_crys = tr.apply(lig_ag_crys)
            rmsd_ag_crys = tr.apply(rmsd_ag_crys)
        
        rec_grid = make_protein_grids_new(rec_ag, cell=self.cell, atom_radius=self.atom_radius, padding=7.0, mode='point')
        sasa_grid = make_surface_mask(rec_ag, sasa=sasa, box_shape=rec_grid.grid.shape[1:], box_origin=rec_grid.origin, cell=self.cell)
        print(rec_grid.grid.shape)
        if 'train' in self.json_file and np.prod(rec_grid.grid.shape[1:]) > 90**3:
            raise RuntimeError('Grid too large')
        
        rec_atom_types = _rec_atom_types(rec_ag)

        # derive lig box size from crystal ligand so that all rotated ligand grids are the same size
        lig_box_size = self._get_box_size_from_lig(lig_ag_crys, padding=7.0)
        G, lig_grid = make_lig_graph(lig_rd, lig_ag, hydrogens='polar', box_kwargs={'cell': self.cell, 'atom_radius': self.atom_radius, 'box_size': lig_box_size, 'mode': 'point'})
        
        # calc rmsd grid
        lig_rd_noh = Chem.MolFromMolFile(case_dir / frag_name + '.mol', removeHs=True)
        symmetries = rmsd_rd_crys.GetSubstructMatches(lig_rd_noh, uniquify=False)
        lig_ag_noh_atoms = lig_rd.GetSubstructMatches(lig_rd_noh, uniquify=True)[0]
        rmsd_map = self._get_rmsd_map(lig_ag.getCoords()[list(lig_ag_noh_atoms)], rmsd_ag_crys.getCoords(), rec_grid, lig_grid, symmetries=symmetries)
        
        if ix % 400 == 0:
            dumpdir = Path('grids').mkdir_p()
            prody.writePDB(dumpdir / 'rec.pdb', rec_ag)
            prody.writePDB(dumpdir / 'lig.pdb', lig_ag)
            prody.writePDB(dumpdir / 'lig_crys.pdb', lig_ag_crys)

        sample = {
            'id': ix,
            'rmsd_map': rmsd_map,
            'ligand_rotation': ligand_rotation,
            'complex_rotation': complex_rotation,
            'rec_grid': rec_grid.grid.astype(DTYPE_FLOAT),
            'rec_grid_origin': rec_grid.origin.astype(DTYPE_FLOAT),
            'rec_grid_delta': rec_grid.delta.astype(DTYPE_FLOAT),
            'rec_atom_types': np.array(rec_atom_types).astype(DTYPE_INT),
            'sasa_grid': sasa_grid.grid.astype(DTYPE_FLOAT),
            'conf_id': item['conf_id'],
            'src': G.edges()[0],
            'dst': G.edges()[1],
            'coords': G.ndata['x'],
            'node_features': G.ndata['f'],
            'lig_grid': G.ndata['grid'],
            'lig_grid_origin': G.ndata['grid_origin'][0],
            'lig_grid_delta': G.ndata['grid_delta'][0],
            'edge_features': G.edata['w'],
            'num_nodes': G.num_nodes(),
            'num_edges': G.num_edges()
        }
        return sample
    
    def __getitem__(self, ix):
        # if there is an error, fall back to the first sample
        try:
            return self._get_item(ix)
        except Exception:
            traceback.print_exc()
            return self._get_item(0)


def main():
    ds = DockingDataset('dataset', 'train_split/debug.json', rot_file='../../prms/rot70k.0.0.6.jm.mol2')
    #print(ds[0]['affinity_class'])
    print(ds[0])


if __name__ == '__main__':
    main()

