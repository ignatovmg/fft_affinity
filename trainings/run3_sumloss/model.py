import torch
import torch.nn.functional as F
import torch.optim
import numpy as np
import math

torch.cuda.manual_seed_all(123456)
torch.manual_seed(123456)
np.random.seed(123456)

from se3cnn import SE3Convolution
from se3cnn.non_linearities import ScalarActivation


class NetSE3(torch.nn.Module):
    def __init__(self,
                 rec_input_channels,
                 lig_input_channels,
                 embedding_size,
                 middle_channels,
                 output_channels,
                 kernel_size=5,
                 n_middle_layers=3,
                 dense_depth=0,
                 middle_activation=F.relu,
                 final_activation=F.tanh,
                 bins=(-10, 0, 1, 2, 3, 10)
                 ):
        super(NetSE3, self).__init__()
        self.bins = bins

        self.kernel_size = kernel_size
        assert kernel_size % 2 == 1
        self.middle_activation = middle_activation
        self.final_activation = final_activation
        self.rec = self._make_block(rec_input_channels, embedding_size, middle_channels, output_channels, n_middle_layers)
        self.lig = self._make_block(lig_input_channels, embedding_size, middle_channels, output_channels, n_middle_layers)
        self.dense = self._make_dense(output_channels, dense_depth)
        self.register_parameter(name='coef', param=torch.nn.Parameter(torch.Tensor([0.0001])))

    def _make_block(self, input_channels, embedding_size, middle_channels, output_channels, n_middle_layers):
        layers = [
            SE3Convolution([(input_channels, 0)], [(embedding_size, 0)], size=1, padding=0, stride=1, bias=None, dyn_iso=True),
            # ScalarActivation([(embedding_size, F.relu)], bias=False)
        ]

        for i in range(n_middle_layers):
            in_size = embedding_size if i == 0 else middle_channels
            layers.append(
                SE3Convolution([(in_size, 0)], [(middle_channels, 0)],
                               size=self.kernel_size,
                               padding=(self.kernel_size - 1) // 2,
                               stride=1,
                               bias=None,
                               dyn_iso=True))
            layers.append(ScalarActivation([(middle_channels, self.middle_activation)], bias=False))

        layers.append(SE3Convolution([(middle_channels, 0)], [(output_channels, 0)],
                                     size=self.kernel_size,
                                     padding=(self.kernel_size - 1) // 2,
                                     stride=1,
                                     bias=None,
                                     dyn_iso=True))

        if self.final_activation is not None:
            layers.append(ScalarActivation([(output_channels, self.final_activation)], bias=False))

        return torch.nn.Sequential(*layers)

    def _make_dense(self, in_size, n_layers):
        if n_layers == 0:
            return None
        if n_layers == 1:
            return torch.nn.Linear(in_size, 1)
        if n_layers == 2:
            return torch.nn.Sequential(
                torch.nn.Linear(in_size, in_size),
                torch.nn.ReLU(),
                torch.nn.Linear(in_size, 1)
            )
        raise RuntimeError('Wrong dense layer depth')

    @staticmethod
    def _calc_gaussian(x, mean, std):
        x = (x - mean) / std
        return torch.exp(-x*x / 2) / (std * math.sqrt(2 * math.pi))

    @classmethod
    def _scalar_to_classes(cls, scalar, bins, std):
        nclasses = len(bins)-1
        probs = torch.zeros((scalar.shape[0], nclasses), dtype=scalar.dtype, device=scalar.device)

        for x in torch.arange((scalar.min()-std*3).item(), (scalar.max()+std*4).item(), std):
            for i in range(nclasses):
                if (bins[i] <= x or i == 0) and (x < bins[i+1] or i == nclasses-1):
                    probs[:, i] += cls._calc_gaussian(x, scalar, std)

        probs /= probs.sum(1)[:, None]
        return probs

    @staticmethod
    def _correlate3d(a, b):
        a = F.pad(a, (0, a.shape[-1] - 1, 0, a.shape[-2] - 1, 0, a.shape[-3] - 1))
        b = F.pad(b, (b.shape[-1] - 1, 0, b.shape[-2] - 1, 0, b.shape[-3] - 1, 0))
        b = b.flip((-3, -2, -1))

        a1 = torch.rfft(a, 3, normalized=False, onesided=False)
        b1 = torch.rfft(b, 3, normalized=False, onesided=False)

        p = torch.zeros_like(a1)
        p[:, :, :, :, :, 0] = a1[:, :, :, :, :, 0] * b1[:, :, :, :, :, 0] - a1[:, :, :, :, :, 1] * b1[:, :, :, :, :, 1]
        p[:, :, :, :, :, 1] = a1[:, :, :, :, :, 0] * b1[:, :, :, :, :, 1] + a1[:, :, :, :, :, 1] * b1[:, :, :, :, :, 0]
        inv = torch.irfft(p, 3, normalized=False, onesided=False)
        return inv

    #def forward(self, rec_grid, rec_origin, rec_delta, lig_grid, lig_origin, lig_delta):
    def forward(self, rec_grid, lig_grid):
        rec_grid = self.rec(rec_grid)
        lig_grid = self.lig(lig_grid)
        cor_grid = self._correlate3d(rec_grid, lig_grid)
        # tanh to keep exponent from hitting the roof
        cor_grid = F.tanh(cor_grid / 10.0) * 10.0
        if self.dense is not None:
            energy_grid = self.dense(cor_grid.transpose(1, -1)).transpose(1, -1)
        else:
            energy_grid = cor_grid.sum(-4)
        energy_grid = energy_grid.reshape(energy_grid.shape[0], -1)
        exp_grid = torch.exp(-energy_grid)
        part_func = exp_grid.sum(1)
        # add coef 10e5 to keep the grid from having too small values
        #prob_grid = exp_grid * (10e3 / part_func[:, None])

        aff_log10 = torch.log10(part_func * self.coef.abs() + 0.0001)
        aff_classes = self._scalar_to_classes(aff_log10, self.bins, 1)

        print('Coef', self.coef)
        print('Energy grid', energy_grid.min(), energy_grid.max(), energy_grid.median())
        print('Log10(affinity)', aff_log10)
        print('Affinity probs', aff_classes)
        #print('Prob grid', prob_grid.min(), prob_grid.max(), prob_grid.median())
        return energy_grid, aff_classes, cor_grid


def _example():
    from dataset import LigandDataset
    ds = LigandDataset('dataset', 'train_split/train.json')
    item = ds[0]
    print(item['rec_grid'].shape, item['lig_grid'].shape)

    model = NetSE3(
        item['rec_grid'].shape[0],
        item['lig_grid'].shape[0],
        8, 8, 8, dense_depth=1,
        bins=(-10, 0, 1, 2, 3, 10)
    ).to('cuda:0')

    a, b, cor = model(
        torch.from_numpy(item['rec_grid'][None, :]).to('cuda:0'),
        torch.from_numpy(item['lig_grid'][None, :]).to('cuda:0')
    )

    from mol_grid import Grid
    print(cor.shape)
    Grid(cor.cpu().detach().sum(1).numpy(), origin=np.array([0., 0., 0.]), delta=np.array([1., 1., 1.])).save('tmp/grid.list')


if __name__ == '__main__':
    _example()
