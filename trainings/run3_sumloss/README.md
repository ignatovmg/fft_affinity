---
Works with https://bitbucket.org/ignatovmg/fft_ml commit 5721ff4

New: In fft_ml replaced serapate backward() calls with a single one for a sum of loss functions
