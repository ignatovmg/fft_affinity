import torch
import torch.nn.functional as F
import torch.optim
import numpy as np
from path import Path
import torch.utils.checkpoint as checkpoint

from se3cnn import SE3Convolution, SE3BNConvolution, SE3BatchNorm
from se3cnn.non_linearities import ScalarActivation

import dgl
from dgl.nn.pytorch import GraphConv, NNConv
from torch import nn
from torch.nn import functional as F
from typing import Dict, Tuple, List
import itertools
import math

from mol_grid import Grid


def conv3d(R_in, R_out, kernel_size):
    assert kernel_size % 2 == 1
    common_block_params = {
        'size': kernel_size,
        'padding': kernel_size // 2,
        'stride': 1,
        'dyn_iso': True,
        'bias': None
    }
    return SE3Convolution(R_in, R_out, **common_block_params)


class BasicBlockSE3(torch.nn.Module):
    def __init__(self, inplanes, planes, kernel_size=5, downsample=None, use_batchnorm=False, activation=F.relu):
        super(BasicBlockSE3, self).__init__()

        layers = [conv3d([(inplanes, 0)], [(planes, 0)], kernel_size)]
        if use_batchnorm:
            layers.append(torch.nn.BatchNorm3d(planes))

        layers.append(ScalarActivation([(planes, activation)], bias=False))

        layers.append(conv3d([(planes, 0)], [(planes, 0)], kernel_size))
        if use_batchnorm:
            layers.append(torch.nn.BatchNorm3d(planes))

        self.layers = torch.nn.ModuleList(layers)

        self.downsample = downsample

    def forward(self, x):
        identity = x
        out = x

        #if self.stride > 1:
        #    out = F.avg_pool3d(out, 3, stride=1, padding=1)

        for l in self.layers:
            #print(out.shape)
            out = l(out)

        if self.downsample is not None:
            identity = self.downsample(identity)

        out += identity
        #out = self.act(out)
        return out
    
    
def _to_numpy(t):
    return t.detach().cpu().numpy()


class NetSE3(torch.nn.Module):
    def __init__(
        self,
        #num_rec_features=99,
        num_rec_features_1d=26,
        num_rec_features_2d=88,
        num_rec_atom_types=99,
        num_lig_features=34,
        num_lig_edge_features=45,
        num_transformer_layers=1,
        num_lig_embed_channels=256,
        num_middle_channels=64,
        num_output_channels=64,
        num_cnn_layers=5,
        num_postfft_layers=0,
        num_dense_layers=3,
        middle_activation=F.relu,
        final_activation=torch.tanh,
        use_batchnorm=False,
        resnet=False
    ):

        super(NetSE3, self).__init__()
        self.__dict__.update({k: v for k, v in locals().items() if k != 'self'})

        #self.RecAttentionEmbedding = AttentionEmbedding(num_rec_features_1d, num_rec_features_2d, num_middle_channels, num_iter=2)
        self.RecAtomTypeEmbed = nn.Embedding(num_rec_atom_types, num_middle_channels)

        self.LigAttentionEmbedding = AttentionEmbedding(num_lig_features, num_lig_edge_features, num_middle_channels)

        self.rec = self._make_block(
            num_middle_channels + 1,
            num_middle_channels,
            num_output_channels,
            num_cnn_layers,
            middle_activation,
            final_activation,
            resnet=resnet,
            use_batchnorm=use_batchnorm,
            kernel_size=7
        )

        self.lig = self._make_block(
            num_middle_channels,
            num_middle_channels,
            num_output_channels,
            num_cnn_layers,
            middle_activation,
            final_activation,
            resnet=resnet,
            use_batchnorm=use_batchnorm,
            kernel_size=5
        )

        #self.dense1 = self._make_dense(num_output_channels, 1, num_dense_layers)
        self.dense1 = self._make_block(
            num_output_channels,
            num_output_channels,
            1,
            3,
            F.relu,
            None,
            False,
            False,
            1
        )

    def _make_block(
        self,
        input_channels,
        middle_channels,
        output_channels,
        num_layers,
        middle_act,
        final_act,
        resnet,
        use_batchnorm,
        kernel_size
    ):
        layers = []
        embedding_size = input_channels

        for i in range(num_layers-1):
            in_size = embedding_size if i == 0 else middle_channels
            if not resnet:
                layers.append(conv3d([(in_size, 0)], [(middle_channels, 0)], kernel_size))
                if use_batchnorm:
                    layers.append(torch.nn.BatchNorm3d(middle_channels))
                layers.append(ScalarActivation([(middle_channels, middle_act)], bias=False))
            else:
                layers.append(BasicBlockSE3(in_size, middle_channels, kernel_size, activation=middle_act, use_batchnorm=use_batchnorm))
                layers.append(ScalarActivation([(middle_channels, middle_act)], bias=False))

        layers.append(conv3d([(middle_channels, 0)], [(output_channels, 0)], kernel_size))
        if final_act is not None:
            layers.append(ScalarActivation([(output_channels, final_act)], bias=False))

        return torch.nn.Sequential(*layers)

    @staticmethod
    def _make_dense(in_size, out_size, n_layers):
        if n_layers == 1:
            return torch.nn.Linear(in_size, out_size, bias=False)
        if n_layers > 1:
            layers = []
            for x in range(n_layers - 1):
                layers += [torch.nn.Linear(in_size, in_size, bias=False), torch.nn.ReLU()]
            layers += [torch.nn.Linear(in_size, out_size, bias=False)]
            return torch.nn.Sequential(*layers)
        raise RuntimeError('Wrong dense layer depth')

    @staticmethod
    def _correlate3d_torch18(a, b):
        a_pad = F.pad(a, (0, b.shape[-1], 0, b.shape[-2], 0, b.shape[-3]))
        b_pad = F.pad(b, (a.shape[-1], 0, a.shape[-2], 0, a.shape[-3], 0))
        b_pad = b_pad.flip((-3, -2, -1))

        a1 = torch.fft.fftn(a_pad, dim=[-3, -2, -1], norm='backward')
        b1 = torch.fft.fftn(b_pad, dim=[-3, -2, -1], norm='backward')
        inv = torch.fft.ifftn(a1 * b1, dim=[-3, -2, -1], norm='backward')
        return inv.real

    @staticmethod
    def _correlate3d_torch16(a, b):
        a_pad = F.pad(a, (0, b.shape[-1], 0, b.shape[-2], 0, b.shape[-3]))
        b_pad = F.pad(b, (a.shape[-1], 0, a.shape[-2], 0, a.shape[-3], 0))
        b_pad = b_pad.flip((-3, -2, -1))

        a1 = torch.rfft(a_pad, 3, normalized=False, onesided=False)
        b1 = torch.rfft(b_pad, 3, normalized=False, onesided=False)

        p = torch.zeros_like(a1)
        p[..., 0] = a1[..., 0] * b1[..., 0] - a1[..., 1] * b1[..., 1]
        p[..., 1] = a1[..., 0] * b1[..., 1] + a1[..., 1] * b1[..., 0]
        inv = torch.irfft(p, 3, normalized=False, onesided=False)
        return inv

    @staticmethod
    def _make_graphs(args):
        graphs = []
        for i in range(args['num_nodes'].shape[0]):
            G = dgl.graph((args['src'][i][:args['num_edges'][i]], args['dst'][i][:args['num_edges'][i]]))
            G.ndata['x'] = args['coords'][i][:args['num_nodes'][i]]
            G.ndata['f'] = args['node_features'][i][:args['num_nodes'][i]]
            G.edata['d'] = G.ndata['x'][G.edges()[1]] - G.ndata['x'][G.edges()[0]]
            G.edata['w'] = args['edge_features'][i][:args['num_edges'][i]]
            graphs.append(G)
        return graphs

    def make_rec(self, args):
        rec_grids = args['rec_grid']
        #rec_features = self.RecAttentionEmbedding({'x1d': args['rec_1d'], 'x2d': args['rec_2d']})
        rec_extra_features = torch.tile(torch.arange(0, self.num_rec_atom_types, dtype=torch.int32, device=rec_grids.device), (rec_grids.shape[0], 1))
        rec_extra_features = self.RecAtomTypeEmbed(rec_extra_features)
        #rec_features = torch.cat([rec_extra_features, rec_features], dim=-2)
        #rec_grids = torch.einsum('baxyz,baf->bfxyz', rec_grids, rec_features)
        rec_grids = torch.einsum('baxyz,baf->bfxyz', rec_grids[:, :self.num_rec_atom_types], rec_extra_features)
        rec_grids = torch.cat([rec_grids, args['sasa_grid']], dim=1)
        rec_grids = self.rec(rec_grids)
        return rec_grids

    def make_lig(self, args):
        lig_features = self.LigAttentionEmbedding({'x1d': args['node_features'][..., 0], 'x2d': args['lig_pairwise']})
        if torch.any(args['id'] % 100 == 0):
            print('node_features')
            print(args['node_features'][..., 0])
            print('lig_features')
            print(lig_features)
        lig_grids_input = torch.einsum('baxyz,baf->bfxyz', args['lig_grid'], lig_features)
        lig_grids = self.lig(lig_grids_input)
        return lig_grids

    def custom(self, function):
        def custom_forward(*args):
            inputs = function(*args)
            return inputs
        return custom_forward

    def forward(self, args):
        # prepare grids
        #rec_grids = checkpoint.checkpoint(self.make_rec, args)
        #lig_grids = checkpoint.checkpoint(self.make_lig, args)
        rec_grids = self.make_rec(args)
        lig_grids = self.make_lig(args)

        # fft docking
        #cor_grids = checkpoint.checkpoint(self.custom(self._correlate3d_torch18), rec_grids, lig_grids)
        cor_grids = self._correlate3d_torch18(rec_grids, lig_grids)

        # apply fc layers
        energy_grids = self.dense1(cor_grids)

        print('Energy grid', energy_grids.min(), energy_grids.median(), energy_grids.max())
        
        for bi, idx in enumerate(args['id']):
            if idx % 400 == 0:
                grid_dir = Path('grids').mkdir_p() 
                grid_delta = _to_numpy(args['rec_grid_delta'][bi])

                lig_shape_numpy = np.array(args['lig_grid'][bi].shape[1:])
                grid = _to_numpy(energy_grids[bi])
                cor_origin = _to_numpy(args['rec_grid_origin'][bi]) - (lig_shape_numpy - 1) * grid_delta + lig_shape_numpy * 0.5 * grid_delta
                Grid(grid, origin=cor_origin, delta=grid_delta).save(grid_dir / 'energy_grid.list')

                if 'rmsd_map' in args:
                    grid = _to_numpy(args['rmsd_map'][bi][None])
                    Grid(grid, origin=cor_origin, delta=grid_delta).save(grid_dir / 'rmsd_grid.list')
                
                grid = _to_numpy(args['rec_grid'][bi][:10])
                grid_origin = _to_numpy(args['rec_grid_origin'][bi])
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'rec_precnn.list')
                
                grid = _to_numpy(rec_grids[bi][:10])
                grid_origin = _to_numpy(args['rec_grid_origin'][bi])
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'rec_cnn.list')
                
                #grid = _to_numpy(lig_grids_input[bi][:10])
                #grid_origin = _to_numpy(args['lig_grid_origin'][bi])
                #Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'lig_precnn.list')
                
                grid = _to_numpy(lig_grids[bi][:10])
                grid_origin = _to_numpy(args['lig_grid_origin'][bi])
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'lig_cnn.list')
                
                grid = _to_numpy(args['sasa_grid'][bi])
                grid_origin = _to_numpy(args['rec_grid_origin'][bi])
                Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'sasa_precnn.list')
                
                #grid = _to_numpy(sasa_mask[bi])
                #grid_origin = _to_numpy(args['rec_grid_origin'][bi])
                #Grid(grid, origin=grid_origin, delta=grid_delta).save(grid_dir / 'sasa_cnn.list')

        return energy_grids


class RowAttentionWithPairBias(nn.Module):
    def __init__(self, num_c, pair_rep_num_c, attn_num_c, num_heads):
        super().__init__()

        self.norm = nn.LayerNorm(num_c)
        self.q = nn.Linear(num_c, attn_num_c * num_heads, bias=False)
        self.k = nn.Linear(num_c, attn_num_c * num_heads, bias=False)
        self.v = nn.Linear(num_c, attn_num_c * num_heads, bias=False)
        self.proj2d = nn.Linear(pair_rep_num_c, num_heads, bias=False)
        self.final = nn.Linear(attn_num_c * num_heads, num_c)

        self.attn_num_c = attn_num_c
        self.num_heads = num_heads

    def forward(self, x1d, x2d):
        x1d = self.norm(x1d)

        q = self.q(x1d).view(*x1d.shape[:-1], self.attn_num_c, self.num_heads)
        k = self.k(x1d).view(*x1d.shape[:-1], self.attn_num_c, self.num_heads)
        v = self.v(x1d).view(*x1d.shape[:-1], self.attn_num_c, self.num_heads)

        aff = torch.einsum('bich,bjch->bijh', q, k)
        bias = self.proj2d(x2d.clone())
        weights = torch.softmax(aff / math.sqrt(self.attn_num_c) + bias, dim=-2)

        x1d = torch.einsum('brch,birh->bich', v, weights)
        x1d = self.final(x1d.reshape(*x1d.shape[:-2], -1))
        return x1d


class Transition(nn.Module):
    def __init__(self, num_c, n):
        super().__init__()
        self.norm = nn.LayerNorm(num_c)
        self.l1 = nn.Linear(num_c, num_c * n)
        self.l2 = nn.Linear(num_c * n, num_c)

    def forward(self, x):
        x = self.norm(x)
        x = self.l1(x).relu()
        x = self.l2(x)
        return x


class OuterProductMean(nn.Module):
    def __init__(self, in_c, mid_c, out_c):
        super().__init__()
        self.norm = nn.LayerNorm(in_c)
        self.l1 = nn.Linear(in_c, mid_c)
        self.l2 = nn.Linear(in_c, mid_c)
        self.final = nn.Linear(mid_c * mid_c, out_c)

    def forward(self, x):
        x = self.norm(x)
        a = self.l1(x)
        b = self.l2(x)
        mean = torch.einsum('bix,bjy->bijxy', a, b)
        mean = mean.view(*mean.shape[:-2], -1)
        pw_update = self.final(mean)
        return pw_update


class TriangleMultiplicationOutgoing(nn.Module):
    def __init__(self, in_c, mid_c):
        super().__init__()
        self.norm1 = nn.LayerNorm(in_c)
        self.norm2 = nn.LayerNorm(mid_c)
        self.l1i = nn.Linear(in_c, mid_c)
        self.l1j = nn.Linear(in_c, mid_c)
        self.l1i_sigm = nn.Linear(in_c, mid_c)
        self.l1j_sigm = nn.Linear(in_c, mid_c)
        self.l2_proj = nn.Linear(mid_c, in_c)
        self.l3_sigm = nn.Linear(in_c, in_c)

    def forward(self, x2d):
        x2d = self.norm1(x2d)
        i = self.l1i(x2d) * torch.sigmoid(self.l1i_sigm(x2d))
        j = self.l1j(x2d) * torch.sigmoid(self.l1j_sigm(x2d))
        out = torch.einsum('bikc,bjkc->bijc', i, j)
        out = self.norm2(out)
        out = self.l2_proj(out)
        out = out * torch.sigmoid(self.l3_sigm(x2d))
        return out


class TriangleMultiplicationIngoing(nn.Module):
    def __init__(self, in_c, mid_c):
        super().__init__()
        self.norm1 = nn.LayerNorm(in_c)
        self.norm2 = nn.LayerNorm(mid_c)
        self.l1i = nn.Linear(in_c, mid_c)
        self.l1j = nn.Linear(in_c, mid_c)
        self.l1i_sigm = nn.Linear(in_c, mid_c)
        self.l1j_sigm = nn.Linear(in_c, mid_c)
        self.l2_proj = nn.Linear(mid_c, in_c)
        self.l3_sigm = nn.Linear(in_c, in_c)

    def forward(self, x2d):
        x2d = self.norm1(x2d)
        i = self.l1i(x2d) * torch.sigmoid(self.l1i_sigm(x2d))
        j = self.l1j(x2d) * torch.sigmoid(self.l1j_sigm(x2d))
        out = torch.einsum('bkic,bkjc->bijc', i, j)
        out = self.norm2(out)
        out = self.l2_proj(out)
        out = out * torch.sigmoid(self.l3_sigm(x2d))
        return out


class TriangleAttentionStartingNode(nn.Module):
    def __init__(self, pair_rep_num_c, attention_num_c, num_heads):
        super().__init__()
        self.attention_num_c = attention_num_c
        self.num_heads = num_heads

        self.norm = nn.LayerNorm(pair_rep_num_c)
        self.q = nn.Linear(pair_rep_num_c, attention_num_c * num_heads, bias=False)
        self.k = nn.Linear(pair_rep_num_c, attention_num_c * num_heads, bias=False)
        self.v = nn.Linear(pair_rep_num_c, attention_num_c * num_heads, bias=False)
        self.bias = nn.Linear(pair_rep_num_c, num_heads, bias=False)
        self.gate = nn.Linear(pair_rep_num_c, attention_num_c * num_heads)
        self.out = nn.Linear(attention_num_c * num_heads, pair_rep_num_c)

    def forward(self, x2d):
        x2d = self.norm(x2d)

        q = self.q(x2d).view(*x2d.shape[:-1], self.attention_num_c, self.num_heads)
        k = self.k(x2d).view(*x2d.shape[:-1], self.attention_num_c, self.num_heads)
        v = self.v(x2d).view(*x2d.shape[:-1], self.attention_num_c, self.num_heads)
        b = self.bias(x2d)
        g = torch.sigmoid(self.gate(x2d).view(*x2d.shape[:-1], self.attention_num_c, self.num_heads))

        b = b.unsqueeze_(1).transpose_(2, 3)
        w = torch.softmax(torch.einsum('bijch,bikch->bijkh', q, k) / math.sqrt(self.attention_num_c) + b, dim=-2)
        out = torch.einsum('bijkh,bikch->bijch', w, v) * g
        out = self.out(out.flatten(start_dim=-2))
        return out


class TriangleAttentionEndingNode(nn.Module):
    def __init__(self, pair_rep_num_c, attention_num_c, num_heads):
        super().__init__()
        self.attention_num_c = attention_num_c
        self.num_heads = num_heads

        self.norm = nn.LayerNorm(pair_rep_num_c)
        self.q = nn.Linear(pair_rep_num_c, attention_num_c * num_heads, bias=False)
        self.k = nn.Linear(pair_rep_num_c, attention_num_c * num_heads, bias=False)
        self.v = nn.Linear(pair_rep_num_c, attention_num_c * num_heads, bias=False)
        self.bias = nn.Linear(pair_rep_num_c, num_heads, bias=False)
        self.gate = nn.Linear(pair_rep_num_c, attention_num_c * num_heads)
        self.out = nn.Linear(attention_num_c * num_heads, pair_rep_num_c)

    def forward(self, x2d):
        x2d = self.norm(x2d)

        q = self.q(x2d).view(*x2d.shape[:-1], self.attention_num_c, self.num_heads)
        k = self.k(x2d).view(*x2d.shape[:-1], self.attention_num_c, self.num_heads)
        v = self.v(x2d).view(*x2d.shape[:-1], self.attention_num_c, self.num_heads)
        b = self.bias(x2d)
        g = torch.sigmoid(self.gate(x2d).view(*x2d.shape[:-1], self.attention_num_c, self.num_heads))

        b = b.unsqueeze_(2)
        w = torch.softmax(torch.einsum('bijch,bkjch->bijkh', q, k) / math.sqrt(self.attention_num_c) + b, dim=-2)
        out = torch.einsum('bijkh,bkjch->bijch', w, v) * g
        out = self.out(out.flatten(start_dim=-2))
        return out


class AttentionEmbeddingIteration(torch.nn.Module):
    def __init__(self, x1d_num_c, x2d_num_c):
        super().__init__()
        self.RowAttentionWithPairBias = RowAttentionWithPairBias(x1d_num_c, x2d_num_c, 32, 4)
        self.Transition = Transition(x1d_num_c, 2)
        self.OuterProductMean = OuterProductMean(x1d_num_c, 32, x2d_num_c)
        self.TriangleMultiplicationOutgoing = TriangleMultiplicationOutgoing(x2d_num_c, 64)
        self.TriangleMultiplicationIngoing = TriangleMultiplicationIngoing(x2d_num_c, 64)
        self.TriangleAttentionStartingNode = TriangleAttentionStartingNode(x2d_num_c, 32, 4)
        self.TriangleAttentionEndingNode = TriangleAttentionEndingNode(x2d_num_c, 32, 4)
        self.PairTransition = Transition(x2d_num_c, 2)

    def forward(self, x):
        x1d = x['x1d']
        x2d = x['x2d']
        x1d += self.RowAttentionWithPairBias(x1d.clone(), x2d)
        x1d += self.Transition(x1d.clone())
        x2d += self.OuterProductMean(x1d.clone())
        x2d += self.TriangleMultiplicationOutgoing(x2d.clone())
        x2d += self.TriangleMultiplicationIngoing(x2d.clone())
        x2d += self.TriangleAttentionStartingNode(x2d.clone())
        x2d += self.TriangleAttentionEndingNode(x2d.clone())
        x2d += self.PairTransition(x2d.clone())
        return {'x1d': x1d, 'x2d': x2d}


class AttentionCombine1d2d(torch.nn.Module):
    def __init__(self, x1d_num_c, x2d_num_c, attn_num_c, num_heads, out_num_c):
        super().__init__()
        self.attn_num_c = attn_num_c
        self.num_heads = num_heads

        self.x1_qv = nn.Linear(x1d_num_c, attn_num_c * num_heads * 2, bias=False)
        self.x2_kv = nn.Linear(x2d_num_c, attn_num_c * num_heads * 2, bias=False)
        self.final_proj = nn.Linear(attn_num_c * num_heads, out_num_c)

    def forward(self, x):
        x1d = x['x1d']
        x2d = x['x2d']
        x1q, x1v = torch.tensor_split(self.x1_qv(x1d).view(*x1d.shape[:-1], self.attn_num_c, self.num_heads * 2), 2, dim=-1)
        x2k, x2v = torch.tensor_split(self.x2_kv(x2d).view(*x2d.shape[:-1], self.attn_num_c, self.num_heads * 2), 2, dim=-1)
        weights = torch.softmax(torch.einsum('bich,bijch->bijh', x1q, x2k), dim=-1)
        out = x1v + (weights.unsqueeze(-2) * x2v).sum(2)
        return self.final_proj(out.flatten(start_dim=-2))


class AttentionEmbedding(torch.nn.Module):
    def __init__(self, x1d_num_c, x2d_num_c, out_num_c, num_iter=3):
        super().__init__()
        self.layers = nn.ModuleList([AttentionEmbeddingIteration(x1d_num_c, x2d_num_c) for x in range(num_iter)])
        self.project = AttentionCombine1d2d(x1d_num_c, x2d_num_c, 32, 4, out_num_c)

    def forward(self, x):
        for l in self.layers:
            x = l(x)
        return self.project(x)


def _example():
    from dataset import DockingDataset
    from torch.utils.data import DataLoader

    def _correlate1d_torch18(a, b):
        a = F.pad(a, (0, a.shape[-1] - 1))
        b = F.pad(b, (b.shape[-1] - 1, 0))
        b = b.flip((-1))

        a1 = torch.fft.fftn(a, dim=[-1], norm='backward')
        b1 = torch.fft.fftn(b, dim=[-1], norm='backward')
        inv = torch.fft.ifftn(a1 * b1, dim=[-1], norm='backward')
        return inv.real

    a = torch.ones(4)
    b = torch.ones(4)
    print(_correlate1d_torch18(a, b))
    #from mol_grid import Grid
    #print(cor.shape)
    #Grid(cor.cpu().detach().sum(1).numpy(), origin=np.array([0., 0., 0.]), delta=np.array([1., 1., 1.])).save('tmp/grid.list')


def example2():
    torch.autograd.set_detect_anomaly(True)

    model = AttentionEmbedding(64, 64, 32)
    pytorch_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print('Num params:', pytorch_total_params)

    model = model
    x1d = torch.zeros((1, 32, 64))
    x2d = torch.zeros((1, 32, 32, 64))
    out = model({'x1d': x1d, 'x2d': x2d})

    print(out)
    #print(torch.cuda.max_memory_allocated('cuda:0') / (8 * 1024 * 1024))
    #(out['x2d'].sum() + out['x1d'].sum()).backward()


def example3():
    model = TriangleMultiplicationIngoing(64, 64)
    x = torch.zeros((1, 8, 8, 64))
    print(model(x).sum().backward())


if __name__ == '__main__':
    example2()
