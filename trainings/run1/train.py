import torch
import torch.nn.functional as F
import torch.optim
import prody
import numpy as np
from path import Path
from torch.utils.data import DataLoader

torch.cuda.manual_seed_all(123456)
torch.manual_seed(123456)
np.random.seed(123456)

from fft_ml.training import engines
from fft_ml.loggers import logger
#from fft_ml.training.metrics import RankingCorrectPairsFraction
from mol_grid import loggers

from dataset import LigandDataset
from model import NetSE3


def train(
        dataset_dir,
        train_set,
        valid_set,
        max_epoch,
        device_name,
        load_epoch=None,
        batch_size=32,
        ncores=4,
        ngpu=1,
        nsamples=None,
        margin=100,
        lr=0.0001,
        weight_decay=0.00001,
        model=NetSE3(95, 51, 20, 20, 10, 3),
        ray_tune=False,
        ray_checkpoint_dir=None,
        output_dir='train'):

    loggers.logger.setLevel('INFO')
    for k, v in locals().items():
        logger.info("{:20s} = {}".format(str(k), str(v)))

    prody.confProDy(verbosity='error')

    logger.info('Getting device..')
    device = engines.get_device(device_name)
    subset = None if nsamples is None else list(range(nsamples))

    logger.info('Creating train dataset..')
    train_set = LigandDataset(dataset_dir, train_set, subset=subset, random_rotation=True)
    train_loader = DataLoader(dataset=train_set, batch_size=batch_size, num_workers=ncores, shuffle=True)

    logger.info('Creating validation dataset..')
    valid_set = LigandDataset(dataset_dir, valid_set, subset=subset, random_rotation=True)
    valid_loader = DataLoader(dataset=valid_set, batch_size=batch_size, num_workers=ncores, shuffle=False)

    num_channels = train_set[0]['rec_grid'].shape[0]

    optimizer = torch.optim.Adam
    optimizer_params = {'lr': lr, 'weight_decay': weight_decay}

    #loss = [torch.nn.MSELoss(), torch.nn.NLLLoss(ignore_index=-1)]
    losses = [torch.nn.MSELoss(), torch.nn.CrossEntropyLoss(ignore_index=-1)]
    #loss_fn = torch.nn.()


    logger.info('Started training..')
    trainer = engines.Trainer(None,
                              max_epoch,
                              train_loader,
                              model,
                              optimizer,
                              optimizer_params,
                              losses,
                              device,
                              x_keys=['rec_grid', 'lig_grid'],
                              y_key=['prob_map', 'affinity_class'],
                              #metrics_dict={'CorrectPairsFrac': RankingCorrectPairsFraction()},
                              metrics_dict=None,
                              test_loader=valid_loader,
                              save_model=True,
                              load_epoch=load_epoch,
                              use_scheduler=True,
                              scheduler_params={'factor': 0.1, 'patience': 3},
                              ngpu=ngpu,
                              every_niter=50,
                              ray_tune=ray_tune,
                              ray_checkpoint_dir=ray_checkpoint_dir,
                              early_stopping=200,
                              collect_garbage_every_n_iter=3,
                              output_dir=Path(output_dir).mkdir_p())
    trainer.run()


def _last_epoch(dir):
    last_epoch = sorted([int(x.stripext().split('_')[-1]) for x in Path(dir).glob('checkpoint*')])
    return None if len(last_epoch) == 0 else last_epoch[-1]


def main():
    import sys

    outdir = Path(sys.argv[1]).mkdir_p()
    gpu_id = int(sys.argv[2])

    # get last computed epoch
    last_epoch = _last_epoch(outdir)

    model = NetSE3(95, 52, 64, 32, 32,
                   kernel_size=5,
                   n_middle_layers=5,
                   dense_depth=2,
                   middle_activation=F.relu,
                   final_activation=F.tanh)

    train('dataset',
          'train_split/train.json',
          'train_split/valid.json',
          max_epoch=200,
          device_name=f'cuda:{gpu_id}',
          load_epoch=last_epoch,
          batch_size=1,
          ncores=0,
          ngpu=1,
          nsamples=None,
          lr=0.0001,
          weight_decay=0.0000001,
          model=model,
          ray_tune=False,
          ray_checkpoint_dir=None,
          output_dir=outdir)


if __name__ == '__main__':
    main()
