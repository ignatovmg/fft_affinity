import sys
import numpy as np
import prody
from sblu.ft import read_rotations, read_ftresults, apply_ftresults_atom_group


# R * (X - com) + com + T = Rnew * X + Tnew
# --> Rnew = R; Tnew = T + com - R * com


def _write_ft_results(fname, ft_results):
    with open(fname, 'w') as f:
        for x in ft_results:
            f.write(f'{x[0]:<4d} {x[1][0]:<8f} {x[1][1]:<8f} {x[1][2]:<8f} {x[2]:<8f}\n')


def _read_ft_results(fname):
    ft_results = []
    with open(fname, 'r') as f:
        for line in f:
            line = line.split()
            ft_results.append([int(line[0]), (float(line[1]), float(line[2]), float(line[3])), float(line[4])])
    return ft_results


if __name__ == '__main__':
    ftfile = sys.argv[1]
    rotfile = sys.argv[2]
    lig_pdb = sys.argv[3]
    out_ftfile = sys.argv[4]

    lig_ag = prody.parsePDB(lig_pdb)
    com = lig_ag.getCoords().mean(0)
    ft = read_ftresults(ftfile)
    rots = read_rotations(rotfile)
    for ftres in ft:
        rot = rots[ftres[0]]
        ftres[1][:] = ftres[1] + com - np.dot(com, rot.T)
    _write_ft_results(out_ftfile, ft)
