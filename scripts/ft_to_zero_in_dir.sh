#!/bin/bash

for x in */????/ft*; do
  python ~/projects/fft_affinity/trainings/run12_docking/ft_to_zero.py $x ~/projects/fft_affinity/prms/rot70k.0.0.6.jm.mol2 $(dirname $x)/lig.pdb $x.zero;
done